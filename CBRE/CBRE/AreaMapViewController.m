//
//  AreaMapViewController.m
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaMapViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "AreaBean.h"
#import "BuildingAnnotation.h"
#import "BuildinDetailViewController.h"
#import "AlbumPhotoViewController.h"
#import "BuildingMiniCell.h"
#import "BuildingCell.h"
#import "CBREPopoverBackgroundView.h"
#import "CBREPopoverNotBackgroundView.h"
#import "CBREMapUtils.h"
#import "AreaDao.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface AreaMapViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property (strong, nonatomic) NSArray<AreaBean> *aAreas;

@end

@implementation AreaMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    _aAreas = [_oCity getAreas];
 
    UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _oListButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(showListClientsShowsInMap:)];
    _oListButton.tag = 1;
    self.navigationItem.rightBarButtonItem = _oListButton;
    
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oBtnShowCities, _oBtnShowAreas, _oLblSectorName, nil] withType:CBREFontTypeHelveticaNeue47LightCondensed];
    [CBREFont setFontInLabel:_oLblListadoCity withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
    [CBREFont setFontInLabel:_oLblListadoLocales withType:CBREFontTypeHelveticaNeue57Condensed];
    _oLblListadoLocales.text =[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.locales"].uppercaseString;
    [_oTblBuildings setBackgroundColor:[UIColor clearColor]];
    [super loadMenuButtonsBack:self andIdMenu:2];
    [self initScreen];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIColor *color = [UIColor whiteColor];
    _oTxtSearh.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.map.placeholder"] attributes:@{NSForegroundColorAttributeName: color}];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _oTxtSearh.leftView = paddingView;
    _oTxtSearh.leftViewMode = UITextFieldViewModeAlways;
}

- (void) initScreen{
    [self loadMapView];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName]];
    [self paintOverlayAreaSelected];
}

- (void) reloadScreen{
    _oFilterMap.nSector = _oSector.nIdSector;
    [self loadAnnotationsAreas];
    [self updateComboLabels];
    [self getSectionsBuildings];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName]];
}

- (void)getSectionsBuildings{
    NSMutableArray *aSections = [[NSMutableArray alloc] init];
    _aBuildginsSections = [[NSMutableDictionary alloc] init];
    
    long nLoopBuildings = 0, nCountBuildings = _aBuildings.count;
    BuildingBean *oBuilding;
    for ( ; nLoopBuildings < nCountBuildings ; nLoopBuildings++) {
        oBuilding = [_aBuildings objectAtIndex:nLoopBuildings];
        NSString *sSectionName = [oBuilding.sName substringToIndex:1].uppercaseString;
        
        NSMutableArray<ClientBean> *aBuildginsSections = NULL;
        if ([aSections containsObject:sSectionName]) {
            aBuildginsSections = (NSMutableArray<ClientBean> *)[_aBuildginsSections objectForKey:sSectionName];
        }else{
            aBuildginsSections = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] init];
            [aSections addObject:sSectionName];
        }
        
        [aBuildginsSections addObject:oBuilding];
        [_aBuildginsSections setObject:aBuildginsSections forKey:sSectionName];
    }
    _aSections = [aSections sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [_oTblBuildings reloadData];
}

- (void)updateComboLabels{
    [_oBtnShowCities setText:_oCity.sName];
    [_oBtnShowAreas setText:_oArea.sName];
    [_oLblSectorName setText:_oSector.sName];
    if (_oSector == NULL) {
        [_oLblSectorName setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.sector.type.todos"].uppercaseString];
    }else{
        [_oLblSectorName setText:_oSector.sName];
    }
    [_oLblListadoCity setText:_oCity.sName.uppercaseString];
}

- (IBAction)showSectors:(id)sender{
    if (_oSectorController == NULL) {
        _oSectorController = [self.storyboard instantiateViewControllerWithIdentifier:@"SectorPopoverViewController"];
        _oSectorController.delegate = self;
        _oSectorController.oSector = _oSector;
    }
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentSector.frame];
    if (_oSectorPopoverController == NULL) {
        _oSectorPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oSectorController];
        _oSectorPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oSectorPopoverController.isPopoverVisible) {
            [_oSectorPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}
- (void)hidePopoverAndSelectedSector:(SectorBean *) oSector{
    if (_oSectorPopoverController != NULL) {
        [_oSectorPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oSector.nIdSector isEqualToNumber:oSector.nIdSector]) {
        _oSector = oSector;
        [self reloadScreen];
    }
}

- (void)loadMapView{
    
    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:CLLocationCoordinate2DMake(_oArea.nLatitude.floatValue, _oArea.nLongitude.floatValue) andZoomLevel:_oArea.nZoom]];
            
            [self loadAnnotationsAreas];
            [self updateComboLabels];
            [self getSectionsBuildings];
  
            [_oActivityIndicator stopAnimating];
            [_oActivityIndicator setHidden:TRUE];
        });
        
    });
}

- (void)loadAnnotationsAreas{
    [self loadBuildingAnnotations:[_oArea getBuildingsByFilterMap:_oFilterMap]];
}

- (void)loadBuildingAnnotations:(NSArray<BuildingBean> *) aBuildings{
    [CBREMapUtils removeAllAnnotations:_oMap];
    
    _aBuildings = aBuildings;
    long nLoopBuildings = 0, nCountBuildings = aBuildings.count;
    BuildingBean *oBuilding = NULL;
    for (; nLoopBuildings < nCountBuildings; nLoopBuildings++) {
        oBuilding = [aBuildings objectAtIndex:nLoopBuildings];
        BuildingAnnotation *oAnnotation = [[BuildingAnnotation alloc] initWithBuilding:oBuilding sPin:_oArea.sPin];
        [_oMap addAnnotation:oAnnotation];
    }
    _aBuildingsFilter = (NSMutableArray<BuildingBean> *)[[NSMutableArray alloc] initWithArray:aBuildings];
    [_oTblBuildingMini reloadData];
    [_oTxtSearh setText:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showCities:(id)sender {
    if (_oCitiesController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oCitiesController = [oStoryBoard instantiateViewControllerWithIdentifier:@"CitiesPopoverViewController"];
        _oCitiesController.delegate = self;
        _oCitiesController.oCity = _oCity;
    }
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentCity.frame];
    if (_oCityPopoverController == NULL) {
        _oCityPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oCitiesController];
        _oCityPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oCityPopoverController.isPopoverVisible) {
            [_oCityPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)hidePopoverAndSelectedCity:(CityBean *)oCity{
    if (_oCityPopoverController != NULL) {
        [_oCityPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oCity.nIdCity isEqualToNumber:oCity.nIdCity]) {
        _oCity = oCity;
        [_oAreasController reloadAreasByCity:_oCity];
        [self loadMapView];
        [self reloadScreen];
    }
}

- (IBAction)showAreas:(id)sender {
    [self loadAreaControllerPopover];
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentArea.frame];
    if (_oAreasPopoverController == NULL) {
        _oAreasPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oAreasController];
        _oAreasPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oAreasPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oAreasPopoverController.isPopoverVisible) {
            [_oAreasPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oAreasPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)loadAreaControllerPopover{
    if (_oAreasController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oAreasController = [oStoryBoard instantiateViewControllerWithIdentifier:@"AreasPopoverViewController"];
        _oAreasController.delegate = self;
        _oAreasController.oCity = _oCity;
        _oAreasController.oArea = _oArea;
    }
}

- (void)hidePopoverAndSelectedArea:(AreaBean *)oArea{
    if (_oAreasPopoverController != NULL) {
        [_oAreasPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oArea.nIdArea isEqualToNumber:oArea.nIdArea]) {
        _oArea = oArea;
        [self paintOverlayAreaSelected];
        [self reloadScreen];
    }
}

- (void)paintOverlayAreaSelected{
    [_oMap removeOverlays:_oMap.overlays];
    NSArray<AreaPointBean> *aAreaPoints = [_oArea getAreaPoints];
    long nLoopPoints = 0, nCountPoints = (long)aAreaPoints.count;
    CLLocationCoordinate2D aAreaPointsCoords[nCountPoints];
    AreaPointBean *oAreaPoint = NULL;
    for (; nLoopPoints < nCountPoints; nLoopPoints++) {
        oAreaPoint = [aAreaPoints objectAtIndex:nLoopPoints];
        aAreaPointsCoords[nLoopPoints] = CLLocationCoordinate2DMake(oAreaPoint.nLatitude.floatValue, oAreaPoint.nLongitude.floatValue);
    }
    MKPolygon *oDrawArea = [MKPolygon polygonWithCoordinates:aAreaPointsCoords count:nCountPoints];
    [oDrawArea setTitle:_oArea.sName];
    oDrawArea.subtitle = _oArea.sColourArea;
    [_oMap addOverlay:oDrawArea];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoDetailBuilding" isEqualToString:segue.identifier]) {
        BuildinDetailViewController *oBuildinDetailViewController = (BuildinDetailViewController *)segue.destinationViewController;
        oBuildinDetailViewController.oBuilding = _oBuildingSelected;
        oBuildinDetailViewController.oCity = _oCity;
        oBuildinDetailViewController.oArea = _oArea;
    }
}

- (IBAction)showFilterMap:(id)sender{
    [self loadFilterMapControllerPopover];
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oBtnShowFilterMap.frame];
    if (_oFilterMapPopoverController == NULL) {
        _oFilterMapPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oFilterMapController];
        _oFilterMapPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oFilterMapPopoverController.isPopoverVisible) {
            [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)loadFilterMapControllerPopover{
    if (_oFilterMapController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oFilterMapController = [oStoryBoard instantiateViewControllerWithIdentifier:@"FilterMapPopoverViewController"];
        _oFilterMapController.delegate = self;
        _oFilterMapController.oFilterMap = _oFilterMap;
    }
}

- (void)hidePopoverAndFilterMap:(FilterMapForm *)oFilter{
    _oFilterMap = oFilter;
    if (_oFilterMapPopoverController != NULL) {
        [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
    }
    [self reloadScreen];
}

#pragma mark rmmapbox delegate functions

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(BuildingAnnotation *)annotation fillMKAnnotationView:oPinView];
    return oPinView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_on"]];
    long nLoopBuilding = 0, nCountBuilding = (long)_aBuildings.count;
    BOOL bBuildingNotFound = TRUE;
    BuildingBean *oBuilding = NULL;
    for (; nLoopBuilding < nCountBuilding && bBuildingNotFound; nLoopBuilding++) {
        oBuilding = [_aBuildings objectAtIndex:nLoopBuilding];
        if (oBuilding.nIdBuilding.intValue == view.tag)
            bBuildingNotFound = FALSE;
    }
    _oBuildingSelected = oBuilding;
    CGPoint oAnnotationPoint = view.layer.frame.origin;
    CGSize oSiceImage = view.layer.frame.size;
    oAnnotationPoint.x += (oSiceImage.width/2);
    oAnnotationPoint.y += 60 + (oSiceImage.height/2);
    [self loadAndShowPopoverBuilding:oBuilding andFrame:oAnnotationPoint];
}


- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_off"]];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if([overlay isKindOfClass:[MKPolygon class]]){
        MKPolygon *oPolygon = (MKPolygon *)overlay;
        MKPolygonRenderer *oPolygonRenderer = [[MKPolygonRenderer alloc] initWithPolygon:oPolygon];
        oPolygonRenderer.lineWidth=1;
        UIColor *oColourArea = [[UIColor alloc] initWhitHexadecimalColor:oPolygon.subtitle andAlpha:1];
        oPolygonRenderer.strokeColor= oColourArea;
        oPolygonRenderer.fillColor=[oColourArea colorWithAlphaComponent:0.5];
        return oPolygonRenderer;
    }
    return nil;
}

- (void)loadBuildingControllerPopover{
    if (_oBuildingController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oBuildingController = [oStoryBoard instantiateViewControllerWithIdentifier:@"BuildingPopoverViewController"];
        _oBuildingController.delegate = self;
    }
}

- (void)loadAndShowPopoverBuilding:(BuildingBean *) oBuilding andFrame:(CGPoint) oPosition{
    [self loadBuildingControllerPopover];
    [_oBuildingController loadBuildingView:oBuilding];
    if (_oBuildingPopoverController == NULL) {
        _oBuildingPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oBuildingController];
        _oBuildingPopoverController.popoverBackgroundViewClass = [CBREPopoverNotBackgroundView class];
        [_oBuildingPopoverController presentPopoverFromRect:CGRectMake(oPosition.x, oPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
        
    }else{
        if (_oBuildingPopoverController.isPopoverVisible) {
            [_oBuildingPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oBuildingPopoverController presentPopoverFromRect:CGRectMake(oPosition.x, oPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
        }
    }
}

#pragma mark building controller view delegate

- (void)closePopoverDetailBuilding{
    [self hidePopoverDetailBuilding];
}

- (void)closePopoverAndSendEmail{
    [self hidePopoverDetailBuilding];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        // Creamos el objeto mail apuntando a la clase MFMailComposeViewController.
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        [mail setSubject:_oBuildingSelected.sName];                                       // Asunto del correo.
        [mail setMessageBody: _oBuildingSelected.sDescription isHTML:NO];    // Mensaje (Puede pasarse código HTML)
        [mail setToRecipients:@[ _oBuildingSelected.sEmail ]];                   // Array con el listado de destinatarios.
        
        mail.mailComposeDelegate = self;
        [self presentViewController:mail animated:YES completion:NULL];
    }
}

#pragma mark - MFMailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Correo enviado correctamente.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Envío de correo fallido: %@", error.description);
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Envío de correo cancelado.");
            break;
        default:
            break;
    }
    
    // Es nuestro deber descartar el controlador.
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)closePopoverAndShowBuildingPhotos{
    [self hidePopoverDetailBuilding];
    AlbumCollectionForm *oAlbumForm = [[AlbumCollectionForm alloc] init];
    oAlbumForm.aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] initWithArray:[_oBuildingSelected getPhotos]];
    AlbumPhotoViewController *oAlbumView = [CBREUtils obtainAlbumPhotoViewControlerWithDocuments:oAlbumForm oStoryBoard:self.storyboard sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"] nIdMenu:2];
    [self.navigationController pushViewController:oAlbumView animated:true];
}

- (void)closePopoverAndShowDetailBuilding{
    [self hidePopoverDetailBuilding];
    [self performSegueWithIdentifier:@"gotoDetailBuilding" sender:self];
}

- (void)hidePopoverDetailBuilding{
    if (_oBuildingPopoverController != NULL) {
        [_oBuildingPopoverController dismissPopoverAnimated:TRUE];
    }
    NSArray *aAnnotationsSelected = _oMap.selectedAnnotations;
    [_oMap deselectAnnotation:[aAnnotationsSelected objectAtIndex:0] animated:TRUE];
}

- (IBAction)showOrHideSearch:(id)sender{
    [_oCtnTableSearch setHidden:!_oCtnTableSearch.hidden];
    [_oTxtSearh setHidden:!_oTxtSearh.hidden];
    [_oTblBuildingMini setHidden:!_oTblBuildingMini.hidden];
    [_oTxtSearh resignFirstResponder];
}


#pragma mark - Table view delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([_oTblBuildings isEqual:tableView]) {
        return(int) _aSections.count;
    }
    return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if ([_oTblBuildings isEqual:tableView]) {
        return [_aSections objectAtIndex:section];
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    if ([_oTblBuildings isEqual:tableView]) {
        NSString *sSection = [_aSections objectAtIndex:section];
    
        UILabel *oLblSection = [[UILabel alloc] initWithFrame:CGRectMake(280, 0, 40, 30)];
        [oLblSection setText:sSection];
        [oLblSection setTextColor:[UIColor whiteColor]];
        [oLblSection setTextAlignment:NSTextAlignmentCenter];
        [oLblSection setFont:[UIFont fontWithName:oLblSection.font.fontName size:24.0]];
        [CBREFont setFontInLabel:oLblSection withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
        UIImageView *oImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"barraSectionMap"]];
    
        [view addSubview:oImageView];
        [view addSubview:oLblSection];
    }

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_oTblBuildings isEqual:tableView]) {
        NSString *sSection = [_aSections objectAtIndex:section];
        NSArray<BuildingBean> *aBuildings = [_aBuildginsSections objectForKey:sSection];
        return aBuildings.count;
    }
    return _aBuildingsFilter.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_oTblBuildings isEqual:tableView]) {
        NSString *sCellIdentifier = @"BuildingCell";
        BuildingCell *oBuildingCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        
        NSString *sSection = [_aSections objectAtIndex:indexPath.section];
        NSMutableArray<BuildingBean> *aBuildings = [_aBuildginsSections objectForKey:sSection];
        BuildingBean *oBuilding = [aBuildings objectAtIndex:indexPath.row];
        oBuildingCell.oBuilding = oBuilding;
        [oBuildingCell.oLblName setText:oBuilding.sName];
        [oBuildingCell.oLblAddress setText:oBuilding.sAddress];
        [oBuildingCell.oLblPhone setText:oBuilding.sPhone];
        [oBuildingCell.oLblEmail setText:oBuilding.sEmail];

        NSURL *oURLImage = [NSURL URLWithString:oBuilding.sImgMain];
        [oBuildingCell.oImgBuilding setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oBuildingCell.oImgBuilding.image andInsertIntUIImageView:oBuildingCell.oImgBuilding];
        
        [oBuildingCell.oBntShowMap addTarget:self action:@selector(showMapAndShowCityInMap:) forControlEvents:UIControlEventTouchDown];
        [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oBuildingCell.oLblName, oBuildingCell.oLblAddress, oBuildingCell.oLblEmail, oBuildingCell.oLblPhone, oBuildingCell.oLblTitleMail, oBuildingCell.oLblTitlePhone, nil] withType:CBREFontTypeHelveticaRegular];
        
        return oBuildingCell;
    }else{
        NSString *sCellIdentifier = @"BuildingMiniCell";
        BuildingMiniCell *oBuildingMiniCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        BuildingBean *oBuilding = [_aBuildingsFilter objectAtIndex:indexPath.row];
        oBuildingMiniCell.oBuilding = oBuilding;
        
        NSURL *oURLImage = [NSURL URLWithString:oBuilding.sImgMain];
        [oBuildingMiniCell.oImgCity setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oBuildingMiniCell.oImgCity.image andInsertIntUIImageView:oBuildingMiniCell.oImgCity];
        
        [oBuildingMiniCell.oLblName setText:oBuilding.sName];
        [oBuildingMiniCell.oLblAddress setText:oBuilding.sAddress];
        [oBuildingMiniCell.oLblPhone setText:oBuilding.sPhone];
        [oBuildingMiniCell.oLblMail setText:oBuilding.sEmail];
        [oBuildingMiniCell setBackgroundColor:[UIColor clearColor]];
        [CBREFont setFontInLabel:oBuildingMiniCell.oLblName withType:CBREFontTypeHelveticaBold];
        [CBREFont setFontInLabels:[NSArray arrayWithObjects:oBuildingMiniCell.oLblAddress, oBuildingMiniCell.oLblTitlePhone, oBuildingMiniCell.oLblPhone, oBuildingMiniCell.oLblMail, nil] withType:CBREFontTypeHelveticaRegular];
        return oBuildingMiniCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_oTblBuildings isEqual:tableView]) {
        BuildingCell *oBuildingCell = (BuildingCell *)[tableView cellForRowAtIndexPath:indexPath];
        _oBuildingSelected = oBuildingCell.oBuilding;
        [self performSegueWithIdentifier:@"gotoDetailBuilding" sender:self];
    }else{
        BuildingMiniCell *oBuildingMiniCell = (BuildingMiniCell *)[tableView cellForRowAtIndexPath:indexPath];
        BuildingBean *oBuilding = oBuildingMiniCell.oBuilding;
        CLLocationCoordinate2D oPositionAnnotation = CLLocationCoordinate2DMake(oBuilding.nLatitude.floatValue, oBuilding.nLongitude.floatValue);
        [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:oPositionAnnotation andZoomLevel:[NSNumber numberWithInt:13]]];
        [self moveScroolToBuildingInTableViewList:oBuilding];
    }
}

- (void)moveScroolToBuildingInTableViewList:(BuildingBean *) oBuilding{
    BOOL bBuildingFound = FALSE;
    long nLoopSections = 0, nCountSections = _aSections.count;
    for (; nLoopSections < nCountSections && !bBuildingFound; nLoopSections++) {
        NSString *sSection = [_aSections objectAtIndex:nLoopSections];
         NSMutableArray<BuildingBean> *aBuildings = [_aBuildginsSections objectForKey:sSection];
        
        long nLoopBuildings = 0, nCountBuildings = aBuildings.count;
        BuildingBean *oBuildingFound = NULL;
        for ( ; nLoopBuildings < nCountBuildings && !bBuildingFound; nLoopBuildings++) {
            oBuildingFound = [aBuildings objectAtIndex:nLoopBuildings];
            if (oBuilding.nIdBuilding.intValue == oBuildingFound.nIdBuilding.intValue) {
                bBuildingFound = TRUE;
                [_oTblBuildings scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:nLoopBuildings inSection:nLoopSections] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
            }
        }
    }
}

- (IBAction)showMapAndShowCityInMap:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    BuildingCell *oBuildingCell = (BuildingCell *)oButton.superview.superview;
    BuildingBean *oBuilding = oBuildingCell.oBuilding;
    
    CLLocationCoordinate2D oPositionAnnotation = CLLocationCoordinate2DMake(oBuilding.nLatitude.floatValue, oBuilding.nLongitude.floatValue);
    [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:oPositionAnnotation andZoomLevel:[NSNumber numberWithInt:13]]];

    _oListButton.tag = 1;
    [_oTblBuildings setHidden:TRUE];
    [_oMap setHidden:FALSE];
    UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_oListButton setImage:image];
    [_oLblListadoLocales setHidden:TRUE];
    [_oLblListadoCity setHidden:TRUE];
}

- (IBAction)showListClientsShowsInMap:(id)sender {
    UIBarButtonItem *oButton = (UIBarButtonItem *)sender;
    if (oButton.tag == 1) {
        [_oTblBuildings setHidden:FALSE];
        [_oLblListadoLocales setHidden:FALSE];
        [_oLblListadoCity setHidden:FALSE];
        [_oMap setHidden:TRUE];
        oButton.tag = 2;
        UIImage *image = [[UIImage imageNamed:@"btnNavShowMap"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [oButton setImage: image];
    }else{
        [_oTblBuildings setHidden:TRUE];
        [_oLblListadoLocales setHidden:TRUE];
        [_oLblListadoCity setHidden:TRUE];
        [_oMap setHidden:FALSE];
        oButton.tag = 1;
        UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [oButton setImage:image];
    }
}

#pragma mark text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *sTextValue = [NSString stringWithString:textField.text];
    sTextValue = [sTextValue
                  stringByReplacingCharactersInRange:range withString:string].uppercaseString;
    
    long nLoopBuilding = 0, nCountBuildings = _aBuildings.count;
    BuildingBean *oBuilding = NULL;
    _aBuildingsFilter = (NSMutableArray<BuildingBean>*)[[NSMutableArray alloc] init];
    for ( ; nLoopBuilding < nCountBuildings ; nLoopBuilding++) {
        oBuilding = [_aBuildings objectAtIndex:nLoopBuilding];
        int nLength = (int)[oBuilding.sName.uppercaseString rangeOfString:sTextValue].length;
        if (nLength > 0 || [@"" isEqualToString:sTextValue]) {
            [_aBuildingsFilter addObject:oBuilding];
        }
    }
    
    [_oTblBuildingMini reloadData];
    return true;
}

@end
