//
//  SpanishMapViewController.m
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SpanishMapViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "CityMapViewController.h"
#import "CBREMapUtils.h"
#import "CityAnnotation.h"

@interface SpanishMapViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property (strong, nonatomic) CityBean *oCitySelected;

@end

@implementation SpanishMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    
    [self initMapAndFillWithCities];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"]]];
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSArray *aAnnotations = _oMapView.selectedAnnotations;
    if (aAnnotations.count > 0)
        [_oMapView deselectAnnotation:[aAnnotations objectAtIndex:0] animated:false];
}

- (void)initMapAndFillWithCities{
    [_oMapView setRegion:[CBREMapUtils makeRegionSpanishCoordinate]];
    [_oMapView setScrollEnabled:FALSE];
    [_oMapView setZoomEnabled:FALSE];
    [self fillMapWithCities];
}

- (void)fillMapWithCities{
    long nLoopCities = 0, nCountCities = (long)_aCities.count;
    CityBean *oCity = NULL;
    for (; nLoopCities < nCountCities ; nLoopCities++) {
        oCity = [_aCities objectAtIndex:nLoopCities];
        if(!oCity.bFlgOnlyCases)
            [_oMapView addAnnotation:[[CityAnnotation alloc] initWithCity:oCity]];
    }
}

#pragma mark prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoAreaCity" isEqualToString:segue.identifier]) {
        CityMapViewController *oCityMapView = (CityMapViewController *)segue.destinationViewController;
        oCityMapView.oCity = _oCitySelected;
    }
}

#pragma mark mapkit delegate functions

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(CityAnnotation *)annotation fillMKAnnotationView:oPinView];
    oPinView.centerOffset = CGPointMake(-1.8, -11.8);
    return oPinView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_on"]];
    long nLoopCity = 0, nCountCity = (long)_aCities.count;
    BOOL bCityNotFound = TRUE;
    CityBean *oCity = NULL;
    for (; nLoopCity < nCountCity && bCityNotFound; nLoopCity++) {
        oCity = [_aCities objectAtIndex:nLoopCity];
        if (oCity.nIdCity.intValue == view.tag)
            bCityNotFound = FALSE;
    }
    _oCitySelected = oCity;
    [self performSegueWithIdentifier:@"gotoAreaCity" sender:self];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_off"]];
}

@end
