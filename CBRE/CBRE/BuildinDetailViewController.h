//
//  BuildinDetailViewController.h
//  CBRE
//
//  Created by Victor Hugo Aliaga on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

//#import "ViewController.h"

#import <MessageUI/MessageUI.h>
#import "PhotoBean.h"
#import "PlantBean.h"
#import "CommunicationBean.h"
#import "AreaBean.h"
#import "CityBean.h"
@class BuildingBean;

@interface BuildinDetailViewController : CBRENavigationViewController<MFMailComposeViewControllerDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) AreaBean *oArea;
@property (strong, nonatomic) BuildingBean *oBuilding;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) NSArray<PhotoBean> *aPhotos;
@property (strong, nonatomic) NSArray<PlantBean> *aPlants;
@property (strong, nonatomic) NSArray<CommunicationBean> *aCommunications;

@property (weak, nonatomic) IBOutlet UICollectionView *oCblPhotos;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollBackground;
@property (weak, nonatomic) IBOutlet UIWebView *oWebDescription;
@property (weak, nonatomic) IBOutlet UITableView *oTblPlants;
@property (weak, nonatomic) IBOutlet UITableView *oTblCommunications;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblImages;
@property (weak, nonatomic) IBOutlet UILabel *lblLocalization;
@property (weak, nonatomic) IBOutlet UILabel *lblPlane;
@property (weak, nonatomic) IBOutlet UILabel *lblComunication;
@property (weak, nonatomic) IBOutlet UILabel *lblSurface;

@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;

- (IBAction)showPlaneBuilding:(id)sender;
- (IBAction)sendMailBuilding:(id)sender;
- (IBAction)showPdfCaseComplete:(id)sender;
- (IBAction)showAlbum:(id)sender;
- (IBAction)showBuildingInsideMap:(id)sender;


@end
