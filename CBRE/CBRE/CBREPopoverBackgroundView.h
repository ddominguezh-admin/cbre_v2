//
//  CBREPopoverBackgroundView.h
//  CBRE
//
//  Created by ddominguezh on 16/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBREPopoverBackgroundView : UIPopoverBackgroundView{
    
    UIImageView *_borderImageView;
    UIImageView *_arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;

}

@end
