//
//  BuildingDetailMapViewController.m
//  CBRE
//
//  Created by ddominguezh on 27/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingDetailMapViewController.h"
#import "CityBean.h"
#import "AreaBean.h"
#import "BuildingAnnotation.h"
#import "CBREMapUtils.h"

@interface BuildingDetailMapViewController ()

@end

@implementation BuildingDetailMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName, _oBuilding.sName]];
 
    [super loadMenuButtonsBack:self andIdMenu:2];
    [self loadMapView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadMapView{
    
    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CLLocationCoordinate2D oPositionAnnotation = CLLocationCoordinate2DMake(_oBuilding.nLatitude.floatValue, _oBuilding.nLongitude.floatValue);
            [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:oPositionAnnotation andZoomLevel:[NSNumber numberWithInt:15]]];
            
            BuildingAnnotation *oAnnotation = [[BuildingAnnotation alloc] initWithBuilding:_oBuilding sPin:_oArea.sPin];
            [_oMap addAnnotation:oAnnotation];
            
            [_oActivityIndicator stopAnimating];
            [_oActivityIndicator setHidden:TRUE];
        });
        
    });
}

#pragma mark mapkit delegate functions
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(BuildingAnnotation *)annotation fillMKAnnotationView:oPinView];
    return oPinView;
}

@end
