//
//  CityBean.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AreaBean.h"
#import "DocumentBean.h"
@class FilterMapClientForm;
@class FilterMapForm;

@protocol CityBean;

@interface CityBean : NSObject

@property (strong, nonatomic) NSNumber *nIdCity;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSNumber *nLatitude;
@property (strong, nonatomic) NSNumber *nLongitude;
@property (strong, nonatomic) NSNumber *nZoom;
@property (nonatomic) BOOL bFlgOnlyCases;
@property (strong, nonatomic) NSArray<AreaBean> *aAreas;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;

- (id)initWhitParameters:(NSNumber *) nIdCity
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                   nZoom:(NSNumber *) nZoom
           bFlgOnlyCases:(BOOL) bFlgOnlyCases;

- (NSArray<AreaBean> *) getAreas;
- (NSArray<AreaBean> *) getAreasByFilterClientMap:(FilterMapClientForm *) oFilterMap;
- (NSArray<AreaBean> *) getAreasByFilterMap:(FilterMapForm *) oFilterMap;
- (NSArray<DocumentBean> *) getDocumentsByType:(int) nIdDocumentType;
- (DocumentBean *)getDocumentByType:(int) ndIdDocumentType;

@end
