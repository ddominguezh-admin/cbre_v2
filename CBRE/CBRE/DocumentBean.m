//
//  DocumentBean.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"

@implementation DocumentBean

- (id)initWhitParameters:(NSNumber *) nIdDocument
                   sName:(NSString *) sName
                sPathURL:(NSString *) sPathURL
                nImgType:(int) nImgType
         nIdDocumentType:(NSNumber *) nIdDocumentType{
    if (self == NULL)
        self = [super init];
    self.nIdDocument = nIdDocument;
    self.sName = sName;
    self.sPathURL = sPathURL;
    self.sExtension = [DocumentBean getExtensionByUrl:sPathURL];
    self.sImgName = [DocumentBean  getImageNameById:nImgType];
    self.nIdDocumentType = nIdDocumentType;
    self.sMimeType = [DocumentBean getMimeTypeByExtension:self.sExtension];
    return self;
}

+ (NSString *) getImageNameById:(int) nImgType{
    switch (nImgType) {
        case 1:
            return @"PDFStandar";
        case 2:
            return @"PDFCaseStudy";
        case 3:
            return @"PDF_01";
        case 4:
            return @"PDF_02";
    }
    return @"";
}

+ (NSString *)getExtensionByUrl:(NSString *) sPath{
    if ([@"" isEqualToString:sPath])
        return @"";
    NSRange oLastPoint = [sPath rangeOfString:@"." options:NSBackwardsSearch];
    return [sPath substringFromIndex:oLastPoint.location+1];
}

+ (NSString *)getMimeTypeByExtension:(NSString *) sExtension{
    NSDictionary *oMimeType = @{
                                @"pdf": @"application/pdf",
                                @"xml": @"application/xml",
                                @"txt": @"text/plain",
                                @"png": @"image/png",
                                @"jpg": @"image/jpeg",
                                @"mp4": @"video/mp4"
                                };
    return [oMimeType objectForKey:sExtension];
}
@end
