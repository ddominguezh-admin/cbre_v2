//
//  OccupantsMenuViewController.m
//  CBRE
//
//  Created by ddominguezh on 03/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "OccupantsMenuViewController.h"
#import "DocumentDao.h"
#import "MenuCollectionForm.h"
#import "MenuCollection.h"

@interface OccupantsMenuViewController ()

@property float nLastPositionX;
@property BOOL isMoveIzqDer;
@end

@implementation OccupantsMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _nLastPositionX = 0;
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"]]];
    [super loadMenuButtonsBack:self andIdMenu:1];
    [self initScreen];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self calcualtePositionScrollByButtonTagPressed:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initScreen{
    _oScrollContent.delegate = self;
    [_oScrollContent setContentSize:CGSizeMake(4450, 0)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [_oLblOcupants setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOccupants.occupants"]];
    [CBREFont setFontInLabel:_oLblOcupants withType:CBREFontTypeHelveticaNeue57Condensed];
    [self initOcupantes];
    [self initNegociacion];
    [self initProyecto];
    [self initSeguimiento];
    [self initInformacion];
}

- (void)initOcupantes{
    [_oLblBusquedaYAnalisis setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.busqueda.y.analisis"]];
    [_oLblBusquedaAlternativas setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.busqueda.de.alternativas"]];
    [_oLblAnalisisEspacio setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.analisis.espacio"]];
    [_oLblEstudiosConsumo setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.estudios.de.construccion"]];
    [_oLblAnalisisEscenarios setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.analisis.de.escenarios"]];
    [_oLblEvaluacionTecnica setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.evaluacion.tecnica"]];

    [CBREFont setFontInLabels:[NSArray arrayWithObjects:_oLblBusquedaYAnalisis, _oLblBusquedaAlternativas, _oLblAnalisisEspacio, _oLblEstudiosConsumo, _oLblAnalisisEscenarios, _oLblEvaluacionTecnica, _oLblCaseStudies, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)initNegociacion{
    [_oLblNegociacionCierre setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.negociacion.y.cierre"]];
    [_oLblNegociacion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.negociacion"]];
    [CBREFont setFontInLabels:[NSArray arrayWithObjects:_oLblNegociacionCierre, _oLblNegociacion, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)initProyecto{
    [_oLblProyectoConstruccion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.proyecto.y.construccion"]];
    [_oLblDesarrolloProyecto setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.desarrollo.del.proyecto"]];
    [_oLblDisenioSostenible setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.disenio.sostenible"]];
    [_oLblGestionCambio setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.gestion.cambio"]];
    [_oLblProcesoLicitacion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.proceso.de.licitacion"]];
    [_oLblGestionConstruccion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.gestion.de.la.construccion"]];
    [_oLblConstruccionSostenible setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.construccion.sostenible"]];
    
    [CBREFont setFontInLabels:[NSArray arrayWithObjects:_oLblProyectoConstruccion, _oLblDesarrolloProyecto, _oLblDisenioSostenible, _oLblGestionCambio, _oLblProcesoLicitacion, _oLblConstruccionSostenible, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    
}

- (void)initSeguimiento{
    [_oLblSeguimiento setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.seguimiento"]];
    [_oLblStayVersusGo setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.stay.versus.go"]];
    [_oLblMediacionAjuste setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.mediacion.y.ajuste"]];
    [_oLblOperacionSostenible setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.operacion.sostenible"]];
    [_oLblFacilityManagement setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.management"]];
    [CBREFont setFontInLabels:[NSArray arrayWithObjects:_oLblSeguimiento, _oLblStayVersusGo, _oLblMediacionAjuste, _oLblOperacionSostenible, _oLblFacilityManagement, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)initInformacion{
    [_oLblKickOff setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.kick.off"]];
    [_oLblAprobacionBuss setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.aprobacion.bussines.plan"]];
    [_oLblFirmaContrato setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.firma.del.contrato"]];
    [_oLblInicioCompra setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.inicio.de.obra"]];
    [_oLblMudanza setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.mudanza"]];
    [_oLblRevisionCumplimiento setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.ocupantes.facility.revision.cumplimiento.objetivo"]];
    
    [CBREFont setFontInLabels:[NSArray arrayWithObjects:_oLblKickOff, _oLblAprobacionBuss, _oLblFirmaContrato, _oLblInicioCompra, _oLblMudanza, _oLblRevisionCumplimiento, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (IBAction)moveContentScroll:(id)sender {
    
    int nTag = (int)((UIButton *) sender).tag;
    [self calcualtePositionScrollByButtonTagPressed:nTag];
}

- (void)calcualtePositionScrollByButtonTagPressed:(int) nTag{
    int nX = 0;
    switch (nTag) {
        case 1:
            nX = 510;
            break;
        case 2:
            nX = 1450;
            break;
        case 3:
            nX = 2415;
            break;
        case 4:
            nX = 3400;
            break;
        default:
            break;
    }
    [_oScrollContent setContentOffset:CGPointMake(nX, 0) animated:TRUE];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
     CGPoint oPosition = scrollView.contentOffset;
     float nPosition = oPosition.x;
     int nX = 0;
     if(_isMoveIzqDer){
     
         nX = 3400;
         if(nPosition < 510){
             nX = 0;
         }else if(nPosition < 1450){
             nX = 510;
         }else if(nPosition < 2415){
             nX = 1450;
         }else if (nPosition < 3400) {
             nX = 2415;
         }
     
     }else{
     
         nX = 510;
         if(nPosition > 2415){
             nX = 3400;
         }else if(nPosition > 1450){
             nX = 2415;
         }else if(nPosition > 510){
             nX = 1450;
         }
     
     }
     [_oScrollContent setContentOffset:CGPointMake(nX, 0) animated:TRUE];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint oPosition = scrollView.contentOffset;

    oPosition.y = 0;
    if (oPosition.x < 0){
        oPosition.x = 0;
        scrollView.contentOffset = oPosition;
    }else if (oPosition.x > 3400){
        oPosition.x = 3400;
        scrollView.contentOffset = oPosition;
    }
    
    float nPosition = oPosition.x;
    _isMoveIzqDer = _nLastPositionX > nPosition;
    _nLastPositionX = nPosition;
}

- (IBAction)pushButtonGoToNewScreen:(id)sender {
    UIButton *oBtnPressed = (UIButton *)sender;
    long nTagButton = (long)oBtnPressed.tag;
    switch (nTagButton) {
        case 1:
        case 4:
        case 11:
        case 16:
            [self showScreen:nTagButton];
            break;
        default:
            [self loadDocumentsOrMenu:nTagButton];
            break;
    }
}

- (void)showScreen:(long) nTag{
    NSString *sStoryboardName = @"";
    
    switch (nTag) {
        case 1:
            sStoryboardName = @"SpanishMapViewController";
            break;
        case 4:
            sStoryboardName = @"AgencyAnalysisViewController";
            break;
        case 11:
            sStoryboardName = @"ProjectManagementGestionViewController";
            break;
        case 16:
            sStoryboardName = @"FacilityMenuViewController";
        default:
            break;
    }
    
    UIViewController *oView = [self.storyboard instantiateViewControllerWithIdentifier:sStoryboardName];
    [self.navigationController pushViewController:oView animated:TRUE];
}

-(void)loadDocumentsOrMenu:(long) nTag{
    switch (nTag) {
        case 2:
        case 3:
        case 6:
        case 8:
        case 9:
        case 12:
        case 13:
        case 14:
        case 15:
            [self loadDocuments:nTag];
            break;
        default:
            [self loadMenu:nTag];
            break;
    }
}

- (void)loadDocuments:(long) nTag{
    NSString *sTitleScreen = @"";
    int nDocumentType = 0;
    switch (nTag) {
        case 2:
            nDocumentType = DocumentOcupantesLugarDeTrabajoEstrategia;
            sTitleScreen = _oLblAnalisisEspacio.text;
            break;
        case 3:
            nDocumentType = DocumentOcupantesEnergiaConsumo;
            sTitleScreen = _oLblEstudiosConsumo.text;
            break;
        case 6:
            nDocumentType = DocumentOcupantesAgenciaNegociacion;
            sTitleScreen = _oLblNegociacion.text;
            break;
        case 8:
            nDocumentType = DocumentOcupantesEnergiaDisenio;
            sTitleScreen = _oLblDisenioSostenible.text;
            break;
        case 9:
            nDocumentType = DocumentOcupantesLugarDeTrabajoCambio;
            sTitleScreen = _oLblGestionCambio.text;
            break;
        case 12:
            nDocumentType = DocumentOcupantesEnergiaConstruccion;
            sTitleScreen = _oLblConstruccionSostenible.text;
            break;
        case 13:
            nDocumentType = DocumentOCupantesAgenciaRenegociacion;
            sTitleScreen = _oLblStayVersusGo.text;
            break;
        case 14:
            nDocumentType = DocumentOcupantesLugarDeTrabajoMediacion;
            sTitleScreen = _oLblMediacionAjuste.text;
            break;
        case 15:
            nDocumentType = DocumentOcupantesEnergiaOperacion;
            sTitleScreen = _oLblOperacionSostenible.text;
            break;
    }
    
    [self obtainPDFView:[[DocumentDao sharedInstance] getDocumentsByType:nDocumentType] sTitleScreen:sTitleScreen];
}

- (void)obtainPDFView:(NSArray<DocumentBean> *) aDocuments sTitleScreen:(NSString *) sTitleScreen{
    NSString *sSubTitleNavBar = [NSString stringWithFormat:@"%@", sTitleScreen];
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:sTitleScreen sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:sSubTitleNavBar sBackground:@"AgencyFondo" nIdMenu:2];
    
    if (oPDFView != NULL)
        [self.navigationController pushViewController:oPDFView animated:true];
}

- (void)loadMenu:(long) nTag{
    MenuCollectionForm *oMenuCollection = [[MenuCollectionForm alloc] init];
    switch (nTag) {
        case 5:
            
            oMenuCollection.sTitleScreen = _oLblEvaluacionTecnica.text;
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlDueDiligence" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.evaluacion.due"] nTypeDocument:DocumentOcupantesGestionProyectosEvaluacionDue]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlTestFits" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.evaluacion.test"] nTypeDocument:DocumentOcupantesGestionProyectosEvaluacionTest]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlInformes" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.evaluacion.informes"] nTypeDocument:DocumentOcupantesGestionProyectosEvaluacionInfores]];
            oMenuCollection.nRows = 1;
            oMenuCollection.nColumns = 3;
            break;
            
        case 7:
            
            oMenuCollection.sTitleScreen = _oLblDesarrolloProyecto.text;
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlRequerimientosClient" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.requerimientos"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionReq]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlPtoAproximado" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.presupuesto"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionPresupuesto]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlTestFitsDoc" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.test"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionTest]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlEquipo" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.equipo"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionEquipo]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlDetalladoPlanos" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.detallado"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionDetallado]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"Cvl3D" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.3d"] nTypeDocument:DocumentOcupantesGestionProyectosDocumentacion3d]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlDocumentos" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.documentos"]  nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionDocumentos]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlPresentacion" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.documentacion.presentacion"] nTypeDocument:DocumentOcupantesGestionProyectosDocumentacionPresentacion]];
            oMenuCollection.nRows = 2;
            oMenuCollection.nColumns = 4;
            break;
            
        case 10:
            
            oMenuCollection.sTitleScreen = _oLblProcesoLicitacion.text;
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlOpenbook" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.proceso.openbook"] nTypeDocument:DocumentOcupantesGestionProyectosProcesoOpenBook]];
            [oMenuCollection.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlLlaveMano" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.proceso.llave"] nTypeDocument:DocumentOcupantesGestionProyectosProcesoLlave]];
            oMenuCollection.nRows = 1;
            oMenuCollection.nColumns = 2;
            break;
            
        default:
            break;
    }
    
    [self showMenu:oMenuCollection];
    
}

- (void)showMenu:(MenuCollectionForm *) oMenuCollection{
    NSString *sSubTitleNavBar = [NSString stringWithFormat:@"%@", oMenuCollection.sTitleScreen];
    UIViewController *oMenuControler = [CBREUtils obtainMenuCollectionViewControlerWithDocuments:oMenuCollection oStoryBoard:self.storyboard sTitleScreen:oMenuCollection.sTitleScreen sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:sSubTitleNavBar sBackground:@"ProjectManagementFondo" nIdMenu:2];
    if (oMenuControler != NULL)
        [self.navigationController pushViewController:oMenuControler animated:true];
}

@end
