//
//  AreaPointBean.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaPointBean.h"

@implementation AreaPointBean

- (id)initWhitParameters:(NSNumber *) nIdPoint
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude{
    if (self == NULL)
        self = [super init];
    self.nIdPoint = nIdPoint;
    self.nLatitude = nLatitude;
    self.nLongitude = nLongitude;
    return self;
}
@end
