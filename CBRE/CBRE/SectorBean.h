//
//  SectorBean.h
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SectorBean;

@interface SectorBean : NSObject

@property (strong, nonatomic) NSNumber *nIdSector;
@property (strong, nonatomic) NSString *sName;

- (id)initWhitParameters:(NSNumber *) nIdSector
                     sName:(NSString *) sName;

@end
