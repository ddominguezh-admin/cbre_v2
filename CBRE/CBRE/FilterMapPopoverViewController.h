//
//  FilterMapPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 06/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FilterMapForm;

@protocol FilterMapPopoverViewControllerDelegate;

@interface FilterMapPopoverViewController : UIViewController

@property (strong, nonatomic) id<FilterMapPopoverViewControllerDelegate> delegate;
@property (strong, nonatomic) FilterMapForm *oFilterMap;

@property (weak, nonatomic) IBOutlet UISwitch *oSwtInmediate;
@property (weak, nonatomic) IBOutlet UISwitch *oSwtMeters;
@property (weak, nonatomic) IBOutlet UISwitch *oSwtPlants;
@property (weak, nonatomic) IBOutlet UISwitch *oSwtRent;
@property (weak, nonatomic) IBOutlet UISlider *oSldMeters;
@property (weak, nonatomic) IBOutlet UISlider *oS2dMeters;
@property (weak, nonatomic) IBOutlet UISlider *oSldPlants;
@property (weak, nonatomic) IBOutlet UISlider *oS2dPlants;
@property (weak, nonatomic) IBOutlet UISlider *oSldRent;
@property (weak, nonatomic) IBOutlet UILabel *oLblMeters;
@property (weak, nonatomic) IBOutlet UILabel *oLb2Meters;
@property (weak, nonatomic) IBOutlet UILabel *oLblPlants;
@property (weak, nonatomic) IBOutlet UILabel *oLb2Plants;
@property (weak, nonatomic) IBOutlet UILabel *oLblRent;
@property (weak, nonatomic) IBOutlet UIButton *oBtnFilter;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleInmediated;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleMeters;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitlePlants;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleRent;
@property (weak, nonatomic) IBOutlet UIButton *oBtnAlq;
@property (weak, nonatomic) IBOutlet UIButton *oBtnVent;

@property (weak, nonatomic) IBOutlet UILabel *olblM2desde;
@property (weak, nonatomic) IBOutlet UILabel *olblM2hasta;
@property (weak, nonatomic) IBOutlet UILabel *olblPlantsdesde;
@property (weak, nonatomic) IBOutlet UILabel *olblPlantsHasta;
@property (weak, nonatomic) IBOutlet UILabel *olblVenta;
@property (weak, nonatomic) IBOutlet UILabel *olblAlquiler;


@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *oLblTitles;


- (IBAction)filterMap:(id)sender;
- (IBAction)changeMeters:(id)sender;
- (IBAction)changeMeters2:(id)sender;
- (IBAction)changePlants:(id)sender;
- (IBAction)changePlants2:(id)sender;
- (IBAction)changeRent:(id)sender;
- (IBAction)enableMeters:(id)sender;
- (IBAction)enablePlants:(id)sender;
- (IBAction)enableRent:(id)sender;
- (IBAction)onClickAlq:(id)sender;
- (IBAction)onClickVent:(id)sender;

@end

@protocol FilterMapPopoverViewControllerDelegate <NSObject>

- (void)hidePopoverAndFilterMap:(FilterMapForm *) oFilter;

@end