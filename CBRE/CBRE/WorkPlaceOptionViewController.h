//
//  WorkPlaceOptionViewController.h
//  CBRE
//
//  Created by ddominguezh on 24/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "CBRENavigationViewController.h"

@interface WorkPlaceOptionViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UIButton *oBtnVideo;
@property (weak, nonatomic) IBOutlet UIButton *oBtnCases;
@property (strong, nonatomic) MPMoviePlayerController *oMoviePlayer;
@property (weak, nonatomic) IBOutlet UILabel *oLblCasesStudio;
@property (weak, nonatomic) IBOutlet UILabel *oLblWorkplace;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;

- (IBAction)showVideo:(id)sender;

@end
