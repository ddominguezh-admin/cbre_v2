//
//  ClientConverter.m
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientConverter.h"
#import "DocumentConverter.h"

@implementation ClientConverter

+ (NSArray<ClientBean> *) jsonToClients:(NSDictionary *) oData{
    NSMutableArray<ClientBean> *aClients = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopClients = 0, nCountClients = (long)oResult.count;
    NSDictionary *oDataClient = NULL;
    
    for (; nLoopClients < nCountClients; nLoopClients++) {
        oDataClient = [oResult objectAtIndex:nLoopClients];
        [aClients addObject:[ClientConverter jsonToClient:oDataClient]];
    }
    
    return aClients;
}

+ (NSArray<ClientBean> *) arrayToClients:(NSArray *) oData{
    NSMutableArray<ClientBean> *aClients = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] init];
    long nLoopClients = 0, nCountClients = (long)oData.count;
    NSDictionary *oDataClient = NULL;
    
    for (; nLoopClients < nCountClients; nLoopClients++) {
        oDataClient = [oData objectAtIndex:nLoopClients];
        [aClients addObject:[ClientConverter jsonToClient:oDataClient]];
    }
    
    return aClients;
}

+ (ClientBean *) jsonToClient:(NSDictionary *) oData{
    NSNumber *nIdClient = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    NSNumber *nLatitude = [oData objectForKey:@"latitud"];
    NSNumber *nLongitude = [oData objectForKey:@"longitud"];
    NSString *sAddress = [oData objectForKey:@"address"];
    NSString *sPhone = [oData objectForKey:@"phone"];
    NSString *sEmail = [oData objectForKey:@"mail"];
    NSString *sImgMain = [oData objectForKey:@"img_main"];
    NSString *sDescriptionHtml = [oData objectForKey:@"description_html"];
    NSString *sVideo = [oData objectForKey:@""];
    NSString *sVideoExtension = [oData objectForKey:@""];
    NSNumber *nMeters = [oData objectForKey:@"meters"];
    ClientBean *oClient = [[ClientBean alloc] initWhitParameters:nIdClient sName:sName nLatitude:nLatitude nLongitude:nLongitude sAddress:sAddress sPhone:sPhone sEmail:sEmail sImgMain:sImgMain sDescriptionHtml:sDescriptionHtml sVideo:sVideo sVideoExtension:sVideoExtension nMeters:nMeters];
    
    if ([oData objectForKey:@"documents"])
        oClient.aDocuments = [DocumentConverter arrayToDocuments:[oData objectForKey:@"documents"]];
    return oClient;
}

@end
