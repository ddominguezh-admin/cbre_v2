//
//  CBRELocalized.m
//  CBRE
//
//  Created by ddominguezh on 20/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBRELocalized.h"

@implementation CBRELocalized

static CBRELocalized *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[CBRELocalized alloc] init];
    return sharedInstance;
}

- (NSString *) getMessage:(NSString *) key{
    return [self obtainLocalizedBundleAndTranslateKey:key];
}

- (NSString *)obtainLocalizedBundleAndTranslateKey:(NSString *) key{
    NSString *sLanguage = [CBRELocalized getLanguageUserDefautl];
    if (![sLanguage isEqualToString:_sLanguage]) {
        _sLanguage = sLanguage;
        _oBundle = [CBRELocalized getLocalizedBundleWithLanguage:sLanguage];
    }
    return [_oBundle localizedStringForKey:key value:@"" table:nil];
}

+ (NSString *)getLanguageUserDefautl{
    NSArray *aLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    return [aLanguages objectAtIndex:0];
}

+ (NSBundle *)getLocalizedBundleWithLanguage:(NSString *) sLanguage{
    NSString *sBundlePath = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings" inDirectory:nil forLocalization:sLanguage];
    return [[NSBundle alloc] initWithPath:[sBundlePath stringByDeletingLastPathComponent]];
}

@end
