//
//  SectorDao.h
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "SectorBean.h"

@interface SectorDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<SectorBean> *) getSectors;

@end
