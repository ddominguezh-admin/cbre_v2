//
//  PhotoBean.m
//  CBRE
//
//  Created by ddominguezh on 16/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PhotoBean.h"

@implementation PhotoBean

- (id)initWhitParameters:(NSNumber *) nIdPhoto
                 sImgURL:(NSString *) sImgURL
               sImgHDURL:(NSString *) sImgHDURL{
    if (self == NULL)
        self = [super init];
    self.nIdPhoto = nIdPhoto;
    self.sImgURL = sImgURL;
    self.sImgHDURL = sImgHDURL;
    return self;
}
@end
