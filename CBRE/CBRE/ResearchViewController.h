//
//  ResearchViewController.h
//  CBRE
//
//  Created by ddominguezh on 28/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@class CityBean;

@interface ResearchViewController : CBRENavigationViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *oTblCitiesMarketViews;
@property (weak, nonatomic) IBOutlet UITableView *oTblCitiesSnapShots;
@property (strong, nonatomic) CityBean *oCity;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UIButton *oBtnMarket;
@property (weak, nonatomic) IBOutlet UIButton *oBtnSnap;

@end
