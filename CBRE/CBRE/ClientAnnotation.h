//
//  ClientAnnotation.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
@class ClientBean;

@interface ClientAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) UIImage *oImage;
@property (strong, nonatomic) ClientBean *oClient;

- (id)initWithClient:(ClientBean *) oClient sPin:(NSString *) sPin;
- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation;

@end
