//
//  MenuBean.m
//  CBRE
//
//  Created by ddominguezh on 18/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SubMenuBean.h"
#import "MenuDao.h"
#import "UIColor+CBREHexColor.h"

@implementation SubMenuBean

- (id)initWhitParameters:(NSNumber *) nIdSubMenu
                   sName:(NSString *) sName
                  sImage:(NSString *) sImage
               nPosition:(NSNumber *) nPosition
               sNameFont:(NSString *) sNameFont
                   nPosX:(NSNumber *) nPosX
                   nPosY:(NSNumber *) nPosY
               nSizeFont:(NSNumber *) nSizeFont
                  sColor:(NSString *) sColor{
    if (self == NULL) {
        self = [super init];
    }
    self.nIdSubMenu = nIdSubMenu;
    self.sName = sName;
    self.sImage = sImage;
    self.nPosition = nPosition;
    self.sNameFont = sNameFont;
    self.nPosX = nPosX;
    self.nPosY = nPosY;
    self.nSizeFont = nSizeFont;
    self.sColor = sColor;
    return self;
}

- (UIButton *)createButtonWithSubMenu{
    
    UIImage *oImageButton = [UIImage imageNamed:_sImage];
    
    CGSize oSizeImage = [[UIImageView alloc] initWithImage:oImageButton].frame.size;
    
    UIButton *oButtonSubMenu = [[UIButton alloc] initWithFrame:CGRectMake(_nPosX.floatValue, _nPosY.floatValue, oSizeImage.width, oSizeImage.height)];
    oButtonSubMenu.tag = _nPosition.intValue;
    [oButtonSubMenu setBackgroundImage:oImageButton forState:UIControlStateNormal];
    [oButtonSubMenu setTitle:[[CBRELocalized sharedInstance] getMessage:_sName].uppercaseString forState:UIControlStateNormal];
    [oButtonSubMenu setHidden:TRUE];
    [oButtonSubMenu setTitleColor:[[UIColor alloc] initWhitHexadecimalColor:_sColor andAlpha:1.0] forState:UIControlStateNormal];
    UIFont *oFont = oButtonSubMenu.titleLabel.font;
    [oButtonSubMenu.titleLabel setFont:[UIFont fontWithName:oFont.fontName size:_nSizeFont.floatValue]];
    [CBREFont setFontInButton:oButtonSubMenu withType:[CBREFont obtainFontTypeWithText:_sNameFont]];
    
    return oButtonSubMenu;
}

@end
