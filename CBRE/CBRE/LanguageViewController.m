//
//  LanguageViewController.m
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "LanguageViewController.h"

@interface LanguageViewController ()

@end

@implementation LanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblSpanish, _oLblEnglish, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [_oLblSpanish setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.language.spanish"].uppercaseString];
    [_oLblEnglish setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.language.english"].uppercaseString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectLanguage:(id)sender {
    UIButton *oButton = (UIButton *)sender;
    if (oButton.tag == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"es", nil] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self.navigationItem.backBarButtonItem setTitle:[[CBRELocalized sharedInstance ] getMessage:@"i18n.common.button.back"]];
}
@end
