//
//  RearchMenuViewController.h
//  CBRE
//
//  Created by ddominguezh on 02/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface RearchMenuViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UILabel *oLblPeriodicalsPresentations;
@property (weak, nonatomic) IBOutlet UILabel *oLblPresentationsSubmarkets;
@property (weak, nonatomic) IBOutlet UILabel *oLblGlobalGuide;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;

- (IBAction)showGuiasGlobales:(id)sender;

@end
