//
//  CBRENavigationViewController.h
//  CBRE
//
//  Created by ddominguezh on 15/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBRENavigationViewController : UIViewController

@property (strong, nonatomic) UIControl *oBackgroundView;
@property (strong, nonatomic) NSMutableArray *aButtons;

- (void)loadMenuButtonsBack:(UIViewController *) oView andIdMenu:(int) nIdMenu;

@end
