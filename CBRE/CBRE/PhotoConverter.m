//
//  PhotoConverter.m
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PhotoConverter.h"

@implementation PhotoConverter

+ (NSArray<PhotoBean> *) jsonToPhotos:(NSDictionary *) oData{
    NSMutableArray<PhotoBean> *aPhotos = (NSMutableArray<PhotoBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopPhotos = 0, nCountPhotos = (long)oResult.count;
    NSDictionary *oDataPhoto = NULL;
    
    for (; nLoopPhotos < nCountPhotos; nLoopPhotos++) {
        oDataPhoto = [oResult objectAtIndex:nLoopPhotos];
        [aPhotos addObject:[PhotoConverter jsonToPhoto:oDataPhoto]];
    }
    
    return aPhotos;
}

+ (PhotoBean *) jsonToPhoto:(NSDictionary *) oData{
    NSNumber *nIdPhoto = [oData objectForKey:@"id"];
    NSString *sImg = [oData objectForKey:@"path"];
    NSString *sImgHD = [oData objectForKey:@"path"];
    return [[PhotoBean alloc] initWhitParameters:nIdPhoto sImgURL:sImg sImgHDURL:sImgHD];
}

@end
