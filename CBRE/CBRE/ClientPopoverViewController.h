//
//  ClientPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClientBean;

@protocol ClientPopoverViewControllerDelegate;

@interface ClientPopoverViewController : UIViewController

@property (strong, nonatomic) id<ClientPopoverViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgClient;

@property (strong, nonatomic) ClientBean *oClient;

- (void)loadClientView:(ClientBean *) oClient;
- (IBAction)closePopover:(id)sender;
- (IBAction)closePopoverAndShowPhtos:(id)sender;
- (IBAction)closePopoverAndSendEmail:(id)sender;
- (IBAction)closePopoverAndShowDetail:(id)sender;


@end

@protocol ClientPopoverViewControllerDelegate <NSObject>

- (void)closePopoverDetailClient;
- (void)closePopoverAndShowClientPhotos;
- (void)closePopoverAndSendEmail;
- (void)closePopoverAndShowDetail;

@end
