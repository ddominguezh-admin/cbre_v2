//
//  PDFDetailForm.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DocumentBean;

@interface PDFDetailForm : NSObject

@property (nonatomic) int nIdMenu;
@property (strong, nonatomic) NSString *sTitleNavBar;
@property (strong, nonatomic) NSString *sSubTitleNavBar;
@property (strong, nonatomic) NSString *sBackground;
@property (strong, nonatomic) DocumentBean *oDocument;

@end
