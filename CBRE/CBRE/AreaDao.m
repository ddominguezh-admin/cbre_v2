//
//  CBREAreaDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaDao.h"
#import "AreaConverter.h"

@implementation AreaDao

static AreaDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[AreaDao alloc] init];
    return sharedInstance;
}

- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"area/?city=%@&", nIdCity]];
        return [AreaConverter jsonToAreas:oData];
    }
    return (NSArray<AreaBean> *)[[NSArray alloc] init];
}

- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity andFilterClientForm:(FilterMapClientForm *) oFilterMap{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"client/areas_client/?city=%@&%@", nIdCity, [oFilterMap getParamsUrl]]];
        NSMutableDictionary *oContentData = [[NSMutableDictionary alloc] init];
        [oContentData setObject:oData forKey:@"results"];
        return [AreaConverter jsonToAreas:oContentData];
    }
    return (NSArray<AreaBean> *)[[NSArray alloc] init];
}

- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity andFilterForm:(FilterMapForm *) oFilterMap{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building/areas_building/?city=%@&%@", nIdCity, [oFilterMap getParamsUrl]]];
        NSMutableDictionary *oContentData = [[NSMutableDictionary alloc] init];
        [oContentData setObject:oData forKey:@"results"];
        return [AreaConverter jsonToAreas:oContentData];
    }
    return (NSArray<AreaBean> *)[[NSArray alloc] init];
}

@end
