//
//  MenuCollectionForm.m
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MenuCollectionForm.h"

@implementation MenuCollectionForm

- (id)init{
    if (self == NULL)
        self = [super init];
    self.aMenus = (NSMutableArray<MenuCollection> *)[[NSMutableArray alloc] init];
    return self;
}
@end
