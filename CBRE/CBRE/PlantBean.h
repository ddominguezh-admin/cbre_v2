//
//  PlantBean.h
//  CBRE
//
//  Created by ddominguezh on 21/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PlantBean;

@interface PlantBean : NSObject

@property (strong , nonatomic) NSString *nPlant;
@property (strong , nonatomic) NSNumber *nMeters;

- (id)initWhitParameters:(NSString *) nPlant
                 nMeters:(NSNumber *) nMeters;

@end
