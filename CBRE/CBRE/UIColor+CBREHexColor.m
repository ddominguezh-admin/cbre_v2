//
//  UIColor+CBREHexColor.m
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "UIColor+CBREHexColor.h"

@implementation UIColor (CBREHexColor)

- (id)initWhitHexadecimalColor:(NSString *) sHexColor andAlpha:(CGFloat ) nAlpha{
    
    unsigned nRgbValue = [UIColor calculateRgbValueByHexColor:sHexColor];
    
    CGFloat nRedRgb = ((nRgbValue & 0xFF0000) >> 16)/255.0;
    CGFloat nGreenRgb = ((nRgbValue & 0xFF00) >> 8)/255.0;
    CGFloat nBlueRgb = (nRgbValue & 0xFF)/255.0;
    
    return [UIColor colorWithRed:nRedRgb green:nGreenRgb blue:nBlueRgb alpha:nAlpha];
}

+ (unsigned)calculateRgbValueByHexColor:(NSString *) sHexColor{
    
    unsigned nRgbValue = 0;
    NSScanner *oScanner = [NSScanner scannerWithString:sHexColor];
    //[scanner setScanLocation:1];
    [oScanner scanHexInt:&nRgbValue];
    return nRgbValue;
}

@end
