//
//  CBRENavigationViewController.m
//  CBRE
//
//  Created by ddominguezh on 15/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBRENavigationViewController.h"
#import "SubMenuBean.h"
#import "MenuDao.h"

@interface CBRENavigationViewController ()

@end

@implementation CBRENavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem.backBarButtonItem setTitle:[[CBRELocalized sharedInstance ] getMessage:@"i18n.common.button.back"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadMenuButtonsBack:(UIViewController *) oView andIdMenu:(int) nIdMenu{
    _oBackgroundView = [self createAndInsertBackgroundView];
    
    [oView.view addSubview:_oBackgroundView];
    [self createButtonMenuView:oView.view];
    
    _aButtons = [[NSMutableArray alloc] init];
    
    NSMutableArray<SubMenuBean> *aSubMenus = [[MenuDao sharedInstance] getMenuById:nIdMenu];
    long nLoopMenu = 0, nCountMenu = aSubMenus.count;
    SubMenuBean *oSubMenu = NULL;
    for (; nLoopMenu < nCountMenu; nLoopMenu++) {
        oSubMenu = [aSubMenus objectAtIndex:nLoopMenu];
        UIButton *oButton = [oSubMenu createButtonWithSubMenu];
        [oButton addTarget:self action:@selector(executeButtonsMenuBack:) forControlEvents:UIControlEventTouchDown];
        [_aButtons addObject:oButton];
        [oView.view addSubview:oButton];
    }
}

- (UIControl *)createAndInsertBackgroundView{
    CGSize oViewSize = self.view.frame.size;
    UIControl *oBackgroundView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, oViewSize.width, oViewSize.height)];
    [oBackgroundView setBackgroundColor:[UIColor blackColor]];
    [oBackgroundView setAlpha:0.4];
    [oBackgroundView addTarget:self action:@selector(hideBakcgroundView:) forControlEvents:UIControlEventTouchDown];
    [oBackgroundView setHidden:TRUE];
    return oBackgroundView;
}

- (IBAction)hideBakcgroundView:(id)sender{
    [self hideButtonMenuView];
}

- (void)createButtonMenuView:(UIView *) oContentView{

    UIImageView *oImageView = [[UIImageView alloc] initWithFrame:CGRectMake(129, 699.5, 479, 40)];
    [oImageView setImage:[UIImage imageNamed:@"barraCBRE"]];
    [oContentView addSubview:oImageView];
    
    UIButton *oButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 622, 117, 119)];
    oButton.tag = 0;
    [oButton setBackgroundImage:[UIImage imageNamed:@"botonCBRE"] forState:UIControlStateNormal];
    [oButton addTarget:self action:@selector(executeButtonsMenuBack:) forControlEvents:UIControlEventTouchDown];
    [oContentView addSubview:oButton];
}

- (IBAction)executeButtonsMenuBack:(id)sender
{
    UIButton *oButton = (UIButton *)sender;
    if (oButton.tag == 0) {
        if ([self isShowBackgroundView]) {
            [self hideButtonMenuView];
        }else{
            [self showButtonMenuView];
        }
    }else{
        NSArray *aViews = self.navigationController.viewControllers;
        UIViewController *oMenuController = [aViews objectAtIndex:oButton.tag];
        [self.navigationController popToViewController:oMenuController animated:TRUE];
    }
}

- (BOOL)isShowBackgroundView{
    return !_oBackgroundView.hidden;
}

- (void)showButtonMenuView{
    [_oBackgroundView setHidden:FALSE];
    [self showOrHideButtons:FALSE];
}

- (void)hideButtonMenuView{
    [_oBackgroundView setHidden:TRUE];
    [self showOrHideButtons:TRUE];
}

- (void)showOrHideButtons:(BOOL) bHidden{
    long nLoopButton = 0, nCountButton = _aButtons.count;
    UIButton *oButton;
    for (; nLoopButton < nCountButton; nLoopButton++) {
        oButton = [_aButtons objectAtIndex:nLoopButton];
        [oButton setHidden:bHidden];
    }
}

@end
