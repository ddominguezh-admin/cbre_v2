//
//  ClientPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientPopoverViewController.h"
#import "ClientBean.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ClientPopoverViewController ()

@end

@implementation ClientPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadInputsWithBuilding];
    [CBREFont setFontInLabel:_oLblName withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadClientView:(ClientBean *) oClient{
    _oClient = oClient;
    [self reloadInputsWithBuilding];
}

- (void)reloadInputsWithBuilding{
    [_oLblName setText:_oClient.sName];
    
    NSURL *oURLImage = [NSURL URLWithString:_oClient.sImgMain];
    [_oImgClient setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [CBREUtils resizeImage:_oImgClient.image andInsertIntUIImageView:_oImgClient];
    
    /*
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        NSURL *oURLImage = [NSURL URLWithString:_oClient.sImgMain];
        NSData *oData = [NSData dataWithContentsOfURL:oURLImage];
        UIImage *oImgPhoto = [UIImage imageWithData:oData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
             [CBREUtils resizeImage:oImgPhoto andInsertIntUIImageView:_oImgClient];
        });
        
    });
    */
}

- (IBAction)closePopover:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverDetailClient];
}

- (IBAction)closePopoverAndShowPhtos:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndShowClientPhotos];
}

- (IBAction)closePopoverAndSendEmail:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndSendEmail];
}

- (IBAction)closePopoverAndShowDetail:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndShowDetail];
}

@end
