//
//  DocumentDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "DocumentBean.h"

@interface DocumentDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<DocumentBean> *)getDocumentsByCity:(NSNumber *) nIdCity andType:(int) nType;
- (NSArray<DocumentBean> *)getDocumentsByClient:(NSNumber *) nIdClient andType:(int) nType;
- (NSArray<DocumentBean> *)getDocumentsByBuilding:(NSNumber *) nIdBuilding andType:(int) nType;
- (NSArray<DocumentBean> *)getDocumentsByType:(int) nType;

@end
