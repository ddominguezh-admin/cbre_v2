//
//  SectorDao.m
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SectorDao.h"
#import "SectorConverter.h"

@implementation SectorDao

static SectorDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[SectorDao alloc] init];
    return sharedInstance;
}

- (NSArray<SectorBean> *) getSectors{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:@"sector?"];
        return [SectorConverter jsonToSectors:oData];
        
    }
    return (NSArray<SectorBean> *)[[NSArray alloc] init];
}

@end
