//
//  BuildingPhotoViewController.m
//  CBRE
//
//  Created by ddominguezh on 16/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AlbumPhotoViewController.h"
#import "BuildingBean.h"
#import "PhotoCollectionCell.h"
#import "AlbumCollectionForm.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface AlbumPhotoViewController ()

@property (strong, nonatomic) NSNumber *nPhotoSelected;
@property (strong, nonatomic) NSIndexPath *oSelectedIndexPath;

@end

@implementation AlbumPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _aPhotos = _oAlbumForm.aPhotos;
    
    UIImage *image = [[UIImage imageNamed:@"btnFotoNavigation"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *oEmailButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(showOrHideCollectionPhoto:)];
    self.navigationItem.rightBarButtonItem = oEmailButton;
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:_oAlbumForm.sTitleNavBar andSubtitle:_oAlbumForm.sSubTitleNavBar];
    [super loadMenuButtonsBack:self andIdMenu:_oAlbumForm.nIdMenu];
    
    [_oBtnPreviosPhoto setEnabled:FALSE];
    [_oBtnNextPhoto setEnabled:TRUE];
    
    _oScrContentImage.delegate = self;
    _oScrContentImage.minimumZoomScale = 1;
    _oScrContentImage.maximumZoomScale = 5;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    if (_aPhotos.count > 0) {
        
        NSIndexPath *nIndex = [NSIndexPath indexPathForItem:0 inSection:0];
        
        [_oCblPhotos selectItemAtIndexPath:nIndex animated:TRUE scrollPosition:UICollectionViewScrollPositionNone];
        
        [self collectionView:_oCblPhotos didSelectItemAtIndexPath:nIndex];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    _oContentImage = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark functions collection view delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"PhotoCollectionCell";
    PhotoCollectionCell *oPhotoCollectionCell = (PhotoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentificer forIndexPath:indexPath];
    
    PhotoBean *oPhoto = [_aPhotos objectAtIndex:indexPath.row];
    
    oPhotoCollectionCell.oPhoto = oPhoto;
    
    if(_oAlbumForm.isOnlinePhotos){
        
        NSURL *oURLImage = [NSURL URLWithString:oPhoto.sImgURL];
        [oPhotoCollectionCell.oImgPhoto setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@"ImgNotPhoto"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oPhotoCollectionCell.oImgPhoto.image andInsertIntUIImageView:oPhotoCollectionCell.oImgPhoto];
    }else{
        UIImage *oImgPhoto = [UIImage imageNamed:oPhoto.sImgURL];
        [CBREUtils resizeImage:oImgPhoto andInsertIntUIImageView:oPhotoCollectionCell.oImgPhoto];
    }

    if (_nPhotoSelected == NULL || _nPhotoSelected.intValue != oPhoto.nIdPhoto.intValue) {
        [oPhotoCollectionCell.oLblBackground setHidden:TRUE];
        [oPhotoCollectionCell setAlpha:0.5];
        [oPhotoCollectionCell.oImgPhoto setAlpha:0.5];
    }else{
        [oPhotoCollectionCell.oLblBackground setHidden:FALSE];
        [oPhotoCollectionCell setAlpha:1];
        [oPhotoCollectionCell.oImgPhoto setAlpha:1];
    }
    
    return oPhotoCollectionCell;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionCell *oPhotoCollectionCell = (PhotoCollectionCell *)[collectionView cellForItemAtIndexPath:_oSelectedIndexPath];
    PhotoBean *oPhoto = oPhotoCollectionCell.oPhoto;
    if (oPhoto.nIdPhoto == _nPhotoSelected){
        [oPhotoCollectionCell.oLblBackground setHidden:TRUE];
        [oPhotoCollectionCell setAlpha:0.5];
        [oPhotoCollectionCell.oImgPhoto setAlpha:0.5];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionCell *oPhotoCollectionCell = (PhotoCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    PhotoBean *oPhoto = oPhotoCollectionCell.oPhoto;
    _nPhotoSelected = oPhoto.nIdPhoto;
    [oPhotoCollectionCell.oLblBackground setHidden:FALSE];
    [oPhotoCollectionCell setAlpha:1];
    [oPhotoCollectionCell.oImgPhoto setAlpha:1];
    [self loadSelectedImageCollectionView:indexPath];
}

- (void)loadSelectedImageCollectionView:(NSIndexPath *) oSelectedIndexPath{
    PhotoBean *oPhoto = [_aPhotos objectAtIndex:oSelectedIndexPath.row];
    
    if(_oAlbumForm.isOnlinePhotos){
        
        NSURL *oURLImage = [NSURL URLWithString:oPhoto.sImgURL];
        [_oContentImage setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@"ImgNotPhoto"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:_oContentImage.image andInsertIntUIImageView:_oContentImage];
        
    }else{
        UIImage *oImgPhoto = [UIImage imageNamed:oPhoto.sImgURL];
        [CBREUtils resizeImage:oImgPhoto andInsertIntUIImageView:_oContentImage];
    }
    
    /*
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        UIImage *oImgPhoto = NULL;
        
        if(_oAlbumForm.isOnlinePhotos){
            NSURL *oURLImage = [NSURL URLWithString:oPhoto.sImgURL];
            NSData *oDataImage = [NSData dataWithContentsOfURL:oURLImage];
            oImgPhoto = [UIImage imageWithData:oDataImage];
        }else{
            oImgPhoto = [UIImage imageNamed:oPhoto.sImgURL];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [CBREUtils resizeImage:oImgPhoto andInsertIntUIImageView:_oContentImage];
        });
        
    });
    */
    _oSelectedIndexPath = oSelectedIndexPath;
    
    [_oBtnPreviosPhoto setEnabled:TRUE];
    [_oBtnNextPhoto setEnabled:TRUE];
    if (oSelectedIndexPath.row == 0) {
        [_oBtnPreviosPhoto setEnabled:FALSE];
    }
    if (oSelectedIndexPath.row == _aPhotos.count-1) {
        [_oBtnNextPhoto setEnabled:FALSE];
    }
    [_oCblPhotos scrollToItemAtIndexPath:oSelectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
}

- (IBAction)showOrHideCollectionPhoto:(id)sender{
    if (_oCblPhotos.alpha == 1) {
        _oCblPhotos.alpha = 0;
        _oLblContentCollection.alpha = 0;
        
    }else{
        _oCblPhotos.alpha = 1;
        _oLblContentCollection.alpha = 0.5;
    }
}

- (IBAction)showPreviousPhoto:(id)sender {
    NSIndexPath *oPreviousIndex = [NSIndexPath indexPathForRow:_oSelectedIndexPath.row-1 inSection:_oSelectedIndexPath.section];
    [self selectedIndexPathCollectionView:oPreviousIndex];
}

- (IBAction)showNextPhoto:(id)sender {
    NSIndexPath *oNextIndex = [NSIndexPath indexPathForRow:_oSelectedIndexPath.row+1 inSection:_oSelectedIndexPath.section];
    [self selectedIndexPathCollectionView:oNextIndex];
}

- (void)selectedIndexPathCollectionView:(NSIndexPath *) oNewSelectedIndexPath{
    [self collectionView:_oCblPhotos didDeselectItemAtIndexPath:_oSelectedIndexPath];
    [self collectionView:_oCblPhotos didSelectItemAtIndexPath:oNewSelectedIndexPath];
}

#pragma mark scroll collection view delegate

#pragma mark - UIScrollViewDelegates

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _oContentImage;
}

/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    CGSize oContentSize = scrollView.contentSize;
    CGSize oImgSize = _oImgCvlSelected.frame.size;
    
    NSLog(@"Y: %f X: %f", offset.y, offset.x);
    
    int nMaxPositionY = oContentSize.height-oImgSize.height;
    int nMaxPositionX = oContentSize.width - oImgSize.width;
    
    NSLog(@"MY: %d MX: %d", nMaxPositionY, nMaxPositionX);
    
    if (offset.y <= 0) {
        offset.y = 0;
    }else if(offset.y > nMaxPositionY){
        offset.y = nMaxPositionY;
    }
    
    if (offset.x <= 0) {
        offset.x = 0;
    }else if(offset.x > nMaxPositionX){
        offset.x = nMaxPositionX;
    }
    
    scrollView.contentOffset = offset;
}
*/
@end
