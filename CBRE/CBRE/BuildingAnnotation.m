//
//  BuildingAnnotation.m
//  CBRE
//
//  Created by ddominguezh on 10/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingAnnotation.h"
#import "BuildingBean.h"

@implementation BuildingAnnotation

- (id)initWithBuilding:(BuildingBean *) oBuilding sPin:(NSString *) sPin{
    if (self == NULL)
        self = [super init];
    self.coordinate = CLLocationCoordinate2DMake(oBuilding.nLatitude.floatValue, oBuilding.nLongitude.floatValue);
    self.oBuilding = oBuilding;
    self.oImage = [UIImage imageNamed:@"pin_off"];
    return self;
}

- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation{
    oPinAnnotation.image = self.oImage;
    oPinAnnotation.enabled = YES;
    oPinAnnotation.canShowCallout = NO;
    oPinAnnotation.tag = self.oBuilding.nIdBuilding.intValue;
}


@end
