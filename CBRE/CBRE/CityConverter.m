//
//  CityConverter.m
//  CBRE
//
//  Created by ddominguezh on 24/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CityConverter.h"

@implementation CityConverter

+ (NSArray<CityBean> *) jsonToCities:(NSDictionary *) oData{
    NSMutableArray<CityBean> *aCities = (NSMutableArray<CityBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopCities = 0, nCountCities = (long)oResult.count;
    NSDictionary *oDataCity = NULL;
    
    for (; nLoopCities < nCountCities; nLoopCities++) {
        oDataCity = [oResult objectAtIndex:nLoopCities];
        [aCities addObject:[CityConverter jsonToCity:oDataCity]];
    }
    
    return aCities;
}

+ (CityBean *) jsonToCity:(NSDictionary *) oData{
    NSNumber *nIdCity = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    NSNumber *nLatitud = [oData objectForKey:@"latitud"];
    NSNumber *nLongitud = [oData objectForKey:@"longitud"];
    NSNumber *nZoom = [oData objectForKey:@"zoom"];
    BOOL bFlgOnlyCases = [[oData objectForKey:@"flg_only_cases"] intValue] == 1;
    return [[CityBean alloc] initWhitParameters:nIdCity sName:sName nLatitude:nLatitud nLongitude:nLongitud nZoom:nZoom bFlgOnlyCases:bFlgOnlyCases];
}

@end
