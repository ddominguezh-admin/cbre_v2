//
//  ClientPhotoViewController.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientPhotoViewController.h"
#import "ClientBean.h"
#import "DocumentDao.h"
#import "CBREPDFReader.h"
#import "CityBean.h"
#import "PhotoCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ClientPhotoViewController ()

@end

@implementation ClientPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _aPhotos = [_oClient getPhotos];
    _aDocuments = [[DocumentDao sharedInstance] getDocumentsByClient:_oClient.nIdClient andType:DocumentClientCasesProjectManagement];
    
    [_oLblClient setText:_oClient.sName];
    [_oLblTitle setText:_oCity.sName];
    
    [_oWebDescription loadHTMLString:_oClient.sDescriptionHtml baseURL:nil];
    [CBREUtils hideBackgroundWebView:_oWebDescription];
    
    if([@"PWC" isEqualToString:_oClient.sName]){
        _oClient.sVideo = @"Vídeo PwC iPad";
        _oClient.sVideoExtension = @"mp4";
    }else if([@"CBRE" isEqualToString:_oClient.sName]){
        _oClient.sVideo = @"CBRE_10-julio_English";
        _oClient.sVideoExtension = @"mp4";
    }
    
    if ([@"" isEqualToString:_oClient.sVideo] || _oClient.sVideo == nil) {
        [self hideVideoClient];
    }else{
        [self showAndLoadVideoClient];
    }
    
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblCaseStudio, _oLblImages, _oLblVideo, nil] withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
    [CBREFont setFontInLabel:_oLblClient withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInLabel:_oLblClient withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue57Condensed];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName, _oClient.sName]];
    
    [super loadMenuButtonsBack:self andIdMenu:2];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)hideVideoClient{
    [_oCtnMoviePlayer setHidden:TRUE];
    [_oBtnPlayVideo setHidden: TRUE];
    [_oLblVideo setHidden:TRUE];
    
    if (_aPhotos.count >= 6) {
        _oClvConstSepatorVideo.constant = (_oClvConstSepatorVideo.constant / 2) - _oClvConstWidth.constant + 16;
        _oClvConstWidth.constant = _oClvConstWidth.constant * 2;
        _oWebConstSeparatorImage.constant += 15;
    }else{
        _oClvConstSepatorVideo.constant = (_oClvConstSepatorVideo.constant / 2) - (_oClvConstWidth.constant / 2);
    }
    
    _oBtnConstSeparatorVideo.constant = (_oBtnConstSeparatorVideo.constant / 2) - (_obtnShowImages.frame.size.width / 2);
    _oLblConstSepartorVideo.constant = (_oLblConstSepartorVideo.constant / 2) - (_oLblImages.frame.size.width / 2);
}

- (void)showAndLoadVideoClient{
    NSURL *oURLFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:_oClient.sVideo ofType:_oClient.sVideoExtension]];
    
    _oMoviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:oURLFile];
    _oMoviePlayer.controlStyle = MPMovieControlStyleDefault;
    _oMoviePlayer.shouldAutoplay = YES;
    _oMoviePlayer.view.frame = CGRectMake(3, 3, 230, 343);
    [_oCtnMoviePlayer insertSubview:_oMoviePlayer.view belowSubview:_oImgPreloadVideo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark functions collection view delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    int nPhotos = (int)_aPhotos.count;
    if (nPhotos < 6) {
        nPhotos = 6;
    }else if(nPhotos > 6){
        nPhotos = 12;
    }
    return nPhotos;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"PhotoCollectionCell";
    PhotoCollectionCell *oCollectionCell = (PhotoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentificer forIndexPath:indexPath];
    
    if (indexPath.row < _aPhotos.count) {
        
        PhotoBean *oPhoto = [_aPhotos objectAtIndex:indexPath.row];
        oCollectionCell.oPhoto = oPhoto;
        
        NSURL *oURLImage = [NSURL URLWithString:oPhoto.sImgURL];
        [oCollectionCell.oImgPhoto setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@"ImgNotPhoto"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oCollectionCell.oImgPhoto.image andInsertIntUIImageView:oCollectionCell.oImgPhoto];
        
    }else{
        [oCollectionCell.oImgPhoto setImage: [UIImage imageNamed:@"ImgNotPhoto"]];
    }
    
    return oCollectionCell;
}

- (IBAction)playVideo:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_oMoviePlayer];
    [_oMoviePlayer setFullscreen:YES animated:YES];
    [_oMoviePlayer play];
    [_oImgPreloadVideo setHidden:TRUE];
}

- (IBAction)sendMailClient:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        [mail setSubject:_oClient.sName];
        [mail setMessageBody: @"" isHTML:NO];
        [mail setToRecipients:@[ _oClient.sEmail ]];
    
        long nLoopDocuments = 0, nCountDocuments = _aDocuments.count;
        DocumentBean *oDocument = NULL;
        for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
            oDocument = [_aDocuments objectAtIndex:nLoopDocuments];
            NSData *oDataFile = [CBREPDFReader loadNSDataPdf:oDocument];
            [mail addAttachmentData:oDataFile mimeType:oDocument.sMimeType fileName:[NSString stringWithFormat:@"%@.%@", oDocument.sName, oDocument.sExtension]];
        }
        
        mail.mailComposeDelegate = self;
        [self presentViewController:mail animated:YES completion:NULL];
    }

}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *oMoviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:oMoviePlayer];
    
    if ([oMoviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        [oMoviePlayer setFullscreen:FALSE animated:YES];
}

#pragma mark - MFMailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Correo enviado correctamente.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Envío de correo fallido: %@", error.description);
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Envío de correo cancelado.");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)showPdfCaseComplete:(id)sender {
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:_aDocuments oStoryBoard:self.storyboard sTitleScreen:_oClient.sName sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName, _oClient.sName]sBackground:@"ProjectManagementFondo" nIdMenu:2];
    if (oPDFView) {
        [self.navigationController pushViewController:oPDFView animated:true];
    }
}

- (IBAction)showAlbum:(id)sender {
    AlbumCollectionForm *oAlbumForm = [[AlbumCollectionForm alloc] init];
    oAlbumForm.aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] initWithArray:[_oClient getPhotos]];
    AlbumPhotoViewController *oAlbumView = [CBREUtils obtainAlbumPhotoViewControlerWithDocuments:oAlbumForm oStoryBoard:self.storyboard sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName, _oClient.sName] nIdMenu:2];
    [self.navigationController pushViewController:oAlbumView animated:true];
}
@end
