//
//  PhotoConverter.h
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoBean.h"

@interface PhotoConverter : NSObject

+ (NSArray<PhotoBean> *) jsonToPhotos:(NSDictionary *) oData;
+ (PhotoBean *) jsonToPhoto:(NSDictionary *) oData;

@end
