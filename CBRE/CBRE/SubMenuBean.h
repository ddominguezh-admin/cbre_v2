//
//  MenuBean.h
//  CBRE
//
//  Created by ddominguezh on 18/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SubMenuBean;

@interface SubMenuBean : NSObject

@property (strong, nonatomic) NSNumber *nIdSubMenu;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSString *sImage;
@property (strong, nonatomic) NSNumber *nPosition;
@property (strong, nonatomic) NSString *sNameFont;
@property (strong, nonatomic) NSNumber *nPosX;
@property (strong, nonatomic) NSNumber *nPosY;
@property (strong, nonatomic) NSNumber *nSizeFont;
@property (strong, nonatomic) NSString *sColor;

- (id)initWhitParameters:(NSNumber *) nIdSubMenu
                   sName:(NSString *) sName
                  sImage:(NSString *) sImage
               nPosition:(NSNumber *) nPosition
               sNameFont:(NSString *) sNameFont
                   nPosX:(NSNumber *) nPosX
                   nPosY:(NSNumber *) nPosY
               nSizeFont:(NSNumber *) nSizeFont
                  sColor:(NSString *) sColor;

- (UIButton *)createButtonWithSubMenu;

@end
