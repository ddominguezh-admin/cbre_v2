//
//  ClientNameCell.h
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClientBean;

@interface ClientNameCell : UITableViewCell

@property (strong, nonatomic) ClientBean *oClient;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
