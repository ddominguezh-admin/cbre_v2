//
//  FacilityMenuViewController.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface FacilityMenuViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UIButton *oBtnCorporativo;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPymes;

- (IBAction)gotoShowPdfsCorporativo:(id)sender;
- (IBAction)gotoShowPdfsPymes:(id)sender;

@end
