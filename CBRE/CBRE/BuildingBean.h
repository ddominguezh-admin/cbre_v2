//
//  BuildingBean.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoBean.h"
#import "PlantBean.h"
#import "CommunicationBean.h"

@protocol BuildingBean;

@interface BuildingBean : NSObject

@property (strong, nonatomic) NSNumber *nIdBuilding;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSNumber *nLatitude;
@property (strong, nonatomic) NSNumber *nLongitude;
@property (strong, nonatomic) NSNumber *nMeters;
@property (strong, nonatomic) NSNumber *nPlants;
@property (strong, nonatomic) NSNumber *nRent;
@property (strong, nonatomic) NSString *sAddress;
@property (strong, nonatomic) NSString *sPhone;
@property (strong, nonatomic) NSString *sEmail;
@property (strong, nonatomic) NSString *sImgMain;
@property (strong, nonatomic) NSString *sDescription;
@property (strong, nonatomic) NSString *sDescriptionHtml;
@property (strong, nonatomic) DocumentBean *oPlane;
@property (strong, nonatomic) NSArray<PhotoBean> *aPhotos;
@property (strong, nonatomic) NSArray<PlantBean> *aPlants;
@property (strong, nonatomic) NSArray<CommunicationBean> *aCommunications;

- (id)initWhitParameters:(NSNumber *) nIdBuilding
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                 nMeters:(NSNumber *) nMeters
                 nPlants:(NSNumber *) nPlants
                   nRent:(NSNumber *) nRent
                sAddress:(NSString *) sAddress
                  sPhone:(NSString *) sPhone
                  sEmail:(NSString *) sEmail
                sImgMain:(NSString *) sImgMain
            sDescription:(NSString *) sDescription
        sDescriptionHtml:(NSString *) sDescriptionHtml;

- (NSArray<PhotoBean> *) getPhotos;
- (NSArray<PlantBean> *) getPlants;
- (NSArray<CommunicationBean> *) getCommunications;
- (NSArray<DocumentBean> *)getPlane;

@end
