//
//  ResearchPresentacionesViewController.h
//  CBRE
//
//  Created by ddominguezh on 22/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBRENavigationViewController.h"

@interface ResearchPresentacionesViewController : CBRENavigationViewController
@property (weak, nonatomic) IBOutlet UILabel *oLblBarcelona;
@property (weak, nonatomic) IBOutlet UILabel *oLblMadrid;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;


- (IBAction)showDocumentMadrid:(id)sender;
- (IBAction)showDocumentBarcelona:(id)sender;

@end
