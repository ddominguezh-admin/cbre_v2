//
//  ClientCityViewController.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "CitiesPopoverViewController.h"
#import "SectorListPopoverViewController.h"
#import "FilterMapClientPopoverViewController.h"
#import "CityBean.h"
#import "SectorBean.h"
#import "FilterMapClientForm.h"

@interface ClientCityMapViewController : CBRENavigationViewController<MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, CitiesPopoverViewControllerDelegate, FilterMapClientPopoverViewControllerDelegate, SectorListPopoverViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *oMap;
@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) SectorBean *oSector;
@property (strong, nonatomic) FilterMapClientForm *oFilterMap;

@property (strong, nonatomic) CitiesPopoverViewController *oCitiesController;
@property (strong, nonatomic) UIPopoverController *oCityPopoverController;
@property (strong, nonatomic) FilterMapClientPopoverViewController *oFilterMapController;
@property (strong, nonatomic) UIPopoverController *oFilterMapPopoverController;
@property (strong, nonatomic) SectorListPopoverViewController *oSectorController;
@property (strong, nonatomic) UIPopoverController *oSectorPopoverController;

@property (weak, nonatomic) IBOutlet UITableView *oTblAreas;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowCities;
@property (weak, nonatomic) IBOutlet UILabel *oLblSelectedCity;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentCity;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oTblConstPosY;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oTblConstHeight;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowSectors;
@property (weak, nonatomic) IBOutlet UILabel *oLblSectorName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentSector;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowFilterMap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;

- (IBAction)showCities:(id)sender;
- (IBAction)showSectors:(id)sender;
- (IBAction)showFilterMap:(id)sender;

@end
