//
//  AreasPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 05/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreasPopoverViewController.h"
#import "CityBean.h"
#import "AreaBean.h"
#import "ButtonAreaCell.h"

@interface AreasPopoverViewController ()

@property (strong, nonatomic) NSArray<AreaBean> *aAreas;
@property bool bFirstSelected;

@end

@implementation AreasPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _aAreas = [_oCity getAreas];
    _bFirstSelected = true;
}

- (void)viewDidAppear:(BOOL)animated{
    if (_bFirstSelected) {
        
        long nLoopArea = 0, nCountArea = _aAreas.count;
        BOOL bAreaFound = false;
        AreaBean *oAreaFound = NULL;
        for (; nLoopArea < nCountArea && !bAreaFound; nLoopArea++) {
            oAreaFound = [_aAreas objectAtIndex:nLoopArea];
            if (oAreaFound.nIdArea.intValue == _oArea.nIdArea.intValue)
                bAreaFound = TRUE;
        }
        
        NSIndexPath *nIndex = [NSIndexPath indexPathForItem:nLoopArea-1 inSection:0];
        [_oTblAreas selectRowAtIndexPath:nIndex animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        ButtonAreaCell *oButtonAreaCell = (ButtonAreaCell *)[_oTblAreas cellForRowAtIndexPath:nIndex];
        [oButtonAreaCell.oLblName setTextColor:[UIColor blackColor]];
        _bFirstSelected = false;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    self.view.superview.alpha = 0.85;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark table view delegate functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aAreas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"AreaMapCell";
    ButtonAreaCell *oButtonAreaCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer];
    if (oButtonAreaCell == nil) {
        oButtonAreaCell = [[ButtonAreaCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sCellIdentificer];
    }
    AreaBean *oArea = [_aAreas objectAtIndex:indexPath.row];
    oButtonAreaCell.oArea = oArea;
    [oButtonAreaCell.oLblName setText:oArea.sName];
    [oButtonAreaCell.oLblName setTextColor:[UIColor whiteColor]];
    [CBREFont setFontInLabel:oButtonAreaCell.oLblName withType:CBREFontTypeHelveticaNeue67Medium];
    return oButtonAreaCell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonAreaCell *oButtonAreaCell = (ButtonAreaCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oButtonAreaCell.oLblName setTextColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonAreaCell *oButtonAreaCell = (ButtonAreaCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oButtonAreaCell.oLblName setTextColor:[UIColor blackColor]];
    if (_delegate != NULL)
        [_delegate hidePopoverAndSelectedArea:oButtonAreaCell.oArea];
}


- (void)reloadAreasByCity:(CityBean *) oCity{
    _oCity = oCity;
    _aAreas = [oCity getAreas];
    [_oTblAreas reloadData];
    if(_aAreas.count > 0 && _delegate != NULL){
        
        NSIndexPath *nAntIndexPath = [NSIndexPath indexPathForItem:[_aAreas indexOfObject:_oArea] inSection:0];
        [self tableView:_oTblAreas didDeselectRowAtIndexPath:nAntIndexPath];

        NSIndexPath *nIndex = [NSIndexPath indexPathForItem:0 inSection:0];
        _oArea = [_aAreas objectAtIndex:0];
        [_oTblAreas selectRowAtIndexPath:nIndex animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        ButtonAreaCell *oButtonAreaCell = (ButtonAreaCell *)[_oTblAreas cellForRowAtIndexPath:nIndex];
        [oButtonAreaCell.oLblName setTextColor:[UIColor blackColor]];
        [_delegate hidePopoverAndSelectedArea:[_aAreas objectAtIndex:0]];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonAreaCell *oCell = (ButtonAreaCell *)cell;
    [oCell setBackgroundColor:[UIColor clearColor]];
    [oCell.oLblName setTextColor:[UIColor whiteColor]];
}

@end
