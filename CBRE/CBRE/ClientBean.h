//
//  ClientBean.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoBean.h"
#import "DocumentBean.h"

@protocol ClientBean;

@interface ClientBean : NSObject

@property (strong, nonatomic) NSNumber *nIdClient;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSNumber *nLatitude;
@property (strong, nonatomic) NSNumber *nLongitude;
@property (strong, nonatomic) NSString *sAddress;
@property (strong, nonatomic) NSString *sPhone;
@property (strong, nonatomic) NSString *sEmail;
@property (strong, nonatomic) NSString *sImgMain;
@property (strong, nonatomic) NSString *sDescriptionHtml;
@property (strong, nonatomic) NSString *sVideo;
@property (strong, nonatomic) NSString *sVideoExtension;
@property (strong, nonatomic) NSNumber *nMeters;
@property (strong, nonatomic) NSArray<PhotoBean> *aPhotos;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;

- (id)initWhitParameters:(NSNumber *) nIdClient
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                sAddress:(NSString *) sAddress
                  sPhone:(NSString *) sPhone
                  sEmail:(NSString *) sEmail
                sImgMain:(NSString *) sImgMain
        sDescriptionHtml:(NSString *) sDescriptionHtml
                  sVideo:(NSString *) sVideo
         sVideoExtension:(NSString *) sVideoExtension
                 nMeters:(NSNumber *) nMeters;

- (id)initWhitParameters:(NSNumber *) nIdClient
                   sName:(NSString *) sName;

- (NSArray<PhotoBean> *) getPhotos;

@end
