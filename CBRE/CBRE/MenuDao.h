//
//  MenuDao.h
//  CBRE
//
//  Created by ddominguezh on 18/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "SubMenuBean.h"

@interface MenuDao : AbstractDao

+ (instancetype)sharedInstance;

- (NSMutableArray<SubMenuBean> *)getMenuById:(int) nIdMenu;

@end
