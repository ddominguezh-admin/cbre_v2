//
//  AreaAnnotation.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
@class AreaBean;

@interface AreaAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) UIImage *oImage;
@property (strong, nonatomic) AreaBean *oArea;

- (id)initWithArea:(AreaBean *) oArea;
- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation;

@end
