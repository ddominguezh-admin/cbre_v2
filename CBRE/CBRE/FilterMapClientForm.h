//
//  FilterMapClientForm.h
//  CBRE
//
//  Created by ddominguezh on 13/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface FilterMapClientForm : NSObject

@property (nonatomic) BOOL bSWitchOne;
@property (nonatomic) BOOL bSWitchTwo;
@property (nonatomic) BOOL bSWitchThree;

@property (strong, nonatomic) NSNumber *nMetersMin;
@property (strong, nonatomic) NSNumber *nMetersMax;
@property (strong, nonatomic) NSNumber *nSector;

- (id)init;
- (NSString *)getParamsUrl;

@end
