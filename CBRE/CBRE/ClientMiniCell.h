//
//  CityMiniCell.h
//  CBRE
//
//  Created by ddominguezh on 21/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClientBean;

@interface ClientMiniCell : UITableViewCell

@property (strong, nonatomic) ClientBean *oClient;

@property (weak, nonatomic) IBOutlet UIImageView *oImgCity;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UILabel *oLblAddress;
@property (weak, nonatomic) IBOutlet UILabel *oLblPhone;
@property (weak, nonatomic) IBOutlet UILabel *oLblMail;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitlePhone;


@end
