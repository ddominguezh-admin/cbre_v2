//
//  FilterMapClientPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 13/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FilterMapClientPopoverViewController.h"
#import "FilterMapClientForm.h"

@interface FilterMapClientPopoverViewController ()

@property bool bFirstSelected;

@end

@implementation FilterMapClientPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblTitle, _oLblTitleMetersOne, _oLblTitleMetersTwo, _oLblTitleMetersThree, nil] withType:CBREFontTypeHelveticaRegular];
    _bFirstSelected = true;
}

- (void)viewDidAppear:(BOOL)animated{
    if (_bFirstSelected) {
        
        [_oSwtMetersOne setOn:_oFilterMap.bSWitchOne];
        [_oSwtMetersTwo setOn:_oFilterMap.bSWitchTwo];
        [_oSwtMetersThree setOn:_oFilterMap.bSWitchThree];
        
        _bFirstSelected = false;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    self.view.superview.alpha = 0.85;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)filterMap:(id)sender {
    FilterMapClientForm *oFilterMap = _oFilterMap;
    
    _oFilterMap.bSWitchOne = _oSwtMetersOne.on;
    _oFilterMap.bSWitchTwo = _oSwtMetersTwo.on;
    _oFilterMap.bSWitchThree = _oSwtMetersThree.on;
    
    if(_oSwtMetersOne.on){
        oFilterMap.nMetersMin = [NSNumber numberWithInt:0];
        oFilterMap.nMetersMax = [NSNumber numberWithInt:2000];
    }
    if (_oSwtMetersTwo.on) {
        oFilterMap.nMetersMax = [NSNumber numberWithInt:5000];
        if (!_oSwtMetersOne.on)
            oFilterMap.nMetersMin = [NSNumber numberWithInt:2000];
    }
    
    if (_oSwtMetersThree.on) {
        if (oFilterMap.nMetersMin == NULL)
            oFilterMap.nMetersMin = [NSNumber numberWithInt:5000];
        oFilterMap.nMetersMax = NULL;
    }
    
    if (_delegate != NULL)
        [_delegate hidePopoverAndFilterMapClient:oFilterMap];
}

- (IBAction)enableMetersOne:(id)sender{
}

- (IBAction)enableMetersTwo:(id)sender{
}

- (IBAction)enableMetersThree:(id)sender{
}

@end
