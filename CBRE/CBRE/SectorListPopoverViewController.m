//
//  SectorListPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SectorListPopoverViewController.h"
#import "SectorDao.h"
#import "SectorCell.h"

@interface SectorListPopoverViewController ()

@property bool bFirstSelected;

@end

@implementation SectorListPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _aSector = (NSMutableArray<SectorBean> *)[[NSMutableArray alloc] init];
    
    SectorBean *oSectorZero = [[SectorBean alloc] init];
    oSectorZero.nIdSector = [NSNumber numberWithInt:0];
    oSectorZero.sName = [[CBRELocalized sharedInstance] getMessage:@"i18n.sector.type.todos"].uppercaseString;
    [_aSector addObject:oSectorZero];
    [_aSector addObjectsFromArray:[[SectorDao sharedInstance] getSectors]];
    
    if (_oSector == NULL)
        _oSector = oSectorZero;
    
    _bFirstSelected = true;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    self.view.superview.alpha = 0.85;
}

- (void)viewDidAppear:(BOOL)animated{
    if (_bFirstSelected) {
        long nLoopSector = 0, nCountSector = _aSector.count;
        BOOL bSectorFound = false;
        SectorBean *oSectorFound = NULL;
        for (; nLoopSector < nCountSector && !bSectorFound; nLoopSector++) {
            oSectorFound = [_aSector objectAtIndex:nLoopSector];
            if (oSectorFound.nIdSector.intValue == _oSector.nIdSector.intValue)
                bSectorFound = TRUE;
        }
        NSIndexPath *nIndex = [NSIndexPath indexPathForItem:nLoopSector-1 inSection:0];
        [_oTblSectors selectRowAtIndexPath:nIndex animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        SectorCell *oSectorCell = (SectorCell *)[_oTblSectors cellForRowAtIndexPath:nIndex];
        [oSectorCell.oLblName setTextColor:[UIColor blackColor]];
         _bFirstSelected = false;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark table view delegate functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aSector.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"SectorCell";
    SectorCell *oSectorCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer];
    if (oSectorCell == nil) {
        oSectorCell = [[SectorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sCellIdentificer];
    }
    
    SectorBean *oSector = [_aSector objectAtIndex:indexPath.row];
    oSectorCell.oSector = oSector;
    [oSectorCell.oLblName setText:oSector.sName.uppercaseString];
    [CBREFont setFontInLabel:oSectorCell.oLblName withType:CBREFontTypeHelveticaNeue67Medium];
    return oSectorCell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    SectorCell *oSectorCell = (SectorCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oSectorCell setBackgroundColor:[UIColor clearColor]];
    [oSectorCell.oLblName setTextColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SectorCell *oSectorCell = (SectorCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oSectorCell setBackgroundColor:[UIColor whiteColor]];
    [oSectorCell.oLblName setTextColor:[UIColor blackColor]];
    if (!_bFirstSelected) {
        if (_delegate != NULL)
            [_delegate hidePopoverAndSelectedSector: oSectorCell.oSector];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SectorCell *oCell = (SectorCell *)cell;
    [oCell setBackgroundColor:[UIColor clearColor]];
    [oCell.oLblName setTextColor:[UIColor whiteColor]];
}

@end
