//
//  ClientCell.h
//  CBRE
//
//  Created by ddominguezh on 18/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClientBean;

@interface ClientCell : UITableViewCell

@property (strong, nonatomic) ClientBean *oClient;
@property (weak, nonatomic) IBOutlet UIImageView *oImgBuilding;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UILabel *oLblAddress;
@property (weak, nonatomic) IBOutlet UILabel *oLblPhone;
@property (weak, nonatomic) IBOutlet UILabel *oLblEmail;
@property (weak, nonatomic) IBOutlet UIButton *oBntShowMap;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleEmail;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitlePhone;

@end
