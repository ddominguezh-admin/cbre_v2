//
//  AlbumCollectionForm.h
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoBean.h"

@interface AlbumCollectionForm : NSObject

@property (nonatomic) int nIdMenu;
@property (strong, nonatomic) NSString *sTitleNavBar;
@property (strong, nonatomic) NSString *sSubTitleNavBar;
@property (strong, nonatomic) NSMutableArray<PhotoBean> *aPhotos;
@property (nonatomic) BOOL isOnlinePhotos;

- (id)init;


@end
