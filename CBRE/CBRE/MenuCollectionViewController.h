//
//  MenuCollectionViewController.h
//  CBRE
//
//  Created by ddominguezh on 03/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@class MenuCollectionForm;

@interface MenuCollectionViewController : CBRENavigationViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) MenuCollectionForm *oMenuForm;
@property (weak, nonatomic) IBOutlet UIImageView *oImgBackground;
@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvMenus;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstPosYTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstPosYBotton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstPosXLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstPosXRight;

- (IBAction)viewMenu:(id)sender;

@end
