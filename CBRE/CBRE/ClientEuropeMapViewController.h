//
//  ClientEuropeMapViewController.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ClientEuropeMapViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet MKMapView *oMapView;

@end
