//
//  AreaBean.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AreaPointBean.h"
#import "BuildingBean.h"

#import "ClientBean.h"
#import "FilterMapForm.h"
#import "FilterMapClientForm.h"

@protocol AreaBean;

@interface AreaBean : NSObject

@property (strong, nonatomic) NSNumber *nIdArea;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSNumber *nLatitude;
@property (strong, nonatomic) NSNumber *nLongitude;
@property (strong, nonatomic) NSNumber *nZoom;
@property (strong, nonatomic) NSString *sLeyenda;
@property (strong, nonatomic) NSString *sPin;
@property (strong, nonatomic) NSString *sColourText;
@property (strong, nonatomic) NSString *sColourArea;
@property (strong, nonatomic) NSArray<AreaPointBean> *aAreaPoints;
@property (strong, nonatomic) NSArray<BuildingBean> *aBuildings;
@property (strong, nonatomic) NSArray<ClientBean> *aClients;

- (id)initWhitParameters:(NSNumber *) nIdArea
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                   nZoom:(NSNumber *) nZoom
                sLeyenda:(NSString *) sLeyenda
                    sPin:(NSString *) sPin
             sColourText:(NSString *) sColourText
             sColourArea:(NSString *) sColourArea
             aAreaPoints:(NSArray<AreaPointBean> *) aAreaPoints;

- (NSArray<AreaPointBean> *) getAreaPoints;
- (NSArray<BuildingBean> *) getBuildings;
- (NSArray<BuildingBean> *) getBuildingsByFilterMap:(FilterMapForm *) oFilterMap;
- (NSArray<ClientBean> *) getClients;
- (NSArray<ClientBean> *) getClientsByFilterMap:(FilterMapClientForm *) oFilterMap;

@end
