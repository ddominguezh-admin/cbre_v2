//
//  ClientEuropeMapViewController.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientEuropeMapViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "ClientCityMapViewController.h"
#import "CBREMapUtils.h"
#import "CityAnnotation.h"

@interface ClientEuropeMapViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property (strong, nonatomic) CityBean *oCitySelected;

@end

@implementation ClientEuropeMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    
    [self initMapAndFillWithCities];
                
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"]]];
    
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSArray *aAnnotations = _oMapView.selectedAnnotations;
    if (aAnnotations.count > 0)
        [_oMapView deselectAnnotation:[aAnnotations objectAtIndex:0] animated:false];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)initMapAndFillWithCities{
    [_oMapView setRegion:[CBREMapUtils makeRegionEuropaCoordinate]];
    [_oMapView setScrollEnabled:FALSE];
    [_oMapView setZoomEnabled:FALSE];
    [self fillMapWithCities];
}

- (void)fillMapWithCities{
    long nLoopCities = 0, nCountCities = (long)_aCities.count;
    CityBean *oCity = NULL;
    for (; nLoopCities < nCountCities ; nLoopCities++) {
        oCity = [_aCities objectAtIndex:nLoopCities];
        [_oMapView addAnnotation:[[CityAnnotation alloc] initWithCity:oCity]];
    }
}

#pragma mark prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoClientAreaCity" isEqualToString:segue.identifier]) {
        ClientCityMapViewController *oCityMapView = (ClientCityMapViewController *)segue.destinationViewController;
        oCityMapView.oCity = _oCitySelected;
    }
}

#pragma mark mapkit delegate functions

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(CityAnnotation *)annotation fillMKAnnotationView:oPinView];
    oPinView.centerOffset = CGPointMake(-1.8, -11.8);
    return oPinView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_on"]];
    long nLoopCity = 0, nCountCity = (long)_aCities.count;
    BOOL bCityNotFound = TRUE;
    CityBean *oCity = NULL;
    for (; nLoopCity < nCountCity && bCityNotFound; nLoopCity++) {
        oCity = [_aCities objectAtIndex:nLoopCity];
        if (oCity.nIdCity.intValue == view.tag)
            bCityNotFound = FALSE;
    }
    _oCitySelected = oCity;
    [self performSegueWithIdentifier:@"gotoClientAreaCity" sender:self];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    [view setImage:[UIImage imageNamed:@"pin_off"]];
}

@end
