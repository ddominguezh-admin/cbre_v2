//
//  ButtonAreaCell.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AreaBean;

@interface ButtonAreaCell : UITableViewCell

@property (strong, nonatomic) AreaBean *oArea;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgBackground;

@end
