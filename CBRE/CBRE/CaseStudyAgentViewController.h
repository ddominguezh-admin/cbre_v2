//
//  CaseStudyAgentViewController.h
//  CBRE
//
//  Created by ddominguezh on 01/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"

@interface CaseStudyAgentViewController : CBRENavigationViewController

@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsNaves;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsLocales;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsStayAndGo;

@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvDocuments;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsPosX;

@property (weak, nonatomic) IBOutlet UIButton *oBtnAllCases;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNaves;
@property (weak, nonatomic) IBOutlet UIButton *oBtnLocales;
@property (weak, nonatomic) IBOutlet UIButton *oBtnStayAndGo;

- (IBAction)previousDocument:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)viewPdf:(id)sender;
- (IBAction)filterAllCases:(id)sender;
- (IBAction)filterNaves:(id)sender;
- (IBAction)filterLocales:(id)sender;
- (IBAction)filterStayAndGo:(id)sender;

@end
