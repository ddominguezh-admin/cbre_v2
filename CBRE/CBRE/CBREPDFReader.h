//
//  CBREPDFReader.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DocumentBean;

@interface CBREPDFReader : NSObject

+ (void)readerPdf:(DocumentBean *) oDocument andShowInsideView:(UIWebView *) oWebView;
+ (NSData *)loadNSDataPdf:(DocumentBean *) oDocument;
+ (NSData *) getPathAndPdfData:(NSString *)sDocumentName;

@end
