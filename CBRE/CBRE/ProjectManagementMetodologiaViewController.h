//
//  ProjectManagementMetodologiaViewController.h
//  CBRE
//
//  Created by ddominguezh on 03/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"

@interface ProjectManagementMetodologiaViewController : CBRENavigationViewController

@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsActas;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsProcedimientos;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsPlanificacion;

@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvDocuments;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsPosX;

@property (weak, nonatomic) IBOutlet UIButton *oBtnAllCases;
@property (weak, nonatomic) IBOutlet UIButton *oBtnActas;
@property (weak, nonatomic) IBOutlet UIButton *oBtnProcedimientos;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPlanificacion;

- (IBAction)previousDocument:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)viewPdf:(id)sender;
- (IBAction)filterAllCases:(id)sender;
- (IBAction)filterActas:(id)sender;
- (IBAction)filterProcedimientos:(id)sender;
- (IBAction)filterPlanificacion:(id)sender;

@end
