//
//  CBREAbstractDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import <sqlite3.h>

NSString * sHost = @"http://ec2-54-76-10-24.eu-west-1.compute.amazonaws.com/api/v1/";

@implementation AbstractDao

NSString *sNameDatabase = @"CBRE.sqlite";

+ (NSString *)getPathSQLite{
    NSString *sPathDatabase = [AbstractDao getInternalPathDatabase];
    [AbstractDao checkIfTheDatabaseExistsAndCopyIfDoesNotExist:sPathDatabase];
    return sPathDatabase;
}

+ (NSString *)getInternalPathDatabase{
    NSArray *aDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *sDirectoryDocs = [aDirectory objectAtIndex:0];
    return [[NSString alloc] initWithString:[sDirectoryDocs stringByAppendingPathComponent:sNameDatabase]];
}

+ (void)checkIfTheDatabaseExistsAndCopyIfDoesNotExist:(NSString *) sPathDatabase{
    NSFileManager *oFileManager = [NSFileManager defaultManager];
    if([oFileManager fileExistsAtPath:sPathDatabase] == NO){
        [oFileManager copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:sNameDatabase] toPath:sPathDatabase error:NULL];
    }
}

- (BOOL)openConnection{
    BOOL isOpen = (sqlite3_open([[AbstractDao getPathSQLite] UTF8String], &_oDatabase) == SQLITE_OK);
    if (!isOpen)
        NSLog(@"Se ha producido un error mientras se abria la base de datos.");
    return isOpen;
}

- (void)closeConnection{
    sqlite3_close(_oDatabase);
}

- (BOOL)executeSQLStatement:(char *) sSQL oStatement:(sqlite3_stmt **) oStatement{
    BOOL isExcute = (sqlite3_prepare_v2(_oDatabase, sSQL, -1, oStatement, NULL) != SQLITE_OK);
    if(isExcute)
        NSLog(@"Problema al preparar el statement");
    return isExcute;
}

- (NSNumber *) getColumnIntNumber:(sqlite3_stmt *) oStatement nColumn:(int)nColumn{
    
    return [NSNumber numberWithInt:sqlite3_column_int(oStatement, nColumn)];
}

- (NSNumber *) getColumnDoubleNumber:(sqlite3_stmt *) oStatement nColumn:(int)nColumn{
    return [NSNumber numberWithDouble:sqlite3_column_double(oStatement, nColumn)];
}

- (NSString *) getColumnString:(sqlite3_stmt *) oStatement nColumn:(int)nColumn{
    char *sColumnValue = (char *) sqlite3_column_text(oStatement, nColumn);
    if (sColumnValue == NULL)
        return @"";
    return [NSString stringWithUTF8String: sColumnValue];
}

- (BOOL)getColumnBool:(sqlite3_stmt *) oStatement nColumn:(int)nColumn{
    return sqlite3_column_int(oStatement, nColumn) == 1;
}

+ (NSURL *)composeURLWithHost:(NSString *) sUrl{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@lang=%@", sHost, sUrl, [CBRELocalized getLanguageUserDefautl]]];
}

- (NSDictionary *)getDataByURL:(NSString *) sUrl{
    
    NSURLRequest *oURLRequest = [NSURLRequest requestWithURL: [AbstractDao composeURLWithHost:sUrl]];
    
    NSURLResponse *oURLResponse = [[NSURLResponse alloc] init];
    NSError *oError;
    NSData *oData = [NSURLConnection sendSynchronousRequest:oURLRequest returningResponse:&oURLResponse error:&oError];
    
    if (oData != nil) {
        
        NSDictionary * oJsonData = [NSJSONSerialization JSONObjectWithData:oData
                                                                   options:kNilOptions
                                                                     error:&oError];
        
        if (oJsonData != nil) {
            return oJsonData;
        }else{
            // Se ha producido un error al parsear el JSON
            NSLog(@"Error al parsear JSON: %@", oError.localizedDescription);
        }
    }else{
        // Error al descargar los datos del servidor
        NSLog(@"Error al descargar datos del servidor: %@", oError.localizedDescription);
    }
    return [[NSDictionary alloc] init];
}

- (BOOL)checkInternetConecction{
    NSURL *sCheckUrl = [NSURL URLWithString:@"http://www.google.com"];
    NSData *oData = [NSData dataWithContentsOfURL:sCheckUrl];
    if (oData != nil)
        return TRUE;
    return FALSE;
}


@end
