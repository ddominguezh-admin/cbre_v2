//
//  BuildingMiniCell.m
//  CBRE
//
//  Created by ddominguezh on 23/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingMiniCell.h"

@implementation BuildingMiniCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
