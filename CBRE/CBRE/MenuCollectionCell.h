//
//  MenuCollectionCell.h
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MenuCollection;

@interface MenuCollectionCell : UICollectionViewCell

@property (strong, nonatomic) MenuCollection *oMenu;
@property (weak, nonatomic) IBOutlet UIButton *oBtnMenu;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
