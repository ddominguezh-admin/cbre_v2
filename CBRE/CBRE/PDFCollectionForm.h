//
//  PDFCollectionForm.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentBean.h"

@interface PDFCollectionForm : NSObject

@property (nonatomic) int nIdMenu;
@property (strong, nonatomic) NSString *sTitleNavBar;
@property (strong, nonatomic) NSString *sSubTitleNavBar;
@property (strong, nonatomic) NSString *sTitleScreen;
@property (strong, nonatomic) NSString *sSubTitleScreen;
@property (strong, nonatomic) NSString *sBackground;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;

@end
