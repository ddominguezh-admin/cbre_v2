//
//  ButtonAreaCell.m
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ButtonAreaCell.h"

@implementation ButtonAreaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if(selected){
        [_oLblName setTextColor:[UIColor blackColor]];
        [self setBackgroundColor:[UIColor whiteColor]];
    }else{
        [_oLblName setTextColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    // Configure the view for the selected state
}

@end
