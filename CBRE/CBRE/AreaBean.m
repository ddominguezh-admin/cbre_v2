//
//  AreaBean.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaBean.h"
#import "BuildingDao.h"
#import "ClientDao.h"
#import "AreaDao.h"

@implementation AreaBean

- (id)initWhitParameters:(NSNumber *) nIdArea
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                   nZoom:(NSNumber *) nZoom
                sLeyenda:(NSString *) sLeyenda
                    sPin:(NSString *)sPin
             sColourText:(NSString *) sColourText
             sColourArea:(NSString *) sColourArea
             aAreaPoints:(NSArray<AreaPointBean> *) aAreaPoints{
    if (self == NULL)
        self = [super init];
    self.nIdArea = nIdArea;
    self.sName = sName;
    self.nLatitude = nLatitude;
    self.nLongitude = nLongitude;
    self.nZoom = nZoom;
    self.sLeyenda = sLeyenda;
    self.sPin = sPin;
    self.sColourText = sColourText;
    self.sColourArea = sColourArea;
    self.aAreaPoints = aAreaPoints;
    return self;
}

- (NSArray<AreaPointBean> *) getAreaPoints{
    return _aAreaPoints;
}

- (NSArray<BuildingBean> *) getBuildings{
    if (_aBuildings == NULL)
        _aBuildings = [[BuildingDao sharedInstance] getBuildingsByArea:_nIdArea];
    return _aBuildings;
}

- (NSArray<BuildingBean> *) getBuildingsByFilterMap:(FilterMapForm *) oFilterMap{
    return [[BuildingDao sharedInstance] getBuildingsByArea:_nIdArea andFilterForm:oFilterMap];
}
- (NSArray<ClientBean> *) getClients{
    if (_aClients == NULL)
        _aClients = [[ClientDao sharedInstance] getClientsByArea:_nIdArea];
    return _aClients;
}

- (NSArray<ClientBean> *) getClientsByFilterMap:(FilterMapClientForm *) oFilterMap{
    return [[ClientDao sharedInstance] getClientsByArea:_nIdArea andFilterForm:oFilterMap];
}

@end
