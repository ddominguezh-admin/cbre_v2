//
//  AgencyAnalysisViewController.m
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AgencyAnalysisViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionForm.h"
#import "PDFCollectionCell.h"
#import "DocumentDao.h"

@interface AgencyAnalysisViewController ()

@end

@implementation AgencyAnalysisViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis"]]];
    
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    [_oLblNameScreen setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.analisis"].uppercaseString];
    
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblAnalisis, _oLblMatriz, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    [_oLblAnalisis setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.analisis.economico"]];
    [_oLblMatriz setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.matriz.evaluacion"]];
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)viewAnalisis:(id)sender {
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = 2;
    oDetailForm.sTitleNavBar = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"];
    oDetailForm.sSubTitleNavBar = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.analisis.economico"]];
    oDetailForm.sBackground = @"AgencyFondo";
    
    NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesAgenciaAnalisisPdf];
    if(aDocuments.count > 0){
        oDetailForm.oDocument = [aDocuments objectAtIndex:0];
        oPdfDetail.oDetailForm = oDetailForm;
        [self.navigationController pushViewController:oPdfDetail animated:true];
    }
}

- (IBAction)viewMatriz:(id)sender {
    NSString *sSubTitle = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.matriz.evaluacion"]];
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:[[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesAgenciaAnalisisMatrix] oStoryBoard:self.storyboard sTitleScreen:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.analisis.matriz.evaluacion"] sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:sSubTitle sBackground:@"AgencyFondo" nIdMenu:2];
    
    [self.navigationController pushViewController: oPDFView animated:true];
}

@end
