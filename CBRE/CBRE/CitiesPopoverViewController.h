//
//  CitiesPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 05/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CityBean;

@protocol CitiesPopoverViewControllerDelegate;

@interface CitiesPopoverViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) id<CitiesPopoverViewControllerDelegate> delegate;
@property (strong, nonatomic) CityBean *oCity;

@property (weak, nonatomic) IBOutlet UITableView *oTblCities;

@end

@protocol CitiesPopoverViewControllerDelegate <NSObject>

- (void)hidePopoverAndSelectedCity:(CityBean *) oCity;

@end

