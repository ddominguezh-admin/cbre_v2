//
//  PDFCollectionViewController.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PDFCollectionForm.h"

@interface PDFCollectionViewController : CBRENavigationViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) PDFCollectionForm *oCollectionForm;

@property (weak, nonatomic) IBOutlet UIImageView *oImgBackground;
@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UILabel *oLblSubtitleScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvDocuments;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsPosX;

- (IBAction)previousDocument:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)viewPdf:(id)sender;

@end
