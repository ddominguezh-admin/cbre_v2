//
//  CommunicationBean.h
//  CBRE
//
//  Created by ddominguezh on 21/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CommunicationBean;

@interface CommunicationBean : NSObject

@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSString *sDetail;
@property (strong, nonatomic) NSString *sImgMain;

- (id)initWhitParameters:(NSString *) sName
                 sDetail:(NSString *) sDetail
      nCommunicationType:(NSNumber *) nCommunicationType;
@end
