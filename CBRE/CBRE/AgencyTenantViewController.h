//
//  AgencyTenantViewController.h
//  CBRE
//
//  Created by ddominguezh on 23/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "CBRENavigationViewController.h"

@interface AgencyTenantViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UIButton *oBtnVideo;
@property (weak, nonatomic) IBOutlet UIButton *oBtnCases;
@property (strong, nonatomic) MPMoviePlayerController *oMoviePlayer;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UILabel *oLblVideo;
@property (weak, nonatomic) IBOutlet UILabel *oLblCases;

- (IBAction)showVideoAgency:(id)sender;

@end
