//
//  CBREAbstractDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface AbstractDao : NSObject

@property (nonatomic) sqlite3 *oDatabase;

+ (NSString *)getPathSQLite;
+ (NSString *)getInternalPathDatabase;
+ (void)checkIfTheDatabaseExistsAndCopyIfDoesNotExist:(NSString *) sPathDatabase;
- (BOOL)openConnection;
- (void)closeConnection;
- (BOOL)executeSQLStatement:(char *) sSQL oStatement:(sqlite3_stmt **) oStatement;
- (NSNumber *) getColumnIntNumber:(sqlite3_stmt *) oStatement nColumn:(int)nColumn;
- (NSNumber *) getColumnDoubleNumber:(sqlite3_stmt *) oStatement nColumn:(int)nColumn;
- (NSString *) getColumnString:(sqlite3_stmt *) oStatement nColumn:(int)nColumn;
- (BOOL)getColumnBool:(sqlite3_stmt *) oStatement nColumn:(int)nColumn;



+ (NSURL *)composeURLWithHost:(NSString *) sUrl;
- (NSDictionary *)getDataByURL:(NSString *) sUrl;
- (BOOL)checkInternetConecction;

@end
