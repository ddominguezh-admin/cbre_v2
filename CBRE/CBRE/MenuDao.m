//
//  MenuDao.m
//  CBRE
//
//  Created by ddominguezh on 18/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MenuDao.h"
#import "SubMenuBean.h"

@implementation MenuDao

static MenuDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[MenuDao alloc] init];
    return sharedInstance;
}

- (NSMutableArray<SubMenuBean> *)getMenuById:(int) nIdMenu{
    if ([super openConnection]) {
        char *sSelect = "SELECT SUBMENU.ID_SUB_MENU, TXT_NAME, TXT_IMAGE, NUM_POSITION, TXT_NAME_FONT, NUM_POS_X, NUM_POS_Y, NUM_SIZE_FONT, TXT_COLOR_FONT FROM CBRE_MENU MENU, CBRE_SUB_MENU SUBMENU WHERE MENU.ID_MENU = ? AND MENU.ID_SUB_MENU = SUBMENU.ID_SUB_MENU";
        
        sqlite3_stmt *oStatement = NULL;
        [super executeSQLStatement:sSelect oStatement:&oStatement];
        sqlite3_bind_int(oStatement, 1, nIdMenu);
        NSMutableArray<SubMenuBean> *aSubMenus = (NSMutableArray<SubMenuBean> *)[[NSMutableArray alloc] init];
        
        while(sqlite3_step(oStatement) == SQLITE_ROW){
        
            NSNumber *nIdSubMenu = [super getColumnIntNumber:oStatement nColumn:0];
            NSString *sName = [super getColumnString:oStatement nColumn:1];
            NSString *sImage = [super getColumnString:oStatement nColumn:2];
            NSNumber *nPosition = [super getColumnIntNumber:oStatement nColumn:3];
            NSString *sNameFont = [super getColumnString:oStatement nColumn:4];
            NSNumber *nPosX = [super getColumnIntNumber:oStatement nColumn:5];
            NSNumber *nPosY = [super getColumnIntNumber:oStatement nColumn:6];
            NSNumber *nSizeFont = [super getColumnIntNumber:oStatement nColumn:7];
            NSString *sColor = [super getColumnString:oStatement nColumn:8];
            
            [aSubMenus addObject:[[SubMenuBean alloc] initWhitParameters:nIdSubMenu sName:sName sImage:sImage nPosition:nPosition sNameFont:sNameFont nPosX:nPosX nPosY:nPosY nSizeFont:nSizeFont sColor:sColor]];
        }
        [super closeConnection];
        return aSubMenus;
    }
    return (NSMutableArray<SubMenuBean> *)[[NSMutableArray alloc] init];
}

@end
