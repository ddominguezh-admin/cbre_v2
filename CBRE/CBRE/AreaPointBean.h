//
//  AreaPointBean.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AreaBean;

@protocol AreaPointBean;

@interface AreaPointBean : NSObject

@property (strong, nonatomic) NSNumber *nIdPoint;
@property (strong, nonatomic) NSNumber *nLatitude;
@property (strong, nonatomic) NSNumber *nLongitude;

- (id)initWhitParameters:(NSNumber *) nIdPoint
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude;

@end
