//
//  SectorListPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectorBean.h"

@protocol SectorListPopoverViewControllerDelegate;

@interface SectorListPopoverViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) id<SectorListPopoverViewControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray<SectorBean> *aSector;
@property (strong, nonatomic) SectorBean *oSector;

@property (weak, nonatomic) IBOutlet UITableView *oTblSectors;

@end

@protocol SectorListPopoverViewControllerDelegate <NSObject>

- (void)hidePopoverAndSelectedSector:(SectorBean *) oSector;

@end
