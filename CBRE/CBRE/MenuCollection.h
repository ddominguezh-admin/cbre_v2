//
//  MenuCollection.h
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuCollection;

@interface MenuCollection : NSObject

@property (strong, nonatomic) NSString *sImgButton;
@property (strong, nonatomic) NSString *sName;
@property (nonatomic) int nTypeDocument;

- (id)initWhitParameters:(NSString *) sImgButton
                   sName:(NSString *) sName
           nTypeDocument:(int) nTypeDocument;
@end
