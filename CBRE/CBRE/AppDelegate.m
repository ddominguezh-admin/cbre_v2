//
//  AppDelegate.m
//  CBRE
//
//  Created by Berganza on 20/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AppDelegate.h"
#import "CityDao.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.window makeKeyAndVisible];
    
    _aCities = [[CityDao sharedInstance] getCities];
    /*_aCitiesTiles = [[NSMutableDictionary alloc] init];
    
    long nLoopCities = 0, nCountCities = _aCities.count;
    CityBean *oCity = NULL;
    
    RMMBTilesSource *offlineSource = [[RMMBTilesSource alloc] init];
    _oMap = [[RMMapView alloc] initWithFrame:CGRectMake(0, 64, 1024, 768-165-64) andTilesource:offlineSource];
    [_oMap setHideAttribution:YES];
    _oMap.adjustTilesForRetinaDisplay = YES;
    
    for (; nLoopCities < nCountCities ; nLoopCities++) {
        oCity = [_aCities objectAtIndex:nLoopCities];
        
        dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        dispatch_async(backgroundQueue, ^{
            RMMBTilesSource *offlineSource = [[RMMBTilesSource alloc] initWithTileSetResource:oCity.sName ofType:@"mbtiles"];
            dispatch_async(dispatch_get_main_queue(), ^{
               [_aCitiesTiles setObject:offlineSource forKey:oCity.sName];
            });
            
        });
    }*/
    
    [[UINavigationBar appearance] setTintColor:[UIColor grayColor]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"BackgroundNavigationBar"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"btnBack"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"btnBack"]];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
