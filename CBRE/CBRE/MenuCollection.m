//
//  MenuCollection.m
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MenuCollection.h"

@implementation MenuCollection

- (id)initWhitParameters:(NSString *) sImgButton
                   sName:(NSString *) sName
           nTypeDocument:(int) nTypeDocument{
    if (self == NULL)
        self = [super init];
    self.sImgButton = sImgButton;
    self.sName = sName;
    self.nTypeDocument = nTypeDocument;
    return self;
}
@end
