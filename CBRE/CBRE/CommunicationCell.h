//
//  CommunicationCell.h
//  CBRE
//
//  Created by ddominguezh on 09/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommunicationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *oImgType;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UILabel *oLblDescription;

@end
