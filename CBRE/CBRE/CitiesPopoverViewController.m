//
//  CitiesPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 05/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CitiesPopoverViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "ButtonCityCell.h"

@interface CitiesPopoverViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property bool bFirstSelected;

@end

@implementation CitiesPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    _bFirstSelected = true;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    self.view.superview.alpha = 0.85;
}

- (void)viewDidAppear:(BOOL)animated{
    if (_bFirstSelected) {
        NSIndexPath *nIndex = [NSIndexPath indexPathForItem:(int) [_aCities indexOfObject:_oCity] inSection:0];
        [_oTblCities selectRowAtIndexPath:nIndex animated:TRUE scrollPosition:UITableViewScrollPositionNone];
        ButtonCityCell *oButtonCityCell = (ButtonCityCell *)[_oTblCities cellForRowAtIndexPath:nIndex];
        [oButtonCityCell.oLblName setTextColor:[UIColor blackColor]];
        _bFirstSelected = false;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark table view delegate functions

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aCities.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"CityMapCell";
    ButtonCityCell *oButtonCityCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer];
    if (oButtonCityCell == nil) {
        oButtonCityCell = [[ButtonCityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sCellIdentificer];
    }
    CityBean *oCity = [_aCities objectAtIndex:indexPath.row];
    oButtonCityCell.oCity = oCity;
    [oButtonCityCell.oLblName setText:oCity.sName];
    [CBREFont setFontInLabel:oButtonCityCell.oLblName withType:CBREFontTypeHelveticaNeue67Medium];
    return oButtonCityCell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonCityCell *oButtonCityCell = (ButtonCityCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oButtonCityCell.oLblName setTextColor:[UIColor whiteColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonCityCell *oButtonCityCell = (ButtonCityCell *)[tableView cellForRowAtIndexPath:indexPath];
    [oButtonCityCell.oLblName setTextColor:[UIColor blackColor]];
    if (_delegate != NULL)
        [_delegate hidePopoverAndSelectedCity:oButtonCityCell.oCity];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonCityCell *oCell = (ButtonCityCell *)cell;
    [oCell setBackgroundColor:[UIColor clearColor]];
    [oCell.oLblName setTextColor:[UIColor whiteColor]];
}

@end
