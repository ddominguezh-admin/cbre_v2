//
//  CBREMapUtils.h
//  CBRE
//
//  Created by ddominguezh on 02/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CBREMapUtils : NSObject

+ (MKCoordinateRegion)makeRegionByCoordinate:(CLLocationCoordinate2D) oCordinate andZoomLevel:(NSNumber *) nZoom;
+ (MKCoordinateRegion)makeRegionSpanishCoordinate;
+ (MKCoordinateRegion)makeRegionEuropaCoordinate;
+ (void)removeAllAnnotations:(MKMapView *) oMap;

@end
