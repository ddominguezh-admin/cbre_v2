//
//  CBRELocalized.h
//  CBRE
//
//  Created by ddominguezh on 20/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBRELocalized : NSObject

@property (strong, nonatomic) NSString *sLanguage;
@property (strong, nonatomic) NSBundle *oBundle;

+ (instancetype)sharedInstance;

- (NSString *) getMessage:(NSString *) key;
- (NSString *)obtainLocalizedBundleAndTranslateKey:(NSString *) key;
+ (NSString *)getLanguageUserDefautl;
+ (NSBundle *)getLocalizedBundleWithLanguage:(NSString *) sLanguage;

@end
