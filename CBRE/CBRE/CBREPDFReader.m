//
//  CBREPDFReader.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBREPDFReader.h"
#import "CBRELocalized.h"
#import "DocumentBean.h"

@implementation CBREPDFReader

+ (void)readerPdf:(DocumentBean *) oDocument andShowInsideView:(UIWebView *) oWebView{
    NSData *oPdfData = [CBREPDFReader loadNSDataPdf:oDocument];
    [oWebView loadData:oPdfData MIMEType:oDocument.sMimeType textEncodingName:@"utf-8" baseURL:nil];
    //NSURLRequest *oRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:oDocument.sPathURL]];
    //[oWebView loadRequest:oRequest];
}

+ (NSData *)loadNSDataPdf:(DocumentBean *) oDocument{
    NSString *sLanguage = [CBRELocalized getLanguageUserDefautl];
    NSString *sURLFormat = [NSString stringWithFormat:@"%@_%@", oDocument.sPathURL, sLanguage];
    NSData *oDataPdf = [CBREPDFReader getPathAndPdfData:sURLFormat];
    if (oDataPdf)
        return oDataPdf;
    
    return [CBREPDFReader getPathAndPdfData:oDocument.sPathURL];
}

+ (NSData *) getPathAndPdfData:(NSString *)sDocumentName{
    NSURL *oURLData = [NSURL URLWithString:sDocumentName];
    return [NSData dataWithContentsOfURL:oURLData];
}



@end
