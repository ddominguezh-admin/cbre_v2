//
//  FilterMapForm.h
//  CBRE
//
//  Created by ddominguezh on 08/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface FilterMapForm : NSObject

@property (nonatomic) BOOL bInmediatedLocal;
@property (nonatomic) BOOL bFilterByMeters;
@property (nonatomic) BOOL bFilterByPlants;
@property (nonatomic) BOOL bFilterByRent;
@property (nonatomic) BOOL bFilterByRentType;
@property (strong, nonatomic) NSNumber *nMeters;
@property (strong, nonatomic) NSNumber *nPlants;
@property (strong, nonatomic) NSNumber *nMeters2;
@property (strong, nonatomic) NSNumber *nPlants2;
@property (strong, nonatomic) NSNumber *nRentType;
@property (strong, nonatomic) NSNumber *nRent;
@property (strong, nonatomic) NSNumber *nSector;

- (NSString *)getParamsUrl;

@end
