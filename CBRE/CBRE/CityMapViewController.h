//
//  CityMapViewController.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "CitiesPopoverViewController.h"
#import "SectorListPopoverViewController.h"
#import "FilterMapPopoverViewController.h"
@class CityBean;

@interface CityMapViewController : CBRENavigationViewController<MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, CitiesPopoverViewControllerDelegate,SectorListPopoverViewControllerDelegate, FilterMapPopoverViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *oMap;
@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) CitiesPopoverViewController *oCitiesController;
@property (strong, nonatomic) UIPopoverController *oCityPopoverController;
@property (assign, nonatomic) IBOutlet UITableView *oTblAreas;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowCities;
@property (weak, nonatomic) IBOutlet UILabel *oLblSelectedCity;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentCity;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oTblConstPosY;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oTblConstHeight;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowFilterMap;
@property (strong, nonatomic) FilterMapPopoverViewController *oFilterMapController;
@property (strong, nonatomic) UIPopoverController *oFilterMapPopoverController;

@property (weak, nonatomic) IBOutlet UILabel *oLblSectorName;
@property (weak, nonatomic) IBOutlet UIButton *oBtnShowSectors;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentSector;

@property (strong, nonatomic) SectorBean *oSector;
@property (strong, nonatomic) SectorListPopoverViewController *oSectorController;
@property (strong, nonatomic) UIPopoverController *oSectorPopoverController;

@property (strong, nonatomic) FilterMapForm *oFilterMap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;

- (IBAction)showCities:(id)sender;
- (IBAction)showSectors:(id)sender;
- (IBAction)showFilterMap:(id)sender;


@end
