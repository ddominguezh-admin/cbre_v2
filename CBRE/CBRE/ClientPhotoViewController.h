//
//  ClientPhotoViewController.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
@class ClientBean;
#import "PhotoBean.h"
#import "DocumentBean.h"
@class CityBean;

@interface ClientPhotoViewController : CBRENavigationViewController<MFMailComposeViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) ClientBean *oClient;
@property (strong, nonatomic) NSArray<PhotoBean> *aPhotos;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (weak, nonatomic) IBOutlet UICollectionView *oCblPhotos;
@property (weak, nonatomic) IBOutlet UIWebView *oWebDescription;
@property (weak, nonatomic) IBOutlet UIView *oCtnMoviePlayer;
@property (strong, nonatomic) MPMoviePlayerController *oMoviePlayer;
@property (weak, nonatomic) IBOutlet UILabel *oLblClient;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UILabel *oLblCaseStudio;
@property (weak, nonatomic) IBOutlet UILabel *oLblImages;
@property (weak, nonatomic) IBOutlet UILabel *oLblVideo;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPlayVideo;
@property (weak, nonatomic) IBOutlet UIImageView *oImgPreloadVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oClvConstSepatorVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oLblConstSepartorVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oBtnConstSeparatorVideo;
@property (weak, nonatomic) IBOutlet UIButton *obtnShowImages;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oLblConstImageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oWebConstSeparatorImage;

- (IBAction)playVideo:(id)sender;
- (IBAction)sendMailClient:(id)sender;
- (IBAction)showPdfCaseComplete:(id)sender;
- (IBAction)showAlbum:(id)sender;

@end
