//
//  AgencyTenantViewController.m
//  CBRE
//
//  Created by ddominguezh on 23/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AgencyTenantViewController.h"

@interface AgencyTenantViewController ()

@end

@implementation AgencyTenantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"]]];
    
    
    NSURL *oURLFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"videogcshdespañol" ofType:@"mp4"]];
    
    _oMoviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:oURLFile];
    _oMoviePlayer.controlStyle = MPMovieControlStyleDefault;
    _oMoviePlayer.shouldAutoplay = YES;
    _oMoviePlayer.view.frame = CGRectMake(0, 0, 0, 0);
    [self.view addSubview:_oMoviePlayer.view];
    
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblVideo, _oLblCases, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    
    [_oLblTitle setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"].uppercaseString];
    
    [_oLblVideo setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.video"]];
    [_oLblCases setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"]];
    
    [super loadMenuButtonsBack:self andIdMenu:2];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showVideoAgency:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_oMoviePlayer];
    [_oMoviePlayer setFullscreen:YES animated:YES];
    [_oMoviePlayer play];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *oMoviePlayer = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:oMoviePlayer];
    
    if ([oMoviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        [oMoviePlayer setFullscreen:FALSE animated:YES];
}

@end
