//
//  CBRECityDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CityDao.h"
#import "CityConverter.h"

@implementation CityDao

static CityDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[CityDao alloc] init];
    return sharedInstance;
}

- (NSArray<CityBean> *) getCities{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:@"city?"];
        return [CityConverter jsonToCities:oData];
    }
    return (NSArray<CityBean> *)[[NSArray alloc] init];
}

@end
