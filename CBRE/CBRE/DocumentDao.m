//
//  DocumentDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentDao.h"
#import "DocumentConverter.h"

@implementation DocumentDao

static DocumentDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[DocumentDao alloc] init];
    return sharedInstance;
}

- (NSArray<DocumentBean> *)getDocumentsByCity:(NSNumber *) nIdCity andType:(int) nType{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"city_documents/?city=%@&document_type=%d&", nIdCity, nType]];
        return [DocumentConverter jsonToDocuments:oData];
        
    }
    return (NSArray<DocumentBean> *)[[NSArray alloc] init];
}

- (NSArray<DocumentBean> *)getDocumentsByClient:(NSNumber *) nIdClient andType:(int) nType{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"client_documents/?client=%@&document_type=%d&", nIdClient, nType]];
        return [DocumentConverter jsonToDocuments:oData];
    }
    
    return (NSArray<DocumentBean> *)[[NSArray alloc] init];
}

- (NSArray<DocumentBean> *)getDocumentsByBuilding:(NSNumber *) nIdBuilding andType:(int) nType{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building_documents/?building=%@&document_type=%d&", nIdBuilding, nType]];
        return [DocumentConverter jsonToDocuments:oData];
        
    }
    return (NSArray<DocumentBean> *)[[NSArray alloc] init];
}

- (NSArray<DocumentBean> *)getDocumentsByType:(int) nType{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"document/?document_type=%d&", nType]];
        return [DocumentConverter jsonToDocuments:oData];

    }
    return (NSArray<DocumentBean> *)[[NSArray alloc] init];
}


@end
