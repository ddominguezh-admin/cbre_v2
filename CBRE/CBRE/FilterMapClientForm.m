//
//  FilterMapClientForm.m
//  CBRE
//
//  Created by ddominguezh on 13/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FilterMapClientForm.h"

@implementation FilterMapClientForm

- (id)init{
    if (self == NULL) {
        self = [super init];
    }
    self.bSWitchOne = TRUE;
    self.bSWitchTwo = TRUE;
    self.bSWitchThree = TRUE;
    return self;
}

- (NSString *)getParamsUrl{
    NSString *sParams = @"";
    
    if (_nMetersMin != NULL)
        sParams = [NSString stringWithFormat:@"%@min_meters=%@&", sParams, _nMetersMin];
    
    if (_nMetersMax != NULL)
        sParams = [NSString stringWithFormat:@"%@max_meters=%@&", sParams, _nMetersMax];

    if (_nSector != NULL && _nSector.intValue != 0)
        sParams = [NSString stringWithFormat:@"%@sector=%@&", sParams, _nSector];
    
    return sParams;
}

@end
