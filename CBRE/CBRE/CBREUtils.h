//
//  CBREUtils.h
//  CBRE
//
//  Created by ddominguezh on 19/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentBean.h"
#import "MenuCollectionViewController.h"
#import "AlbumCollectionForm.h"
#import "AlbumPhotoViewController.h"

@interface CBREUtils : NSObject

+ (CGPoint)calculatePositionPopoverButtons:(CGRect) oFrame;
+ (void) hideBackgroundWebView:(UIView *) oView;
+ (UIView *)createTitleViewNavigation:(NSString *) sTitle;
+ (UIView *)createTitleViewNavigation:(NSString *) sTitle andSubtitle:(NSString *) sSubtitle;
+ (UIViewController *)obtainViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sSubTitleScreen:(NSString *) sSubTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu;
+ (UIViewController *)obtainViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu;
+ (MenuCollectionViewController *) obtainMenuCollectionViewControlerWithDocuments:(MenuCollectionForm *) oMenuCollectionForm oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu;
+ (AlbumPhotoViewController *) obtainAlbumPhotoViewControlerWithDocuments:(AlbumCollectionForm *) oAlbumForm oStoryBoard:(UIStoryboard *) oStoryBoard sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar nIdMenu:(int) nIdMenu;
+ (CGSize)sizeOfLabel:(UILabel *) label withText:(NSString *)text;
+ (void)createAndResizeImage:(NSString *) sImage andInsertIntUIImageView:(UIImageView *) oContentImage;
+ (void)resizeImage:(UIImage *) oImage andInsertIntUIImageView:(UIImageView *) oContentImage;

@end
