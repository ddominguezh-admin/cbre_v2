//
//  BuildingDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingDao.h"
#import "FilterMapForm.h"
#import "BuildingBean.h"
#import "BuildingConverter.h"
#import "PhotoConverter.h"

@implementation BuildingDao

static BuildingDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[BuildingDao alloc] init];
    return sharedInstance;
}

- (NSArray<BuildingBean> *)getBuildingsByArea:(NSNumber *) nIdArea{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building/?area=%@&", nIdArea]];
        return [BuildingConverter jsonToBuildings:oData];
    }
    return (NSArray<BuildingBean> *)[[NSArray alloc] init];
}

- (BuildingBean *)getBuildingByID:(NSNumber *) nId{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building/%@?", nId]];
        return [BuildingConverter jsonToBuilding:oData];
        
    }
    return (BuildingBean *)[[NSArray alloc] init];
}

- (NSArray<BuildingBean> *)getBuildingsByArea:(NSNumber *) nIdArea andFilterForm:(FilterMapForm *)oFilterMap{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building/?area=%@&%@", nIdArea, [oFilterMap getParamsUrl]]];
        return [BuildingConverter jsonToBuildings:oData];
    }
    return (NSArray<BuildingBean> *)[[NSArray alloc] init];
}

- (NSArray<PhotoBean> *)getPhotosByBuilding:(NSNumber *) nIdBuilding{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"building_documents/?building=%@&document_type=%d?", nIdBuilding, DocumentBuildingPhoto]];
        return [PhotoConverter jsonToPhotos:oData];

    }
    return (NSArray<PhotoBean> *)[[NSArray alloc] init];
}

@end
