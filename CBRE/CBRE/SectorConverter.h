//
//  SectorConverter.h
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SectorBean.h"

@interface SectorConverter : NSObject

+ (NSArray<SectorBean> *) jsonToSectors:(NSDictionary *) oData;
+ (SectorBean *) jsonToSector:(NSDictionary *) oData;

@end
