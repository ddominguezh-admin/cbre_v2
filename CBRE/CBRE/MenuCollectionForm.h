//
//  MenuCollectionForm.h
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuCollection.h"

@interface MenuCollectionForm : NSObject

@property (nonatomic) int nIdMenu;
@property (nonatomic) int nRows;
@property (nonatomic) int nColumns;
@property (strong, nonatomic) NSString *sTitleNavBar;
@property (strong, nonatomic) NSString *sSubTitleNavBar;
@property (strong, nonatomic) NSString *sTitleScreen;
@property (strong, nonatomic) NSString *sSubTitleScreen;
@property (strong, nonatomic) NSString *sBackground;
@property (strong, nonatomic) NSMutableArray<MenuCollection> *aMenus;

- (id)init;

@end
