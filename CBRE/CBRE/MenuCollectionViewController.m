//
//  MenuCollectionViewController.m
//  CBRE
//
//  Created by ddominguezh on 03/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MenuCollectionViewController.h"
#import "MenuCollectionCell.h"
#import "MenuCollection.h"
#import "MenuCollectionForm.h"
#import "DocumentDao.h"
#import "DocumentBean.h"
#import "AlbumPhotoViewController.h"

@interface MenuCollectionViewController ()

@end

@implementation MenuCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    
    [_oImgBackground setImage:[UIImage imageNamed:_oMenuForm.sBackground]];
    [_oLblNameScreen setText:_oMenuForm.sTitleScreen.uppercaseString];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:_oMenuForm.sTitleNavBar andSubtitle:_oMenuForm.sSubTitleNavBar];
    
    [self configurePositionAndWidthCollection];
    [super loadMenuButtonsBack:self andIdMenu:_oMenuForm.nIdMenu];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)configurePositionAndWidthCollection{
    CGRect oFrameCollection = _oClvMenus.frame;
    CGSize oFrameSize =  oFrameCollection.size;
    if (_oMenuForm.nRows == 1) {
        //float nHeight = (oFrameSize.height / 2);
        _oClvConstHeight.constant = 262;
        _oClvConstPosYTop.constant = 63;
        _oClvConstPosYBotton.constant = 321;
    }
    if (_oMenuForm.nColumns < 4) {
        float nWidth = (oFrameSize.width / 4) * (4 - _oMenuForm.nColumns);
        oFrameSize.width -= nWidth;
        _oClvConstWidth.constant -= nWidth;
        _oClvConstPosXLeft.constant += nWidth / 2;
        _oClvConstPosXRight.constant += nWidth / 2;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark collection view delegate functions

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _oMenuForm.aMenus.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"MenuCollectionCell";
    MenuCollectionCell *oMenuCollectionCell = (MenuCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    MenuCollection *oMenuCollection = [_oMenuForm.aMenus objectAtIndex:indexPath.row];
    oMenuCollectionCell.oMenu = oMenuCollection;
    [oMenuCollectionCell.oBtnMenu setImage:[UIImage imageNamed:oMenuCollection.sImgButton] forState:UIControlStateNormal];
    [oMenuCollectionCell.oBtnMenu setTag:indexPath.row];
    [oMenuCollectionCell.oBtnMenu addTarget:self action:@selector(viewMenu:) forControlEvents:UIControlEventTouchDown];
    [oMenuCollectionCell.oLblName setText:oMenuCollection.sName];
    [CBREFont setFontInLabel:oMenuCollectionCell.oLblName withType:CBREFontTypeHelveticaNeue57Condensed];

    return oMenuCollectionCell;
}

- (IBAction)viewMenu:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    MenuCollection *oMenuCollection = [_oMenuForm.aMenus objectAtIndex:oButton.tag];
    
    NSString *sSubTitleNavBar = [NSString stringWithFormat: @"%@ / %@", _oMenuForm.sSubTitleNavBar, oMenuCollection.sName];
    
    if (DocumentOcupantesGestionProyectosDocumentacion3d == oMenuCollection.nTypeDocument) {
        
        AlbumCollectionForm *oAlbumForm = [[AlbumCollectionForm alloc] init];
        oAlbumForm.aPhotos = [self getPhotos3D];
        oAlbumForm.isOnlinePhotos = false;
        AlbumPhotoViewController *oAlbumView = [CBREUtils obtainAlbumPhotoViewControlerWithDocuments:oAlbumForm oStoryBoard:self.storyboard sTitleNavBar:_oMenuForm.sTitleNavBar sSubTitleNavBar:sSubTitleNavBar nIdMenu:_oMenuForm.nIdMenu];
        [self.navigationController pushViewController:oAlbumView animated:true];
    
    }else if(DocumentOcupantesGestionProyectosCasos == oMenuCollection.nTypeDocument){
        
        UIViewController *oMapCases = [self.storyboard instantiateViewControllerWithIdentifier:@"ClientEuropeMapViewController"];
        [self.navigationController pushViewController:oMapCases animated:true];
        
    }else{
        NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:oMenuCollection.nTypeDocument];
        
        UIViewController *oPDFControler = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:_oMenuForm.sTitleScreen sTitleNavBar:_oMenuForm.sTitleNavBar sSubTitleNavBar:sSubTitleNavBar sBackground:_oMenuForm.sBackground nIdMenu:_oMenuForm.nIdMenu];
        if (oPDFControler != NULL)
            [self.navigationController pushViewController:oPDFControler animated:true];
    }
    
}

- (NSMutableArray<PhotoBean> *)getPhotos3D{
    NSMutableArray<PhotoBean> *aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] init];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:1] sImgURL:@"3D_09" sImgHDURL:@"3D_09"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:2] sImgURL:@"3D_31_05_2010_Cafeteria" sImgHDURL:@"3D_31_05_2010_Cafeteria"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:3] sImgURL:@"3D_Cafeteria_03" sImgHDURL:@"3D_Cafeteria_03"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:4] sImgURL:@"3D_Camara cafeteria" sImgHDURL:@"3D_Camara cafeteria"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:5] sImgURL:@"3D_Camara_out" sImgHDURL:@"3D_Camara_out"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:6] sImgURL:@"3D_Camara-zona de descanso" sImgHDURL:@"3D_Camara-zona de descanso"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:7] sImgURL:@"3D_camara03" sImgHDURL:@"3D_camara03"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:8] sImgURL:@"3D_comedor01" sImgHDURL:@"3D_comedor01"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:9] sImgURL:@"3D_huevo blanco" sImgHDURL:@"3D_huevo blanco"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:10] sImgURL:@"3D_imagen general02" sImgHDURL:@"3D_imagen general02"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:11] sImgURL:@"3D_imagen interior_despacho" sImgHDURL:@"3D_imagen interior_despacho"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:12] sImgURL:@"3D_imagen interior" sImgHDURL:@"3D_imagen interior"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:13] sImgURL:@"3D_Imagen recepcion_02" sImgHDURL:@"3D_Imagen recepcion_02"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:14] sImgURL:@"3D_imagen-interior_comedor" sImgHDURL:@"3D_imagen-interior_comedor"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:15] sImgURL:@"3D_interior_01" sImgHDURL:@"3D_interior_01"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:16] sImgURL:@"3D_la recepcion05" sImgHDURL:@"3D_la recepcion05"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:17] sImgURL:@"3D_la sala con la mesa redonda" sImgHDURL:@"3D_la sala con la mesa redonda"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:18] sImgURL:@"3D_OPCION_B_02" sImgHDURL:@"3D_OPCION_B_02"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:19] sImgURL:@"3D_recepcion cerca de la escalera_paseo castillana200" sImgHDURL:@"3D_recepcion cerca de la escalera_paseo castillana200"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:20] sImgURL:@"3D_Sala reunion camara" sImgHDURL:@"3D_Sala reunion camara"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:21] sImgURL:@"3D_Sala" sImgHDURL:@"3D_Sala"]];
    [aPhotos addObject:[[PhotoBean alloc] initWhitParameters:[NSNumber numberWithInt:22] sImgURL:@"3D_tuenti_interior_03" sImgHDURL:@"3D_tuenti_interior_03"]];
    return aPhotos;
}
@end
