//
//  CBREPopoverBackgroundView.m
//  CBRE
//
//  Created by ddominguezh on 16/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBREPopoverBackgroundView.h"

#define CONTENT_INSET 1.0
#define CAP_INSET 5.0
#define ARROW_BASE 0.0
#define ARROW_HEIGHT 0.0

@implementation CBREPopoverBackgroundView

-(id)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        /*_borderImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"PopoverNotBackground"] resizableImageWithCapInsets:UIEdgeInsetsMake(CAP_INSET,CAP_INSET,CAP_INSET,CAP_INSET)]];
         _arrowView = nil;
         [self setBackgroundColor:[UIColor clearColor]];
         self.layer.cornerRadius = 0.0;
         [self addSubview:_borderImageView];
         [self addSubview:_arrowView];*/
        self.alpha = 0;
        
    }
    return self;
}

- (CGFloat) arrowOffset {
    return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
}


+(UIEdgeInsets)contentViewInsets{
    return UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
}

+(CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
    return ARROW_BASE;
}

-  (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat _height = self.frame.size.height;
    CGFloat _width = self.frame.size.width;
    CGFloat _left = 0.0;
    CGFloat _top = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
    
    [_arrowView setTransform:_rotation];
    
}

@end
