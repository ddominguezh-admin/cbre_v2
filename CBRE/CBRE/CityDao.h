//
//  CBRECityDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "CityBean.h"

@interface CityDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<CityBean> *) getCities;

@end
