//
//  PlantsCell.h
//  CBRE
//
//  Created by ddominguezh on 09/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *oLblPlant;
@property (weak, nonatomic) IBOutlet UILabel *oLblMeters;

@end
