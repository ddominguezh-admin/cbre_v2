//
//  LanguageViewController.h
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface LanguageViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UILabel *oLblSpanish;
@property (weak, nonatomic) IBOutlet UILabel *oLblEnglish;

- (IBAction)selectLanguage:(id)sender;


@end
