//
//  CBREFont.m
//  CBRE
//
//  Created by ddominguezh on 02/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBREFont.h"

@implementation CBREFont

static CBREFont *sharedInstance;

+ (NSString *)obtainFontNameWithType:(CBREFontTypes) oFontType{
    NSString *sFontName = @"";
    if (oFontType == CBREFontTypeHelveticaNeue57Condensed) {
        sFontName = @"HelveticaNeue-Condensed";
    }else if(oFontType == CBREFontTypeHelveticaNeue77BoldCondensed){
        sFontName = @"HelveticaNeue-CondensedBold";
    }else if(oFontType == CBREFontTypeHelveticaNeue47LightCondensed){
        sFontName = @"HelveticaNeue-LightCond";
    }else if(oFontType == CBREFontTypeHelveticaNeue67Medium){
        sFontName = @"HelveticaNeue-Condensed";
    }else if(oFontType == CBREFontTypeHelveticaNeue87HeavyCondensed){
        sFontName = @"HelveticaNeue-HeavyCond";
    }else if(oFontType == CBREFontTypeHelveticaRegular){
        sFontName = @"HelveticaNeue-Roman";
    }else if(oFontType == CBREFontTypeHelveticaBold){
        sFontName = @"HelveticaNeue-Heavy";
        
    }
    return sFontName;
}

+ (CBREFontTypes)obtainFontTypeWithText:(NSString *) oFontName{
    CBREFontTypes sFontType = CBREFontTypeHelveticaNeue57Condensed;
    if ([@"CBREFontTypeHelveticaNeue57Condensed" isEqualToString:oFontName]) {
        sFontType = CBREFontTypeHelveticaNeue57Condensed;
    }else if([@"CBREFontTypeHelveticaNeue77BoldCondensed" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaNeue77BoldCondensed;
    }else if([@"CBREFontTypeHelveticaNeue47LightCondensed" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaNeue47LightCondensed;
    }else if([@"CBREFontTypeHelveticaNeue67Medium" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaNeue67Medium;
    }else if([@"CBREFontTypeHelveticaNeue87HeavyCondensed" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaNeue87HeavyCondensed;
    }else if([@"CBREFontTypeHelveticaRegular" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaRegular;
    }else if([@"CBREFontTypeHelveticaBold" isEqualToString:oFontName]){
        sFontType = CBREFontTypeHelveticaBold;
        
    }
    return sFontType;
}

+ (void)setFontInLabels:(NSArray *) aLabels withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitLabels:aLabels];
}

+ (void)setFontInLabel:(UILabel *) oLabel withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitLabel:oLabel];
}

+ (void)setFontInTextViews:(NSArray *) aTextViews withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitTextViews:aTextViews];
}

+ (void)setFontInTextView:(UITextView *) oTextView withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitTextView:oTextView];
}

+ (void)setFontInButtons:(NSArray *) aButtons withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitButtons:aButtons];
}

+ (void)setFontInButton:(UIButton *) oButton withType:(CBREFontTypes) oFonType{
    NSString *sFontName = [CBREFont obtainFontNameWithType:oFonType];
    [CBREFont setFontName:sFontName whitButton:oButton];
}

+ (void)setFontName:(NSString *) sFontName whitLabels:(NSArray *) aLabels{
    long nLoopLabel = 0, nCountLabel = aLabels.count;
    UILabel *oLabel = NULL;
    for ( ; nLoopLabel < nCountLabel ; nLoopLabel++) {
        oLabel = [aLabels objectAtIndex:nLoopLabel];
        [CBREFont setFontName:sFontName whitLabel:oLabel];
    }
}

+ (void)setFontName:(NSString *) sFontName whitLabel:(UILabel *) oLabel {
    UIFont *oFont = oLabel.font;
    [oLabel setFont:[UIFont fontWithName:sFontName size:oFont.pointSize]];
}

+ (void)setFontName:(NSString *) sFontName whitTextViews:(NSArray *) aTextViews {
    long nLoopTextView = 0, nCountTextView = aTextViews.count;
    UITextView *oTextView = NULL;
    for ( ; nLoopTextView < nCountTextView ; nLoopTextView++) {
        oTextView = [aTextViews objectAtIndex:nLoopTextView];
        [CBREFont setFontName:sFontName whitTextView:oTextView];
    }
}

+ (void)setFontName:(NSString *) sFontName whitTextView:(UITextView *) oTextView {
    UIFont *oFont = oTextView.font;
    [oTextView setFont:[UIFont fontWithName:sFontName size:oFont.pointSize]];
}

+ (void)setFontName:(NSString *) sFontName whitButtons:(NSArray *) aButtons {
    long nLoopButton = 0, nCountButton = aButtons.count;
    UIButton *oButton = NULL;
    for ( ; nLoopButton < nCountButton ; nLoopButton++) {
        oButton = [aButtons objectAtIndex:nLoopButton];
        [CBREFont setFontName:sFontName whitButton:oButton];
    }
}

+ (void)setFontName:(NSString *) sFontName whitButton:(UIButton *) oButton {
    [CBREFont setFontName:sFontName whitLabel:oButton.titleLabel];
}

@end
