//
//  ClientAreaViewController.h
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "CitiesPopoverViewController.h"
#import "AreasPopoverViewController.h"
#import "FilterMapClientPopoverViewController.h"
#import "ClientPopoverViewController.h"
#import "SectorListPopoverViewController.h"
#import <MessageUI/MessageUI.h>

@class CityBean;
@class AreaBean;
#import "ClientBean.h"

@interface ClientAreaMapViewController : CBRENavigationViewController<MFMailComposeViewControllerDelegate, MKMapViewDelegate, CitiesPopoverViewControllerDelegate, AreasPopoverViewControllerDelegate, FilterMapClientPopoverViewControllerDelegate, ClientPopoverViewControllerDelegate, SectorListPopoverViewControllerDelegate,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) AreaBean *oArea;
@property (strong, nonatomic) SectorBean *oSector;
@property (strong, nonatomic) NSArray<ClientBean> *aClients;
@property (strong, nonatomic) NSMutableArray<ClientBean> *aClientsFilter;
@property (strong, nonatomic) ClientBean *oClientSelected;
@property (strong, nonatomic) FilterMapClientForm *oFilterMap;

@property (strong, nonatomic) NSArray *aSections;
@property (strong, nonatomic) NSMutableDictionary *aClientSections;
@property (weak, nonatomic) IBOutlet UITableView *oTblClients;

@property (weak, nonatomic) IBOutlet MKMapView *oMap;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowCities;
@property (weak, nonatomic) IBOutlet UILabel *oLblCityName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentCity;
@property (weak, nonatomic) IBOutlet UIButton *oBtnShowAreas;
@property (weak, nonatomic) IBOutlet UILabel *oLblAreaName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentArea;
@property (weak, nonatomic) IBOutlet UIButton *oBtnShowFilterMap;
@property (weak, nonatomic) IBOutlet UIButton *oBtnShowSectors;
@property (weak, nonatomic) IBOutlet UILabel *oLblSectorName;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentSector;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *oListButton;

@property (strong, nonatomic) CitiesPopoverViewController *oCitiesController;
@property (strong, nonatomic) UIPopoverController *oCityPopoverController;
@property (strong, nonatomic) AreasPopoverViewController *oAreasController;
@property (strong, nonatomic) UIPopoverController *oAreasPopoverController;
@property (strong, nonatomic) FilterMapClientPopoverViewController *oFilterMapController;
@property (strong, nonatomic) UIPopoverController *oFilterMapPopoverController;
@property (strong, nonatomic) ClientPopoverViewController *oClientController;
@property (strong, nonatomic) UIPopoverController *oClientPopoverController;
@property (strong, nonatomic) SectorListPopoverViewController *oSectorController;
@property (strong, nonatomic) UIPopoverController *oSectorPopoverController;
@property (weak, nonatomic) IBOutlet UITableView *oTblClientMini;
@property (weak, nonatomic) IBOutlet UIControl *oCtnTableSearch;
@property (weak, nonatomic) IBOutlet UITextField *oTxtSearh;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *oLblListadoCases;
@property (weak, nonatomic) IBOutlet UILabel *oLblListadoCity;

- (IBAction)showCities:(id)sender;
- (IBAction)showAreas:(id)sender;
- (IBAction)showSectors:(id)sender;
- (IBAction)showListClientsShowsInMap:(id)sender;
- (IBAction)showFilterMap:(id)sender;
- (IBAction)showOrHideSearch:(id)sender;

@end
