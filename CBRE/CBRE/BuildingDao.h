//
//  BuildingDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "BuildingBean.h"
@class FilterMapForm;
#import "PhotoBean.h"

@interface BuildingDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<BuildingBean> *)getBuildingsByArea:(NSNumber *) nIdArea;
- (NSArray<BuildingBean> *)getBuildingsByArea:(NSNumber *) nIdArea andFilterForm:(FilterMapForm *)oFilterMap;
- (BuildingBean *)getBuildingByID:(NSNumber *) nId;
- (NSArray<PhotoBean> *)getPhotosByBuilding:(NSNumber *) nIdBuilding;

@end
