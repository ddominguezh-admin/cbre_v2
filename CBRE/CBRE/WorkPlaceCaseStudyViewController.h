//
//  WorkPlaceCaseStudyViewController.h
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"
#import "ClientBean.h"

@interface WorkPlaceCaseStudyViewController : CBRENavigationViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) NSMutableArray<DocumentBean> *aAllDocument;
@property (strong, nonatomic) ClientBean *oClient;

@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvDocuments;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsPosX;

@property (weak, nonatomic) IBOutlet UIButton *oBtnFilterClient;
@property (weak, nonatomic) IBOutlet UILabel *oLblDefinicion;
@property (weak, nonatomic) IBOutlet UILabel *oLblGestion;
@property (weak, nonatomic) IBOutlet UILabel *oLblMediacion;
@property (weak, nonatomic) IBOutlet UIButton *oBtnDefinicion;
@property (weak, nonatomic) IBOutlet UIButton *oBtnGestion;
@property (weak, nonatomic) IBOutlet UIButton *oBtnMediacion;

@property (strong, nonatomic) NSMutableArray<ClientBean> *aClients;
@property (weak, nonatomic) IBOutlet UITableView *oTblClients;
@property (weak, nonatomic) IBOutlet UIControl *oCntViewPopup;

- (IBAction)previousDocument:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)viewPdf:(id)sender;
- (IBAction)selectedCheck:(id)sender;
- (IBAction)showPopoverClientList:(id)sender;
- (IBAction)hidePopoverControl:(id)sender;

@end
