//
//  DocumentConverter.h
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentBean.h"

@interface DocumentConverter : NSObject

+ (NSArray<DocumentBean> *) jsonToDocuments:(NSDictionary *) oData;
+ (NSArray<DocumentBean> *) arrayToDocuments:(NSArray *) oData;
+ (DocumentBean *) jsonToDocument:(NSDictionary *) oData;

@end
