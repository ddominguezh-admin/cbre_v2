//
//  AlbumCollectionForm.m
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AlbumCollectionForm.h"

@implementation AlbumCollectionForm

- (id)init{
    if (self == NULL)
        self = [super init];
    self.nIdMenu = 1;
    self.aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] init];
    self.isOnlinePhotos = true;
    return self;
}
@end
