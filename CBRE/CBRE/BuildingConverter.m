//
//  BuildingConverter.m
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingConverter.h"

@implementation BuildingConverter

+ (NSArray<BuildingBean> *) jsonToBuildings:(NSDictionary *) oData{
    NSMutableArray<BuildingBean> *aBuildings = (NSMutableArray<BuildingBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopBuildings = 0, nCountBuildings = (long)oResult.count;
    NSDictionary *oDataBuilding = NULL;
    
    for (; nLoopBuildings < nCountBuildings; nLoopBuildings++) {
        oDataBuilding = [oResult objectAtIndex:nLoopBuildings];
        [aBuildings addObject:[BuildingConverter jsonToBuilding:oDataBuilding]];
    }
    
    return aBuildings;
}

+ (BuildingBean *) jsonToBuilding:(NSDictionary *) oData{
    
    NSNumber *nIdBuilding = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    NSNumber *nLatitude = [oData objectForKey:@"latitud"];
    NSNumber *nLongitude = [oData objectForKey:@"longitud"];
    NSNumber *nMeters = [oData objectForKey:@"meters"];
    NSNumber *nPlants = [oData objectForKey:@"plants"];
    NSNumber *nRent = [oData objectForKey:@"rent"];
    NSString *sAddress = [oData objectForKey:@"address"];
    NSString *sPhone = [oData objectForKey:@"phone"];
    NSString *sEmail = [oData objectForKey:@"mail"];
    NSString *sImgMain = [oData objectForKey:@"img_main"];
    NSString *sDescription = [oData objectForKey:@"description"];
    NSString *sDescriptionHtml = [oData objectForKey:@"description_html"];
    
    BuildingBean *oBuildingBean = [[BuildingBean alloc] initWhitParameters:nIdBuilding sName:sName nLatitude:nLatitude nLongitude:nLongitude nMeters:nMeters nPlants:nPlants nRent:nRent sAddress:sAddress sPhone:sPhone sEmail:sEmail sImgMain:sImgMain sDescription:sDescription sDescriptionHtml:sDescriptionHtml];
    
    oBuildingBean.aPlants = [BuildingConverter jsonToPlants:oData];
    oBuildingBean.aCommunications = [BuildingConverter jsonToCommunications:oData];
    return oBuildingBean;
    
}

+ (NSArray<PlantBean> *) jsonToPlants:(NSDictionary *) oData{
    NSMutableArray<PlantBean> *aPlants = (NSMutableArray<PlantBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"plants_detail"];
    
    long nLoopPlants = 0, nCountPlants = (long)oResult.count;
    NSDictionary *oDataPlant = NULL;
    
    for (; nLoopPlants < nCountPlants; nLoopPlants++) {
        oDataPlant = [oResult objectAtIndex:nLoopPlants];
        [aPlants addObject:[BuildingConverter jsonToPlant:oDataPlant]];
    }

    return aPlants;
}

+ (PlantBean *) jsonToPlant:(NSDictionary *) oData{
    NSString *nPlant = [oData objectForKey:@"plant"];
    NSNumber *nMeters = [oData objectForKey:@"num_meters"];
    return [[PlantBean alloc] initWhitParameters:nPlant nMeters:nMeters];
    
    /*
     "num_rent": 32483.0
     */
}

+ (NSArray<CommunicationBean> *) jsonToCommunications:(NSDictionary *) oData{
    
    NSMutableArray<CommunicationBean> *aCommunications = (NSMutableArray<CommunicationBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"comunications"];
    
    long nLoopCommunications = 0, nCountCommunications = (long)oResult.count;
    NSDictionary *oDataCommunication = NULL;
    
    for (; nLoopCommunications < nCountCommunications; nLoopCommunications++) {
        oDataCommunication = [oResult objectAtIndex:nLoopCommunications];
        [aCommunications addObject:[BuildingConverter jsonToCommunication:oDataCommunication]];
    }
    
    return aCommunications;
}

+ (CommunicationBean *) jsonToCommunication:(NSDictionary *) oData{
    NSString *sName = [oData objectForKey:@"comunication_name"];
    NSString *sDetail = [oData objectForKey:@"comunication_detail"];
    NSNumber *nCommunicationType = [oData objectForKey:@"comunication_type"];
    return [[CommunicationBean alloc] initWhitParameters:sName sDetail:sDetail nCommunicationType:nCommunicationType];
}

@end
