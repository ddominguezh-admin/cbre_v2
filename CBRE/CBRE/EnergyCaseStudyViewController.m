//
//  EnergyCaseStudyViewController.m
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "EnergyCaseStudyViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionCell.h"
#import "DocumentDao.h"

@interface EnergyCaseStudyViewController ()

@property (strong, nonatomic) DocumentBean *oDocumentSelected;
@property int indexPathSelected;

@end

@implementation EnergyCaseStudyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _indexPathSelected = 1;
    
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnAllCases, _oBtnCertificacion, _oBtnConstruccion, _oBtnDisenyo, _oBtnOperacion, _oBtnStudy, nil] withType:CBREFontTypeHelveticaRegular];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    
    _aDocumentsStudy = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesEnergiaCasosConsumo];
    _aDocumentsDisenyo = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesEnergiaCasosDisenio];
    _aDocumentsConstruction = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesEnergiaCasosConstruccion];
    _aDocumentsOperation = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesEnergiaCasosOperacion];
    _aDocumentsCertification = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesEnergiaCasosCertificacion];
    
    [self addFormatToButton:_oBtnAllCases andText:@"i18n.occupants.energy.casesStudy.todos"];
    [self addFormatToButton:_oBtnCertificacion andText:@"i18n.occupants.energy.casesStudy.certificacion"];
    [self addFormatToButton:_oBtnConstruccion andText:@"i18n.occupants.energy.casesStudy.construccion"];
    [self addFormatToButton:_oBtnDisenyo andText:@"i18n.occupants.energy.casesStudy.disenyo"];
    [self addFormatToButton:_oBtnOperacion andText:@"i18n.occupants.energy.casesStudy.operacion"];
    [self addFormatToButton:_oBtnStudy andText:@"i18n.occupants.energy.casesStudy.estudios"];
    
    NSString *sSubTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", sSubTitle, [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.energia.casesStudies"]]];
    
    [super loadMenuButtonsBack:self andIdMenu:1];
    
    [self executeAllCases];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)addFormatToButton:(UIButton *) oButton andText:(NSString *) sKey{
    [oButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [oButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [oButton setTitle:[[CBRELocalized sharedInstance] getMessage:sKey] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection view delegate functions

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aDocuments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"PDFCollectionCell";
    PDFCollectionCell *oPDFCollectionCell = (PDFCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    DocumentBean *oDocument = [_aDocuments objectAtIndex:indexPath.row];
    oPDFCollectionCell.oDocument = oDocument;
    [oPDFCollectionCell.oBtnPdf setImage:[UIImage imageNamed:oDocument.sImgName] forState:UIControlStateNormal];
    [oPDFCollectionCell.oBtnPdf setTag:indexPath.row];
    [oPDFCollectionCell.oBtnPdf addTarget:self action:@selector(viewPdf:) forControlEvents:UIControlEventTouchDown];
    [oPDFCollectionCell.oLblName setText:oDocument.sName];
    return oPDFCollectionCell;
}

- (IBAction)previousDocument:(id)sender {
    BOOL bCanMove = _indexPathSelected > 1;
    if (bCanMove) {
        _indexPathSelected--;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)nextDocument:(id)sender {
    long nLastStep = _aDocuments.count - 2;
    BOOL bCanMove = _indexPathSelected < nLastStep;
    if (bCanMove) {
        _indexPathSelected++;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)viewPdf:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = 2;
    oDetailForm.sTitleNavBar = @"OCUPANTES";
    oDetailForm.sSubTitleNavBar = @"Energy";
    oDetailForm.sBackground = @"EnergyFondo";
    oDetailForm.oDocument = [_aDocuments objectAtIndex:oButton.tag];
    oPdfDetail.oDetailForm = oDetailForm;
    [self.navigationController pushViewController:oPdfDetail animated:true];
}

- (IBAction)filterAllCases:(id)sender {
    [self resetColorLabels];
    [self executeAllCases];
}

- (void)executeAllCases{
    [self setSelectedLable:_oBtnAllCases];
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    
    [aDocuments addObjectsFromArray:_aDocumentsStudy];
    [aDocuments addObjectsFromArray:_aDocumentsDisenyo];
    [aDocuments addObjectsFromArray:_aDocumentsConstruction];
    [aDocuments addObjectsFromArray:_aDocumentsOperation];
    [aDocuments addObjectsFromArray:_aDocumentsCertification];
    
    _aDocuments = (NSArray<DocumentBean> *)[[NSArray alloc] initWithArray:aDocuments];
    [self reloadCollectionData];
}

- (IBAction)filterStudies:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnStudy];
    _aDocuments = _aDocumentsStudy;
    [self reloadCollectionData];
}

- (IBAction)filterDisenyos:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnDisenyo];
    _aDocuments = _aDocumentsDisenyo;
    [self reloadCollectionData];
}

- (IBAction)filterConstruction:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnConstruccion];
    _aDocuments = _aDocumentsConstruction;
    [self reloadCollectionData];
}

- (IBAction)filterOperation:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnOperacion];
    _aDocuments = _aDocumentsOperation;
    [self reloadCollectionData];
}

- (IBAction)filterCertification:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnCertificacion];
    _aDocuments = _aDocumentsCertification;
    [self reloadCollectionData];
}

- (void)resetColorLabels{
    [_oBtnAllCases setSelected:FALSE];
    [_oBtnCertificacion setSelected:FALSE];
    [_oBtnConstruccion setSelected:FALSE];
    [_oBtnDisenyo setSelected:FALSE];
    [_oBtnOperacion setSelected:FALSE];
    [_oBtnStudy setSelected:FALSE];
    _indexPathSelected = 1;
}

- (void)setSelectedLable:(UIButton *) oButton{
    NSString *sTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.energy.casesStudy.energy"];
    [_oLblNameScreen setText:[NSString stringWithFormat:@"%@ (%@)", sTitle, oButton.titleLabel.text].uppercaseString];
    [oButton setSelected:TRUE];
}

- (void)reloadCollectionData{
    long nCountDocuments = _aDocuments.count;
    if (nCountDocuments < 4) {
        [_oBtnPrevious setHidden:TRUE];
        [_oBtnNext setHidden:TRUE];
    }else{
        [_oBtnPrevious setHidden:FALSE];
        [_oBtnNext setHidden:FALSE];
    }
    if (nCountDocuments < 3)
        [self calculateFrameCollectionView:nCountDocuments];
    else
        [self calculateFrameCollectionView:3];
    [_oClvDocuments reloadData];
}

- (void)calculateFrameCollectionView:(long) nCountDocuments{
    float nWidth = 760 - ((760 / 3) * nCountDocuments);
    _oConstClvDocsWidth.constant = 760 - nWidth;
    _oConstClvDocsPosX.constant = 128 + (nWidth / 2);
}


@end
