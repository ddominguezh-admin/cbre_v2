//
//  AreaConverter.h
//  CBRE
//
//  Created by ddominguezh on 24/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AreaBean.h"
#import "AreaPointBean.h"

@interface AreaConverter : NSObject

+ (NSArray<AreaBean> *) jsonToAreas:(NSDictionary *) oData;
+ (AreaBean *) jsonToArea:(NSDictionary *) oData;
+ (NSArray<AreaPointBean> *) jsonToAreaPoints:(NSArray *) aJsonPoints;
+ (AreaPointBean *) jsonToAreaPoint:(NSDictionary *) oData;

@end
