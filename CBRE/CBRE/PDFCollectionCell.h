//
//  PDFCollectionCell.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DocumentBean;

@interface PDFCollectionCell : UICollectionViewCell

@property (strong, nonatomic) DocumentBean *oDocument;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPdf;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
