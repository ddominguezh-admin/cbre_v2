//
//  ClientBean.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientBean.h"
#import "ClientDao.h"

@implementation ClientBean

- (id)initWhitParameters:(NSNumber *) nIdClient
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                sAddress:(NSString *) sAddress
                  sPhone:(NSString *) sPhone
                  sEmail:(NSString *) sEmail
                sImgMain:(NSString *) sImgMain
        sDescriptionHtml:(NSString *) sDescriptionHtml
                  sVideo:(NSString *) sVideo
         sVideoExtension:(NSString *) sVideoExtension
                 nMeters:(NSNumber *) nMeters{
    if (self == NULL)
        self = [super init];
    self.nIdClient = nIdClient;
    self.sName = sName;
    self.nLatitude = nLatitude;
    self.nLongitude = nLongitude;
    self.sAddress = sAddress;
    self.sPhone = sPhone;
    self.sEmail = sEmail;
    self.sImgMain = sImgMain;
    self.sDescriptionHtml = [NSString stringWithFormat:@"<style type=\"text/css\">ul { padding:0px 8px; margin: 0px 2px 0px 10px; } li{color: rgb(4, 253, 16);} .white {color:white;} .verde {color:rgb(105, 246, 66);} div {float: left; text-align: justify; width: %@} .enter{line-height: 1px; height: 1px; width: %@; float:left;}</style><body style=\"background-color: transparent; font-family:Helvetica Neue; width: 290px;\">%@</body>", @"98%", @"100%", sDescriptionHtml];
    self.sVideo = sVideo;
    self.sVideoExtension = sVideoExtension;
    self.nMeters = nMeters;
    return self;
}

- (id)initWhitParameters:(NSNumber *) nIdClient
                   sName:(NSString *) sName{
    if (self == NULL)
        self = [super init];
    self.nIdClient = nIdClient;
    self.sName = sName;
    return self;
}
- (NSArray<PhotoBean> *) getPhotos{
    if (_aPhotos == NULL)
        _aPhotos = [[ClientDao sharedInstance] getPhotosByClient:_nIdClient];
    return _aPhotos;
}

@end
