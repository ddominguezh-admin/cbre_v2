//
//  CityBean.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CityBean.h"
#import "AreaDao.h"
#import "DocumentDao.h"

@implementation CityBean

- (id)initWhitParameters:(NSNumber *) nIdCity
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                   nZoom:(NSNumber *) nZoom
           bFlgOnlyCases:(BOOL) bFlgOnlyCases{
    if (self == NULL)
        self = [self init];
    self.nIdCity = nIdCity;
    self.sName = sName;
    self.nLatitude = nLatitude;
    self.nLongitude = nLongitude;
    self.nZoom = nZoom;
    self.bFlgOnlyCases = bFlgOnlyCases;
    return self;

}

- (NSArray<AreaBean> *) getAreas{
    if(_aAreas == NULL)
        _aAreas = [[AreaDao sharedInstance] getAreasByIdCity:_nIdCity];
    return _aAreas;
}

- (NSArray<AreaBean> *) getAreasByFilterClientMap:(FilterMapClientForm *) oFilterMap{
    return [[AreaDao sharedInstance] getAreasByIdCity:_nIdCity andFilterClientForm:oFilterMap];
}

- (NSArray<AreaBean> *) getAreasByFilterMap:(FilterMapForm *) oFilterMap{
    return [[AreaDao sharedInstance] getAreasByIdCity:_nIdCity andFilterForm:oFilterMap];
}

- (NSArray<DocumentBean> *) getDocumentsByType:(int) nIdDocumentType{
    return [[DocumentDao sharedInstance] getDocumentsByCity:_nIdCity andType:nIdDocumentType];
}

- (DocumentBean *)getDocumentByType:(int) nIdDocumentType{
    NSArray<DocumentBean> * aDocuments = [[DocumentDao sharedInstance] getDocumentsByCity:_nIdCity andType:nIdDocumentType];
    if (aDocuments.count > 0)
        return [aDocuments objectAtIndex:0];
    return NULL;
}

@end
