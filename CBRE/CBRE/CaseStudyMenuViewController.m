//
//  CaseStudyMenuViewController.m
//  CBRE
//
//  Created by ddominguezh on 01/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CaseStudyMenuViewController.h"

@interface CaseStudyMenuViewController ()

@end

@implementation CaseStudyMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnAgencia, _oBtnEnergia, _oBtnProject, _oBtnWorkplace, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    [_oBtnEnergia.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [_oBtnEnergia.titleLabel setTextAlignment:NSTextAlignmentCenter];

    [_oLblTitle setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"].uppercaseString];
    [_oBtnAgencia setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent"] forState:UIControlStateNormal];
    [_oBtnEnergia setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.energia"] forState:UIControlStateNormal];
    [_oBtnProject setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"] forState:UIControlStateNormal];
    [_oBtnWorkplace setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.workplace"] forState:UIControlStateNormal];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"]];
    
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
