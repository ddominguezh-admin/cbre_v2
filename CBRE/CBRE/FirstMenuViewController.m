//
//  FirstMenuViewController.m
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FirstMenuViewController.h"

@interface FirstMenuViewController ()

@end

@implementation FirstMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.navigationItem.backBarButtonItem setTitle:[[CBRELocalized sharedInstance ] getMessage:@"i18n.common.button.back"]];
    
    NSString *sOffices = [[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.offices"];
    [_oLblGoOffices setText:sOffices];
    NSString *sHotels = [[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.hotels"];
    [_oLblGoHotels setText:sHotels];
    NSString *sIndustrial = [[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.industrial"];
    [_oLblGoIndustrial setText:sIndustrial];
    NSString *sResidential = [[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.residential"];
    [_oLblGoResidential setText:sResidential];
    NSString *sRetail = [[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.retail"];
    [_oLblGoRetail setText:sRetail];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblGoHotels, _oLblGoIndustrial, _oLblGoOffices, _oLblGoResidential, _oLblGoRetail, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.firstMenu.navigation.title"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
