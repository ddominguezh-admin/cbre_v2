//
//  ResearchViewController.m
//  CBRE
//
//  Created by ddominguezh on 28/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ResearchViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "ButtonCityCell.h"
#import "DocumentDao.h"

@interface ResearchViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;

@end

@implementation ResearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    if (_aCities.count > 4){
        [_oTblCitiesMarketViews setScrollEnabled:TRUE];
        [_oTblCitiesSnapShots setScrollEnabled:TRUE];
    }
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnMarket, _oBtnSnap, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.first.title"] andSubtitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.periodicalsPresentations.title"]];
    [super loadMenuButtonsBack:self andIdMenu:3];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *sTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.periodicalsPresentations.title"];
    [_oLblTitle setText:sTitle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aCities.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"ButtonCityCell";
    ButtonCityCell *oButtonCityCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer forIndexPath:indexPath];
    CityBean *oCity = [_aCities objectAtIndex:indexPath.row];
    oButtonCityCell.oCity = oCity;
    [oButtonCityCell.oLblName setText:oCity.sName];
    
    [[oButtonCityCell contentView] setBackgroundColor:[UIColor clearColor]];
    [[oButtonCityCell backgroundView] setBackgroundColor:[UIColor clearColor]];
    [oButtonCityCell setBackgroundColor:[UIColor clearColor]];
    [CBREFont setFontInLabel:oButtonCityCell.oLblName withType:CBREFontTypeHelveticaNeue57Condensed];
    return oButtonCityCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonCityCell *oButtonCityCell = (ButtonCityCell *)[tableView cellForRowAtIndexPath:indexPath];
    _oCity = oButtonCityCell.oCity;
    
    NSArray<DocumentBean> *aDocuments = (NSArray<DocumentBean> *)[[NSArray alloc] init];
    NSString *sTitleScreen = @"";

    if ([tableView isEqual:_oTblCitiesMarketViews]){
        aDocuments = [_oCity getDocumentsByType:DocumentCityMarket];
        sTitleScreen = @"MARKET";
    }else{
        aDocuments = [_oCity getDocumentsByType:DocumentCitySnapshot];
        sTitleScreen = @"SNAPSHOT";
    }

    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:sTitleScreen sSubTitleScreen:_oCity.sName sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.first.title"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.periodicalsPresentations.title"], sTitleScreen] sBackground:@"Fondo_Imagen_Research" nIdMenu:3];
    
    if (oPDFView != NULL)
        [self.navigationController pushViewController:oPDFView animated:true];
}

@end
