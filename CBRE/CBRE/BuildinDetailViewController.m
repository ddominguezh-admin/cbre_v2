//
//  BuildinDetailViewController.m
//  CBRE
//
//  Created by Victor Hugo Aliaga on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildinDetailViewController.h"
#import "BuildingDao.h"
#import "DocumentBean.h"
#import "PlantsCell.h"
#import "CommunicationCell.h"
#import "DocumentDao.h"
#import "CBREPDFReader.h"
#import "BuildingDetailMapViewController.h"
#import "UIColor+CBREHexColor.h"
#import "CBRELocalized.h"
#import "PhotoCollectionCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface BuildinDetailViewController ()

@end

@implementation BuildinDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        _oBuilding = [[BuildingDao sharedInstance] getBuildingByID:_oBuilding.nIdBuilding];
        _aPhotos = [_oBuilding getPhotos];
        _aPlants = [_oBuilding getPlants];
        _aCommunications = [_oBuilding getCommunications];
        _aDocuments = [[DocumentDao sharedInstance] getDocumentsByBuilding:_oBuilding.nIdBuilding andType:DocumentBuildingAgencyDetailFile];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_oCblPhotos reloadData];
            [_oTblCommunications reloadData];
            [_oTblPlants reloadData];
            [_oWebDescription loadHTMLString:_oBuilding.sDescriptionHtml baseURL:nil];
            
            [_oActivityIndicator setHidden:TRUE];
            [_oActivityIndicator stopAnimating];
        });
        
    });
    
    
    [CBREUtils hideBackgroundWebView:_oWebDescription];
    self.lblName.text = [_oBuilding.sName uppercaseString];
    self.lblCity.text = [_oCity.sName uppercaseString];
    self.lbDescription.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.descripcion"] uppercaseString];
    self.lblImages.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.imagenes"] uppercaseString];
    self.lblSurface.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.superficie"] uppercaseString];
    self.lblPlane.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.plano"] uppercaseString];
    self.lblComunication.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.comunicacion"] uppercaseString];
    self.lblLocalization.text = [[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.localizacion"] uppercaseString];

    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:self.lblName, nil] withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:self.lblCity, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    [CBREFont setFontInLabels:[NSArray arrayWithObjects:self.lbDescription,self.lblImages,self.lblPlane,self.lblLocalization,self.lblSurface,self.lblComunication,nil] withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName, _oBuilding.sName]];
    [super loadMenuButtonsBack:self andIdMenu:2];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)showPlaneBuilding:(id)sender{
    UIViewController *oPDFDetail = [CBREUtils obtainViewControlerWithDocuments:[_oBuilding getPlane] oStoryBoard:self.storyboard sTitleScreen:@"Plano" sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@ / %@ / %@ /%@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName, _oBuilding.sName, @"Plano"] sBackground:@"Luto" nIdMenu:2];
    if (oPDFDetail != NULL)
        [self.navigationController pushViewController:oPDFDetail animated:true];
}

#pragma mark collection view delegate functions

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"PhotoCollectionCell";
    PhotoCollectionCell *oCollectionCell = (PhotoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentificer forIndexPath:indexPath];
    
    if (indexPath.row < _aPhotos.count) {
        
        PhotoBean *oPhoto = [_aPhotos objectAtIndex:indexPath.row];
        oCollectionCell.oPhoto = oPhoto;
        
        NSURL *oURLImage = [NSURL URLWithString:oPhoto.sImgURL];
        [oCollectionCell.oImgPhoto setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@"ImgNotPhoto"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oCollectionCell.oImgPhoto.image andInsertIntUIImageView:oCollectionCell.oImgPhoto];
        
    }else{
        [oCollectionCell.oImgPhoto setImage: [UIImage imageNamed:@"ImgNotPhoto"]];
    }
    
    return oCollectionCell;
}

#pragma mark table view delegate functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    long nRows = 0;
    if ([tableView isEqual:_oTblCommunications]) {
        nRows = _aCommunications.count;
    }else{
        nRows = _aPlants.count + 2;
    }
    return nRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:_oTblCommunications]) {
        
        NSString *sCellIdentifier = @"CommunicationCell";
        CommunicationCell *oCommunicationCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        CommunicationBean *oCommunication = [_aCommunications objectAtIndex:indexPath.row];
        [oCommunicationCell.oImgType setContentMode:UIViewContentModeScaleAspectFit];
        [oCommunicationCell.oImgType setImage:[UIImage imageNamed:oCommunication.sImgMain]];
        [oCommunicationCell.oLblName setText:oCommunication.sName];
        [oCommunicationCell.oLblDescription setText:oCommunication.sDetail];
        
        [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oCommunicationCell.oLblName, oCommunicationCell.oLblDescription, nil] withType:CBREFontTypeHelveticaRegular];
        
        [oCommunicationCell setBackgroundColor:[UIColor clearColor]];
        [oCommunicationCell.contentView setBackgroundColor:[UIColor clearColor]];
        return oCommunicationCell;
        
    }else if([tableView isEqual:_oTblPlants]){
        NSString *sCellIdentifier = @"PlantsCell";
        PlantsCell *oPlantCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        
        if (_aPlants.count > indexPath.row) {
        
            PlantBean *oPlant = [_aPlants objectAtIndex:indexPath.row];
            [oPlantCell.oLblPlant setText:[NSString stringWithFormat:@"%@", oPlant.nPlant]];
            [oPlantCell.oLblMeters setText:[NSString stringWithFormat:@"%@ m2", oPlant.nMeters]];
            [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oPlantCell.oLblMeters, oPlantCell.oLblPlant, nil] withType:CBREFontTypeHelveticaRegular];

            if (indexPath.row % 2 == 0) {
                [oPlantCell.contentView setBackgroundColor:[UIColor colorWithRed:96.0/255.0 green:163.0/255.0 blue:132.0/255.0 alpha:1]];
            }else{
                [oPlantCell.contentView setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:150.0/255.0 blue:111.0/255.0 alpha:1]];
            }
        }else{
            [oPlantCell.oLblPlant setText:@""];
            [oPlantCell.oLblMeters setText:@""];
            
            if (_aPlants.count % 2 == 0) {
                [oPlantCell.contentView setBackgroundColor:[UIColor colorWithRed:96.0/255.0 green:163.0/255.0 blue:132.0/255.0 alpha:1]];
            }else{
                [oPlantCell.contentView setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:150.0/255.0 blue:111.0/255.0 alpha:1]];
            }
        }
        
        return oPlantCell;
    }
    
    return NULL;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if([tableView isEqual:_oTblPlants]){
        UIView *oHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 236.0, 60)];
        [oHeaderView setBackgroundColor:[[UIColor alloc] initWhitHexadecimalColor:@"99CCA6" andAlpha:1.0]];
    
        UIColor *oTextColor =[UIColor colorWithRed:49.0/255.0 green:85.0/255.0 blue:63.0/255.0 alpha:1];
    
        UILabel *oLblPlant = [[UILabel alloc] initWithFrame:CGRectMake(10, 21, 68, 21)];
        [oLblPlant setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.planta"]];
        [oLblPlant setTextColor:oTextColor];
        [oLblPlant setFont:[UIFont fontWithName:@"" size:15]];
    
        UILabel *oLblBarra = [[UILabel alloc] initWithFrame:CGRectMake(81, 0, 1, 60)];
        [oLblBarra setBackgroundColor:oTextColor];
    
    
        UILabel *oLblSuperficie = [[UILabel alloc] initWithFrame:CGRectMake(90, 21, 134, 21)];
        [oLblSuperficie setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.superficie"]];
        [oLblSuperficie setTextColor:oTextColor];
        [oLblSuperficie setFont:[UIFont fontWithName:@"" size:15]];
        [oLblSuperficie setTextAlignment:NSTextAlignmentRight];
    
        [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oLblPlant, oLblSuperficie, nil] withType:    CBREFontTypeHelveticaBold];
    
        UILabel *oLblSeparador = [[UILabel alloc] initWithFrame:CGRectMake(0, 59, 236, 1)];
        [oLblSeparador setBackgroundColor:oTextColor];
    
        [oHeaderView addSubview:oLblPlant];
        [oHeaderView addSubview:oLblBarra];
        [oHeaderView addSubview:oLblSuperficie];
        [oHeaderView addSubview:oLblSeparador];
    
        return oHeaderView;
    }
    return NULL;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if([tableView isEqual:_oTblPlants]){
        UIView *oFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 236.0, 25)];
    
        if (_aPlants.count % 2 == 0) {
            [oFooterView setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:150.0/255.0 blue:111.0/255.0 alpha:1]];
        }else{
            [oFooterView setBackgroundColor:[UIColor colorWithRed:96.0/255.0 green:163.0/255.0 blue:132.0/255.0 alpha:1]];
        }

        UILabel *oLblTotal = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 68, 21)];
        [oLblTotal setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agency.detail.total"]];
        [oLblTotal setTextColor:[UIColor whiteColor]];
        [oLblTotal setFont:[UIFont fontWithName:@"" size:15]];
    
        UILabel *oLblTotalNumber = [[UILabel alloc] initWithFrame:CGRectMake(90, 2, 134, 21)];
        
        long nLoopPlants = 0, nCountPlants = _aPlants.count;
        double nTotalMeters = 0.0;
        PlantBean *oPlant = NULL;
        for (; nLoopPlants < nCountPlants; nLoopPlants++) {
            oPlant = [_aPlants objectAtIndex:nLoopPlants];
            nTotalMeters += oPlant.nMeters.doubleValue;
        }
        
        [oLblTotalNumber setText:[NSString stringWithFormat:@"%.2f m2", nTotalMeters]];
        [oLblTotalNumber setTextColor:[UIColor whiteColor]];
        [oLblTotalNumber setFont:[UIFont fontWithName:@"" size:15]];
        [oLblTotalNumber setTextAlignment:NSTextAlignmentRight];
        [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oLblTotal, oLblTotalNumber, nil] withType:CBREFontTypeHelveticaBold];
    
        UILabel *oLblSeparador = [[UILabel alloc] initWithFrame:CGRectMake(0, 24, 236, 1)];
        [oLblSeparador setBackgroundColor:[UIColor colorWithRed:49.0/255.0 green:85.0/255.0 blue:63.0/255.0 alpha:1]];
    
        [oFooterView addSubview:oLblTotal];
        [oFooterView addSubview:oLblTotalNumber];
        [oFooterView addSubview:oLblSeparador];
        return oFooterView;
    }
    return NULL;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    _scrollBackground.contentOffset = CGPointMake(_scrollBackground.contentOffset.x, 0);
}

- (IBAction)sendMailBuilding:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        [mail setSubject:_oBuilding.sName];
        [mail setMessageBody: @"" isHTML:NO];
        [mail setToRecipients:@[ _oBuilding.sEmail ]];
        
        long nLoopDocuments = 0, nCountDocuments = _aDocuments.count;
        DocumentBean *oDocument = NULL;
        for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
            oDocument = [_aDocuments objectAtIndex:nLoopDocuments];
            NSData *oDataFile = [CBREPDFReader loadNSDataPdf:oDocument];
            [mail addAttachmentData:oDataFile mimeType:oDocument.sMimeType fileName:[NSString stringWithFormat:@"%@.%@", oDocument.sName, oDocument.sExtension]];
        }
        
        mail.mailComposeDelegate = self;
        [self presentViewController:mail animated:YES completion:NULL];
    }
    
}

#pragma mark - MFMailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Correo enviado correctamente.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Envío de correo fallido: %@", error.description);
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Envío de correo cancelado.");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)showPdfCaseComplete:(id)sender {
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:_aDocuments oStoryBoard:self.storyboard sTitleScreen:_oBuilding.sName sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"]  sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName, _oBuilding.sName] sBackground:@"EnergyFondo" nIdMenu:2];
    if (oPDFView) {
        [self.navigationController pushViewController:oPDFView animated:true];
    }
}

- (IBAction)showAlbum:(id)sender {
    AlbumCollectionForm *oAlbumForm = [[AlbumCollectionForm alloc] init];
    oAlbumForm.aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] initWithArray:[_oBuilding getPhotos]];
    AlbumPhotoViewController *oAlbumView = [CBREUtils obtainAlbumPhotoViewControlerWithDocuments:oAlbumForm oStoryBoard:self.storyboard sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"]  sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.busqueda"], _oCity.sName, _oArea.sName, _oBuilding.sName] nIdMenu:2];
    [self.navigationController pushViewController:oAlbumView animated:true];
}

- (IBAction)showBuildingInsideMap:(id)sender {
    [self performSegueWithIdentifier:@"gotoDetailMap" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoDetailMap" isEqualToString:segue.identifier]) {
        BuildingDetailMapViewController *oBuildingDetailMapViewController = (BuildingDetailMapViewController *)segue.destinationViewController;
        oBuildingDetailMapViewController.oBuilding = _oBuilding;
        oBuildingDetailMapViewController.oCity = _oCity;
        oBuildingDetailMapViewController.oArea = _oArea;
    }
}

@end
