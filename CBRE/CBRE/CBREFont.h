//
//  CBREFont.h
//  CBRE
//
//  Created by ddominguezh on 02/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBREFont : NSObject

typedef enum {
    CBREFontTypeHelveticaNeue57Condensed = 1,
    CBREFontTypeHelveticaNeue77BoldCondensed = 2,
    CBREFontTypeHelveticaNeue47LightCondensed = 3,
    CBREFontTypeHelveticaNeue67Medium = 4,
    CBREFontTypeHelveticaNeue87HeavyCondensed = 5,
    CBREFontTypeHelveticaRegular = 6,
    CBREFontTypeHelveticaBold = 7
} CBREFontTypes;

+ (NSString *)obtainFontNameWithType:(CBREFontTypes) oFontType;
+ (CBREFontTypes)obtainFontTypeWithText:(NSString *) oFontName;
+ (void)setFontInLabels:(NSArray *) aLabels withType:(CBREFontTypes) oFonType;
+ (void)setFontInLabel:(UILabel *) oLabel withType:(CBREFontTypes) oFonType;
+ (void)setFontInTextViews:(NSArray *) aTextViews withType:(CBREFontTypes) oFonType;
+ (void)setFontInTextView:(UITextView *) oTextView withType:(CBREFontTypes) oFonType;
+ (void)setFontInButtons:(NSArray *) aButtons withType:(CBREFontTypes) oFonType;
+ (void)setFontInButton:(UIButton *) oButton withType:(CBREFontTypes) oFonType;


@end
