//
//  CBREAreaDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "AreaBean.h"
#import "AreaPointBean.h"
#import "FilterMapClientForm.h"
#import "FilterMapForm.h"

@interface AreaDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity;
- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity andFilterClientForm:(FilterMapClientForm *) oFilterMap;
- (NSArray<AreaBean> *)getAreasByIdCity:(NSNumber *) nIdCity andFilterForm:(FilterMapForm *) oFilterMap;

@end
