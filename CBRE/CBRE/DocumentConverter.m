//
//  DocumentConverter.m
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentConverter.h"

@implementation DocumentConverter

+ (NSArray<DocumentBean> *) jsonToDocuments:(NSDictionary *) oData{
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopDocuments = 0, nCountDocuments = (long)oResult.count;
    NSDictionary *oDataDocument = NULL;
    
    for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
        oDataDocument = [oResult objectAtIndex:nLoopDocuments];
        [aDocuments addObject:[DocumentConverter jsonToDocument:oDataDocument]];
    }
    
    return aDocuments;
}

+ (NSArray<DocumentBean> *) arrayToDocuments:(NSArray *) oData{
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    
    long nLoopDocuments = 0, nCountDocuments = (long)oData.count;
    NSDictionary *oDataDocument = NULL;
    
    for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
        oDataDocument = [oData objectAtIndex:nLoopDocuments];
        [aDocuments addObject:[DocumentConverter jsonToDocument:oDataDocument]];
    }
    
    return aDocuments;
}

+ (DocumentBean *) jsonToDocument:(NSDictionary *) oData{
    NSNumber *nIdDocument = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    NSString *sPathURL = [oData objectForKey:@"path"];
    NSNumber *nImgType = [oData objectForKey:@"document_image_main"];
    NSNumber *nIdDocumentType = [oData objectForKey:@"document_type"];
    return [[DocumentBean alloc] initWhitParameters:nIdDocument sName:sName sPathURL:sPathURL nImgType:nImgType.intValue nIdDocumentType:nIdDocumentType];
}

@end
