//
//  SectorBean.m
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SectorBean.h"

@implementation SectorBean

- (id)initWhitParameters:(NSNumber *) nIdSector
                     sName:(NSString *) sName{
    if (self == NULL)
        self = [super init];
    self.nIdSector = nIdSector;
    self.sName = [[CBRELocalized sharedInstance] getMessage:sName];
    return self;
}

@end
