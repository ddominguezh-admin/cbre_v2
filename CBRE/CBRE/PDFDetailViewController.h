//
//  PDFDetailViewController.h
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MessageUI/MessageUI.h>
@class PDFDetailForm;

@interface PDFDetailViewController : CBRENavigationViewController <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) PDFDetailForm *oDetailForm;

@property (weak, nonatomic) IBOutlet UIImageView *oImgBackground;
@property (weak, nonatomic) IBOutlet UIWebView *oWebPdfViewer;
@property (weak, nonatomic) IBOutlet UILabel *oLblPdfName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;

- (IBAction)sendEmail:(id)sender;

@end
