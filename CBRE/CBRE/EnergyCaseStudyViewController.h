//
//  EnergyCaseStudyViewController.h
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"

@interface EnergyCaseStudyViewController : CBRENavigationViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsStudy;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsDisenyo;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsConstruction;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsOperation;
@property (strong, nonatomic) NSArray<DocumentBean> *aDocumentsCertification;

@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UICollectionView *oClvDocuments;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oConstClvDocsPosX;

@property (weak, nonatomic) IBOutlet UIButton *oBtnAllCases;
@property (weak, nonatomic) IBOutlet UIButton *oBtnStudy;
@property (weak, nonatomic) IBOutlet UIButton *oBtnDisenyo;
@property (weak, nonatomic) IBOutlet UIButton *oBtnConstruccion;
@property (weak, nonatomic) IBOutlet UIButton *oBtnOperacion;
@property (weak, nonatomic) IBOutlet UIButton *oBtnCertificacion;

- (IBAction)previousDocument:(id)sender;
- (IBAction)nextDocument:(id)sender;
- (IBAction)viewPdf:(id)sender;
- (IBAction)filterAllCases:(id)sender;
- (IBAction)filterStudies:(id)sender;
- (IBAction)filterDisenyos:(id)sender;
- (IBAction)filterConstruction:(id)sender;
- (IBAction)filterOperation:(id)sender;
- (IBAction)filterCertification:(id)sender;

@end
