//
//  UIColor+CBREHexColor.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CBREHexColor)

- (id)initWhitHexadecimalColor:(NSString *) sHex andAlpha:(CGFloat ) alpha;
+ (unsigned)calculateRgbValueByHexColor:(NSString *) sHexColor;

@end
