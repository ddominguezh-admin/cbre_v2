//
//  CommunicationBean.m
//  CBRE
//
//  Created by ddominguezh on 21/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CommunicationBean.h"

@implementation CommunicationBean

- (id)initWhitParameters:(NSString *) sName
                 sDetail:(NSString *) sDetail
      nCommunicationType:(NSNumber *) nCommunicationType{
    if (self == NULL)
        self = [super init];
    self.sName = sName;
    self.sDetail = sDetail;
    
    self.sImgMain = [CommunicationBean obtainImageCommunicationType:nCommunicationType];
    
    
    
    return self;
}

+ (NSString *)obtainImageCommunicationType:(NSNumber *) nCommunicationType{
    switch (nCommunicationType.intValue) {
        case 1:
            return @"Icon_Bus";
        case 2:
            return @"Icon_Metro";
        case 3:
            return @"Icon_Coche";
        case 4:
            return @"Icon_Coche";
        case 5:
            return @"Icon_Avion";
        case 6:
            return @"Icon_Tren";
        default:
            return @"";
            
    }
}

@end
