//
//  CustomerDao.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AbstractDao.h"
#import "ClientBean.h"
#import "FilterMapClientForm.h"
#import "PhotoBean.h"

@interface ClientDao : AbstractDao

+ (instancetype)sharedInstance;
- (NSArray<ClientBean> *)getClientsByArea:(NSNumber *) nIdArea;
- (NSArray<ClientBean> *)getClientsByArea:(NSNumber *) nIdArea andFilterForm:(FilterMapClientForm *)oFilterMap;
- (NSArray<ClientBean> *)getAllClientsName;
- (NSArray<ClientBean> *)getAllClientsWorkplaceName;
- (void)fillFullDataCustomer:(ClientBean **) oClient;
- (NSArray<PhotoBean> *)getPhotosByClient:(NSNumber *) nIdClient;
- (NSArray<ClientBean> *)getClientsDocumentsWorkplace;

@end
