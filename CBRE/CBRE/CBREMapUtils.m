//
//  CBREMapUtils.m
//  CBRE
//
//  Created by ddominguezh on 02/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBREMapUtils.h"

@implementation CBREMapUtils

+ (MKCoordinateRegion)makeRegionByCoordinate:(CLLocationCoordinate2D) oCordinate andZoomLevel:(NSNumber *) nZoom{
    CLLocationDistance oDiscanteLevel = 1000 * (15 - nZoom.intValue);
    return MKCoordinateRegionMakeWithDistance(oCordinate, oDiscanteLevel, oDiscanteLevel);
}

+ (MKCoordinateRegion)makeRegionSpanishCoordinate{
    return MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(40.419, -3.7072), 950000, 950000);
}

+ (MKCoordinateRegion)makeRegionEuropaCoordinate{
    return MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(50.419, 14.7072), 3750000, 3750000);
}

+ (void)removeAllAnnotations:(MKMapView *) oMap{
    NSArray *aAnnotations = oMap.annotations;
    long nLoopAnnotations = 0, nCountAnnotations = (long)aAnnotations.count;
    for (; nLoopAnnotations < nCountAnnotations; nLoopAnnotations++) {
        [oMap removeAnnotation:[aAnnotations objectAtIndex:nLoopAnnotations]];
    }
}

@end
