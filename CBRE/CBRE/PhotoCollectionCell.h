//
//  PhotoCollectionCell.h
//  CBRE
//
//  Created by ddominguezh on 09/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PhotoBean;

@interface PhotoCollectionCell : UICollectionViewCell

@property (strong, nonatomic) PhotoBean *oPhoto;

@property (weak, nonatomic) IBOutlet UILabel *oLblBackground;
@property (weak, nonatomic) IBOutlet UIImageView *oImgPhoto;

@end
