//
//  BuildingBean.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingBean.h"
#import "BuildingDao.h"
#import "DocumentDao.h"
#import "DocumentBean.h"

@implementation BuildingBean

- (id)initWhitParameters:(NSNumber *) nIdBuilding
                   sName:(NSString *) sName
               nLatitude:(NSNumber *) nLatitude
              nLongitude:(NSNumber *) nLongitude
                 nMeters:(NSNumber *) nMeters
                 nPlants:(NSNumber *) nPlants
                   nRent:(NSNumber *) nRent
                sAddress:(NSString *) sAddress
                  sPhone:(NSString *) sPhone
                  sEmail:(NSString *) sEmail
                sImgMain:(NSString *) sImgMain
            sDescription:(NSString *) sDescription
        sDescriptionHtml:(NSString *) sDescriptionHtml{
    if (self == NULL)
        self = [super init];
    self.nIdBuilding = nIdBuilding;
    self.sName = sName;
    self.nLatitude = nLatitude;
    self.nLongitude = nLongitude;
    self.nMeters = nMeters;
    self.nPlants = nPlants;
    self.nRent = nRent;
    self.sAddress = sAddress;
    self.sPhone = sPhone;
    self.sEmail = sEmail;
    self.sImgMain = sImgMain;
    self.sDescription = sDescription;
    self.sDescriptionHtml = [NSString stringWithFormat:@"<style type=\"text/css\">ul { padding:0px 8px; margin: 0px 2px 0px 10px; } li{color: rgb(4, 253, 16);} .white {color:white;} .verde {color:rgb(105, 246, 66);} div {float: left; text-align: justify; width: %@} .enter{line-height: 1px; height: 1px; width: %@; float:left;}</style><body style=\"background-color: transparent; font-family:Helvetica Neue; width: 290px;\">%@</body>", @"98%", @"100%", sDescriptionHtml];
    return self;
}

- (NSArray<PhotoBean> *) getPhotos{
    if (_aPhotos == NULL)
        _aPhotos = [[BuildingDao sharedInstance] getPhotosByBuilding:_nIdBuilding];
    return _aPhotos;
}

- (NSArray<PlantBean> *) getPlants{
    return _aPlants;
}

- (NSArray<CommunicationBean> *) getCommunications{
    return _aCommunications;
}

- (NSArray<DocumentBean> *)getPlane{
    return [[DocumentDao sharedInstance] getDocumentsByBuilding:_nIdBuilding andType:DocumentBuildingPlane];
}

@end
