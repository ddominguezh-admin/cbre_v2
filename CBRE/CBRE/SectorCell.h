//
//  SectorCell.h
//  CBRE
//
//  Created by ddominguezh on 12/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectorBean.h"

@interface SectorCell : UITableViewCell

@property (strong, nonatomic) SectorBean *oSector;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
