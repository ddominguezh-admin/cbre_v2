//
//  ClientAnnotation.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientAnnotation.h"
#import "ClientBean.h"

@implementation ClientAnnotation

- (id)initWithClient:(ClientBean *) oClient sPin:(NSString *) sPin{
    if (self == NULL)
        self = [super init];
    self.coordinate = CLLocationCoordinate2DMake(oClient.nLatitude.floatValue, oClient.nLongitude.floatValue);
    self.oClient = oClient;
    self.oImage = [ClientAnnotation getImageInBundle:[NSString stringWithFormat:@"%@.png", oClient.sName.lowercaseString]];
    return self;
}

- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation{
    oPinAnnotation.image = self.oImage;
    oPinAnnotation.enabled = YES;
    oPinAnnotation.canShowCallout = NO;
    oPinAnnotation.tag = self.oClient.nIdClient.intValue;
}

+ (UIImage *)getImageInBundle:(NSString *) sPinURL{
    /*NSString *sPathComplete = [ClientAnnotation getCompletePathImage:sPinURL];
    UIImage *oPinImage = [UIImage imageWithContentsOfFile:sPathComplete];
    if(oPinImage == nil)
        oPinImage = [ClientAnnotation donwloadImageAnsSaveInBundle:sPinURL];
    return oPinImage;*/
    return [UIImage imageNamed:sPinURL];
}

+ (UIImage *) donwloadImageAnsSaveInBundle:(NSString *) sPinURL{
    NSData *oImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:sPinURL]];
    NSString *sPathComplete = [ClientAnnotation getCompletePathImage:sPinURL];
    NSError *oWriteError = nil;
    [oImageData writeToFile:sPathComplete options:NSDataWritingAtomic error:&oWriteError];
    return [UIImage imageWithData:oImageData];
}

+ (NSString *)getCompletePathImage:(NSString *) sPinURL{
    NSString *sPath = [ClientAnnotation getPathBundle];
    return [sPath stringByAppendingFormat:@"/%@", [ClientAnnotation getRealNameImage:sPinURL]];
}

+ (NSString *)getPathBundle{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
    /*
     NSString *sPath = [[NSBundle mainBundle] resourcePath];
    NSRange oLastOccurrencyPole = [sPath rangeOfString:@"/" options:NSBackwardsSearch];
    return [sPath substringToIndex:oLastOccurrencyPole.location];*/
}

+ (NSString *) getRealNameImage: (NSString *) sPinURL{
    NSRange oLastOccurrencyPole = [sPinURL rangeOfString:@"/" options:NSBackwardsSearch];
    return [NSString stringWithFormat:@"%@", [sPinURL substringFromIndex:(oLastOccurrencyPole.location + 1)]];
}

@end
