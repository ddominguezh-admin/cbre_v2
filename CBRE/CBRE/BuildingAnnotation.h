//
//  BuildingAnnotation.h
//  CBRE
//
//  Created by ddominguezh on 10/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
@class BuildingBean;

@interface BuildingAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) UIImage *oImage;
@property (strong, nonatomic) BuildingBean *oBuilding;

- (id)initWithBuilding:(BuildingBean *) oBuilding sPin:(NSString *) sPin;
- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation;

@end
