//
//  RearchMenuViewController.m
//  CBRE
//
//  Created by ddominguezh on 02/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "RearchMenuViewController.h"
#import "MenuCollectionForm.h"
#import "MenuCollection.h"
#import "CityBean.h"
#import "AppDelegate.h"
#import "DocumentDao.h"

@interface RearchMenuViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;

@end

@implementation RearchMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.title"]];
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *sPeriodicalsPresentations = [[CBRELocalized sharedInstance] getMessage:@"i18n.periodicalsPresentations.title"];
    [_oLblPeriodicalsPresentations setText:sPeriodicalsPresentations];
    NSString *sPresentationsSubmarkets = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.presentationsSubmarkets"];
    [_oLblPresentationsSubmarkets setText:sPresentationsSubmarkets];
    NSString *sGlobalGuide = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.globalGuide"];
    [_oLblGlobalGuide setText:sGlobalGuide];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblGlobalGuide, _oLblPeriodicalsPresentations, _oLblPresentationsSubmarkets, _oLblTitle, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showGuiasGlobales:(id)sender {
    NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:DocumentResearchGuiasGlobales];
    [self showDocumentsPresentations:aDocuments];
}

- (void)showDocumentsPresentations:(NSArray<DocumentBean> *) aDocuments{
    NSString *sTitleScreen = _oLblGlobalGuide.text;
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:sTitleScreen sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.first.title"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.presentationsSubmarkets"].uppercaseString, sTitleScreen] sBackground:@"Fondo_Imagen_Research" nIdMenu:3];
    
    if (oPDFView != NULL)
        [self.navigationController pushViewController:oPDFView animated:true];
    
}

@end
