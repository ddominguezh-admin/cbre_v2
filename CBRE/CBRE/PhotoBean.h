//
//  PhotoBean.h
//  CBRE
//
//  Created by ddominguezh on 16/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoBean;

@interface PhotoBean : NSObject

@property (strong, nonatomic) NSNumber *nIdPhoto;
@property (strong, nonatomic) NSString *sImgURL;
@property (strong, nonatomic) NSString *sImgHDURL;

- (id)initWhitParameters:(NSNumber *) nIdPhoto
                 sImgURL:(NSString *) sImgURL
               sImgHDURL:(NSString *) sImgURLHD;

@end
