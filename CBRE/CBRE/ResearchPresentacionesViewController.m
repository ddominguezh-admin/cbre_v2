//
//  ResearchPresentacionesViewController.m
//  CBRE
//
//  Created by ddominguezh on 22/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ResearchPresentacionesViewController.h"
#import "DocumentDao.h"

@interface ResearchPresentacionesViewController ()

@end

@implementation ResearchPresentacionesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblBarcelona, _oLblMadrid, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    _oLblTitle.text = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.presentationsSubmarkets"].uppercaseString;
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.first.title"] andSubtitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.presentationsSubmarkets"]];
    
    [super loadMenuButtonsBack:self andIdMenu:3];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showDocumentMadrid:(id)sender {
    NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:DocumentResearchPresentacionesSubmercadosMadrid];
    [self showDocumentsPresentations:aDocuments andCityName:_oLblMadrid.text];
}

- (IBAction)showDocumentBarcelona:(id)sender {
    NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:DocumentResearchPresentacionesSubmercadosBarcelona];
    [self showDocumentsPresentations:aDocuments andCityName:_oLblBarcelona.text];
}

- (void)showDocumentsPresentations:(NSArray<DocumentBean> *) aDocuments andCityName:(NSString *) sCityName{
    NSString *sTitleScreen = sCityName;
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:sTitleScreen sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.navigation.first.title"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.menuResearch.presentationsSubmarkets"].uppercaseString, sTitleScreen] sBackground:@"Fondo_Imagen_Research" nIdMenu:3];
    
    if (oPDFView != NULL)
        [self.navigationController pushViewController:oPDFView animated:true];
    
}

@end
