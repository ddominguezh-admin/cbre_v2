//
//  DocumentBean.h
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DocumentBean;

@interface DocumentBean : NSObject

@property (strong, nonatomic) NSNumber *nIdDocument;
@property (strong, nonatomic) NSString *sName;
@property (strong, nonatomic) NSString *sPathURL;
@property (strong, nonatomic) NSString *sExtension;
@property (strong, nonatomic) NSString *sImgName;
@property (strong, nonatomic) NSNumber *nIdDocumentType;
@property (strong, nonatomic) NSString *sMimeType;
@property (strong, nonatomic) NSNumber *nIdOwner;
@property (strong, nonatomic) NSString *sOwnerName;

- (id)initWhitParameters:(NSNumber *) nIdDocument
                   sName:(NSString *) sName
                sPathURL:(NSString *) sPathURL
                nImgType:(int) nImgType
         nIdDocumentType:(NSNumber *) nIdDocumentType;

@end
