//
//  PDFCollectionViewController.m
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PDFCollectionViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionCell.h"

@interface PDFCollectionViewController ()

@property (strong, nonatomic) NSArray<DocumentBean> *aDocuments;
@property (strong, nonatomic) DocumentBean *oDocumentSelected;

@property int indexPathSelected;

@end

@implementation PDFCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    [CBREFont setFontInLabel:_oLblSubtitleScreen withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    
    [_oImgBackground setImage:[UIImage imageNamed:_oCollectionForm.sBackground]];
    [_oLblNameScreen setText:_oCollectionForm.sTitleScreen];
    _aDocuments = _oCollectionForm.aDocuments;
    _indexPathSelected = 1;
    if (_oCollectionForm.sSubTitleScreen != NULL) {
        [_oLblSubtitleScreen setText:_oCollectionForm.sSubTitleScreen];
    }else{
        [_oLblSubtitleScreen setHidden:TRUE];
    }
    
    long nCountDocuments = _aDocuments.count;
    if (nCountDocuments < 4) {
        [_oBtnPrevious setHidden:TRUE];
        [_oBtnNext setHidden:TRUE];
        if (nCountDocuments < 3)
            [self calculateFrameCollectionView:nCountDocuments];
    }

    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:_oCollectionForm.sTitleNavBar andSubtitle:_oCollectionForm.sSubTitleNavBar];
    
    [super loadMenuButtonsBack:self andIdMenu:_oCollectionForm.nIdMenu];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)calculateFrameCollectionView:(long) nCountDocuments{
    CGRect oFrame = _oClvDocuments.frame;
    float nWidth = oFrame.size.width - ((oFrame.size.width / 3) * nCountDocuments);
    _oConstClvDocsWidth.constant = oFrame.size.width - nWidth;
    _oConstClvDocsPosX.constant = (oFrame.origin.x - 99) + (nWidth / 2);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection view delegate functions

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aDocuments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"PDFCollectionCell";
    PDFCollectionCell *oPDFCollectionCell = (PDFCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    DocumentBean *oDocument = [_aDocuments objectAtIndex:indexPath.row];
    oPDFCollectionCell.oDocument = oDocument;
    [oPDFCollectionCell.oBtnPdf setImage:[UIImage imageNamed:oDocument.sImgName] forState:UIControlStateNormal];
    [oPDFCollectionCell.oBtnPdf setTag:indexPath.row];
    [oPDFCollectionCell.oBtnPdf addTarget:self action:@selector(viewPdf:) forControlEvents:UIControlEventTouchDown];
    [oPDFCollectionCell.oLblName setText:oDocument.sName];
    return oPDFCollectionCell;
}

- (IBAction)previousDocument:(id)sender {
    BOOL bCanMove = _indexPathSelected > 1;
    if (bCanMove) {
        _indexPathSelected--;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)nextDocument:(id)sender {
    long nLastStep = _aDocuments.count - 2;
    BOOL bCanMove = _indexPathSelected < nLastStep;
    if (bCanMove) {
        _indexPathSelected++;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)viewPdf:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    _oDocumentSelected = [_aDocuments objectAtIndex:oButton.tag];
    [self performSegueWithIdentifier:@"gotoDetailPdf" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoDetailPdf" isEqualToString:segue.identifier]) {
        PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)segue.destinationViewController;
        PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
        oDetailForm.nIdMenu = _oCollectionForm.nIdMenu;
        oDetailForm.sTitleNavBar = _oCollectionForm.sTitleNavBar;
        oDetailForm.sSubTitleNavBar = _oCollectionForm.sSubTitleNavBar;
        oDetailForm.sBackground = _oCollectionForm.sBackground;
        oDetailForm.oDocument = _oDocumentSelected;
        oPdfDetail.oDetailForm = oDetailForm;
    }
}

@end
