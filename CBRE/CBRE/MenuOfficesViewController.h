//
//  MenuOfficesViewController.h
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface MenuOfficesViewController : CBRENavigationViewController


@property (weak, nonatomic) IBOutlet UILabel *oLblOffices;
@property (weak, nonatomic) IBOutlet UILabel *oLblPropietaries;
@property (weak, nonatomic) IBOutlet UILabel *oLblOccupants;
@property (weak, nonatomic) IBOutlet UILabel *oLblResearch;

@end
