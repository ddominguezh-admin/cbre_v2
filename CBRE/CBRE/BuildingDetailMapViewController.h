//
//  BuildingDetailMapViewController.h
//  CBRE
//
//  Created by ddominguezh on 27/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "CBRENavigationViewController.h"
@class CityBean;
@class AreaBean;
@class BuildingBean;

@interface BuildingDetailMapViewController : CBRENavigationViewController<MKMapViewDelegate>

@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) AreaBean *oArea;
@property (strong, nonatomic) BuildingBean *oBuilding;

@property (weak, nonatomic) IBOutlet MKMapView *oMap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;

@end
