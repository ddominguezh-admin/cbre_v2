//
//  PDFDetailViewController.m
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PDFDetailViewController.h"
#import "CBREPDFReader.h"
#import "PDFDetailForm.h"
#import "DocumentBean.h"

@interface PDFDetailViewController ()

@property (strong, nonatomic) DocumentBean *oDocument;

@end

@implementation PDFDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    
    UIImage *image = [[UIImage imageNamed:@"Icon_Mail"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *oEmailButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(sendEmail:)];
    self.navigationItem.rightBarButtonItem = oEmailButton;
    
    [_oImgBackground setImage:[UIImage imageNamed:_oDetailForm.sBackground]];
    _oDocument = _oDetailForm.oDocument;
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        
        NSData *oDataDocument = [CBREPDFReader loadNSDataPdf:_oDocument];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_oWebPdfViewer loadData:oDataDocument MIMEType:_oDocument.sMimeType textEncodingName:@"utf-8" baseURL:nil];
            
            [_oActivityIndicator setHidden:TRUE];
            [_oActivityIndicator stopAnimating];
        });
    });
    
    [_oLblPdfName setText:_oDocument.sName];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:_oDetailForm.sTitleNavBar andSubtitle:_oDetailForm.sSubTitleNavBar];
    [self loadMenuButtonsBack:self andIdMenu:_oDetailForm.nIdMenu];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)sendEmail:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        [mail setSubject:_oDocument.sName];
        [mail setMessageBody: @"" isHTML:NO];
        [mail setToRecipients:@[ @"" ]];
        
        NSData *oDataFile = [CBREPDFReader loadNSDataPdf:_oDocument];
        [mail addAttachmentData:oDataFile mimeType:_oDocument.sMimeType fileName:[NSString stringWithFormat:@"%@.%@", _oDocument.sName, _oDocument.sExtension]];
        
        mail.mailComposeDelegate = self;
        [self presentViewController:mail animated:YES completion:NULL];
    }
}

#pragma mark - MFMailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Correo enviado correctamente.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Envío de correo fallido: %@", error.description);
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Envío de correo cancelado.");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


@end
