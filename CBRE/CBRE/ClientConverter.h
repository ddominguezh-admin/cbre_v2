//
//  ClientConverter.h
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClientBean.h"

@interface ClientConverter : NSObject

+ (NSArray<ClientBean> *) jsonToClients:(NSDictionary *) oData;
+ (NSArray<ClientBean> *) arrayToClients:(NSArray *) oData;
+ (ClientBean *) jsonToClient:(NSDictionary *) oData;

@end
