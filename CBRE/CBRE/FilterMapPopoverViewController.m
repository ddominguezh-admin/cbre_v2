//
//  FilterMapPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 06/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FilterMapPopoverViewController.h"
#import "FilterMapForm.h"
#import "CBRELocalized.h"
@interface FilterMapPopoverViewController ()

@property bool bFirstSelected;

@end

@implementation FilterMapPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblRent, _oLblPlants, _oLblMeters, nil] withType:CBREFontTypeHelveticaNeue67Medium];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblTitleInmediated, _oLblTitleMeters, _oLblTitlePlants, _oLblTitleRent, _oBtnFilter.titleLabel, nil] withType:CBREFontTypeHelveticaNeue67Medium];
    [CBREFont setFontInLabels:self.oLblTitles withType:CBREFontTypeHelveticaNeue67Medium];
    
    _oLblTitlePlants.text= [[[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.plantas"] capitalizedString];
    _olblPlantsdesde.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.desde"];
    _olblPlantsHasta.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.hasta"];
    _olblM2desde.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.desde"];
    _olblM2hasta.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.hasta"];
    _olblVenta.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.venta"];
    _olblAlquiler.text= [[CBRELocalized sharedInstance] getMessage:@"i18n.popover.type.alquiler"];
    _bFirstSelected = true;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_bFirstSelected) {
        
        _oSwtInmediate.on = _oFilterMap.bInmediatedLocal;
        
        _oSwtMeters.on = _oFilterMap.bFilterByMeters;
        [self enableMeters:_oSwtMeters];
        if (_oFilterMap.bFilterByMeters) {
            _oSldMeters.value = _oFilterMap.nMeters.floatValue/5000;
            _oS2dMeters.value = _oFilterMap.nMeters2.floatValue/5000;
        }
        
        _oSwtPlants.on = _oFilterMap.bFilterByPlants;
        [self enablePlants:_oSwtPlants];
        if (_oFilterMap.bFilterByMeters) {
            _oSldPlants.value = _oFilterMap.nPlants.floatValue/40;
            _oS2dPlants.value = _oFilterMap.nPlants2.floatValue/40;
        }
        
        _oBtnAlq.selected = _oFilterMap.nRentType.intValue == 1;
        _oBtnVent.selected = _oFilterMap.nRentType.intValue == 2;
        
        _bFirstSelected = false;
    }
    [self setValueMeters:_oSldMeters];
    [self setValueMeters2:_oS2dMeters];
    [self setValuePlants:_oSldPlants];
    [self setValuePlants2:_oS2dPlants];
    [self setValueRent:_oSldRent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
    self.view.superview.alpha = 0.85;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filterMap:(id)sender {
    FilterMapForm *oFilterMap = [[FilterMapForm alloc] init];
    oFilterMap.bInmediatedLocal = _oSwtInmediate.on;
    oFilterMap.bFilterByMeters = _oSwtMeters.on;
    oFilterMap.bFilterByPlants = _oSwtPlants.on;
    oFilterMap.bFilterByRent = _oSwtRent.on;
    oFilterMap.nMeters = [NSNumber numberWithInt:_oSldMeters.value*5000];
    oFilterMap.nMeters2 = [NSNumber numberWithInt:_oS2dMeters.value*5000];
    oFilterMap.nPlants = [NSNumber numberWithInt:_oSldPlants.value*40];
    oFilterMap.nPlants2 = [NSNumber numberWithInt:_oS2dPlants.value*40];
    if (_oBtnAlq.selected) {
        oFilterMap.bFilterByRentType = YES;
        oFilterMap.nRentType = [NSNumber numberWithInt:1];
    }else if (_oBtnVent.selected){
        
        oFilterMap.bFilterByRentType = YES;
        oFilterMap.nRentType = [NSNumber numberWithInt:2];
    }else{
        
        oFilterMap.bFilterByRentType = NO;
        oFilterMap.nRentType = [NSNumber numberWithInt:0];
    }
    //oFilterMap.nRent = [NSNumber numberWithInt:_oSldRent.value*1000];
    if (_delegate != NULL)
        [_delegate hidePopoverAndFilterMap:oFilterMap];
}

- (IBAction)changeMeters:(id)sender {
    UISlider *oSlider = (UISlider *)sender;
    [self setValueMeters:oSlider];
}

- (void)setValueMeters:(UISlider *)oSlider{
    [_oLblMeters setText:[NSString stringWithFormat:@"%d",  (int)(oSlider.value*5000)]];
    CGRect trackRect = [oSlider trackRectForBounds:oSlider.bounds];
    CGRect thumbRect = [oSlider thumbRectForBounds:oSlider.bounds
                                            trackRect:trackRect
                                                value:oSlider.value];
    
    _oLblMeters.center = CGPointMake(thumbRect.origin.x + 15 + oSlider.frame.origin.x,  oSlider.frame.origin.y - 7);
}

- (IBAction)changeMeters2:(id)sender {
    UISlider *oSlider = (UISlider *)sender;
    [self setValueMeters2:oSlider];
}

- (void)setValueMeters2:(UISlider *)oSlider{
    [_oLb2Meters setText:[NSString stringWithFormat:@"%d",  (int)(oSlider.value*5000)]];
    CGRect trackRect = [oSlider trackRectForBounds:oSlider.bounds];
    CGRect thumbRect = [oSlider thumbRectForBounds:oSlider.bounds
                                         trackRect:trackRect
                                             value:oSlider.value];
    
    _oLb2Meters.center = CGPointMake(thumbRect.origin.x + 15 + oSlider.frame.origin.x,  oSlider.frame.origin.y - 7);
}

- (IBAction)changePlants:(id)sender {
    UISlider *oSlider = (UISlider *)sender;
    [self setValuePlants:oSlider];
}

- (void)setValuePlants:(UISlider *)oSlider{
    [_oLblPlants setText:[NSString stringWithFormat:@"%d",  (int)(oSlider.value*40)]];
    CGRect trackRect = [oSlider trackRectForBounds:oSlider.bounds];
    CGRect thumbRect = [oSlider thumbRectForBounds:oSlider.bounds
                                         trackRect:trackRect
                                             value:oSlider.value];
    
    _oLblPlants.center = CGPointMake(thumbRect.origin.x + 15 + oSlider.frame.origin.x,  oSlider.frame.origin.y - 7);
}

- (IBAction)changePlants2:(id)sender {
    UISlider *oSlider = (UISlider *)sender;
    [self setValuePlants2:oSlider];
}

- (void)setValuePlants2:(UISlider *)oSlider{
    [_oLb2Plants setText:[NSString stringWithFormat:@"%d",  (int)(oSlider.value*40)]];
    CGRect trackRect = [oSlider trackRectForBounds:oSlider.bounds];
    CGRect thumbRect = [oSlider thumbRectForBounds:oSlider.bounds
                                         trackRect:trackRect
                                             value:oSlider.value];

    _oLb2Plants.center = CGPointMake(thumbRect.origin.x + 15 + oSlider.frame.origin.x,  oSlider.frame.origin.y - 7);
}

- (IBAction)changeRent:(id)sender{
    UISlider *oSlider = (UISlider *)sender;
    [self setValueRent:oSlider];
}



- (void)setValueRent:(UISlider *)oSlider{
    [_oLblRent setText:[NSString stringWithFormat:@"%d",  (int)(oSlider.value*5000)]];
    CGRect trackRect = [oSlider trackRectForBounds:oSlider.bounds];
    CGRect thumbRect = [oSlider thumbRectForBounds:oSlider.bounds
                                         trackRect:trackRect
                                             value:oSlider.value];
    
    _oLblRent.center = CGPointMake(thumbRect.origin.x + 15 + oSlider.frame.origin.x,  oSlider.frame.origin.y - 7);
}

- (IBAction)enableMeters:(id)sender{
    UISwitch *oSwitch = (UISwitch *)sender;
    [_oSldMeters setEnabled:oSwitch.on];
    [_oS2dMeters setEnabled:oSwitch.on];
}

- (IBAction)enablePlants:(id)sender{
     UISwitch *oSwitch = (UISwitch *)sender;
    [_oSldPlants setEnabled:oSwitch.on];
    [_oS2dPlants setEnabled:oSwitch.on];
}

- (IBAction)enableRent:(id)sender{
     UISwitch *oSwitch = (UISwitch *)sender;
    [_oSldRent setEnabled:oSwitch.on];
}

- (IBAction)onClickAlq:(id)sender {
    [_oBtnAlq setSelected:!_oBtnAlq.selected];
    [_oBtnVent setSelected:NO];
}

- (IBAction)onClickVent:(id)sender {
    [_oBtnVent setSelected:!_oBtnVent.selected];
    [_oBtnAlq setSelected:NO];
}

@end
