//
//  CityAnnotation.m
//  CBRE
//
//  Created by ddominguezh on 07/09/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CityAnnotation.h"
#import "CityBean.h"

@implementation CityAnnotation

- (id)initWithCity:(CityBean *) oCity{
    if (self == NULL)
        self = [super init];
    self.coordinate = CLLocationCoordinate2DMake(oCity.nLatitude.floatValue, oCity.nLongitude.floatValue);
    self.oCity = oCity;
    self.oImage = [UIImage imageNamed:@"pin_off"];
    return self;
}

- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation{
    oPinAnnotation.image = self.oImage;
    oPinAnnotation.enabled = YES;
    oPinAnnotation.canShowCallout = NO;
    oPinAnnotation.tag = self.oCity.nIdCity.intValue;
}

@end
