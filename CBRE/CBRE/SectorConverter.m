//
//  SectorConverter.m
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "SectorConverter.h"

@implementation SectorConverter

+ (NSArray<SectorBean> *) jsonToSectors:(NSDictionary *) oData{
    NSMutableArray<SectorBean> *aSectors = (NSMutableArray<SectorBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopSectors = 0, nCountSectors = (long)oResult.count;
    NSDictionary *oDataSector = NULL;
    
    for (; nLoopSectors < nCountSectors; nLoopSectors++) {
        oDataSector = [oResult objectAtIndex:nLoopSectors];
        [aSectors addObject:[SectorConverter jsonToSector:oDataSector]];
    }
    
    return aSectors;
}

+ (SectorBean *) jsonToSector:(NSDictionary *) oData{
    NSNumber *nIdSector = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    return [[SectorBean alloc] initWhitParameters:nIdSector sName:sName];
}

@end
