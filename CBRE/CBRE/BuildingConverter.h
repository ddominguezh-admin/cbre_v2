//
//  BuildingConverter.h
//  CBRE
//
//  Created by ddominguezh on 26/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BuildingBean.h"

@interface BuildingConverter : NSObject

+ (NSArray<BuildingBean> *) jsonToBuildings:(NSDictionary *) oData;
+ (BuildingBean *) jsonToBuilding:(NSDictionary *) oData;

@end
