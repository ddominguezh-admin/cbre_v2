//
//  AreaMapViewController.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "CitiesPopoverViewController.h"
#import "AreasPopoverViewController.h"
#import "FilterMapPopoverViewController.h"
#import "BuildingPopoverViewController.h"
#import <MessageUI/MessageUI.h>
#import "SectorListPopoverViewController.h"
@class CityBean;
@class AreaBean;
#import "BuildingBean.h"

@interface AreaMapViewController : CBRENavigationViewController<MFMailComposeViewControllerDelegate, MKMapViewDelegate, CitiesPopoverViewControllerDelegate, AreasPopoverViewControllerDelegate, FilterMapPopoverViewControllerDelegate, BuildingPopoverViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, SectorListPopoverViewControllerDelegate>

@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) AreaBean *oArea;
@property (strong, nonatomic) NSArray<BuildingBean> *aBuildings;
@property (strong, nonatomic) NSMutableArray<BuildingBean> *aBuildingsFilter;
@property (strong, nonatomic) BuildingBean *oBuildingSelected;

@property (strong, nonatomic) NSMutableDictionary *aBuildginsSections;
@property (strong, nonatomic) NSArray *aSections;

@property (weak, nonatomic) IBOutlet MKMapView *oMap;

@property (weak, nonatomic) IBOutlet UILabel *oBtnShowCities;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentCity;
@property (weak, nonatomic) IBOutlet UILabel *oBtnShowAreas;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentArea;
@property (weak, nonatomic) IBOutlet UIButton *oBtnShowFilterMap;

@property (strong, nonatomic) CitiesPopoverViewController *oCitiesController;
@property (strong, nonatomic) UIPopoverController *oCityPopoverController;
@property (strong, nonatomic) AreasPopoverViewController *oAreasController;
@property (strong, nonatomic) UIPopoverController *oAreasPopoverController;
@property (strong, nonatomic) FilterMapPopoverViewController *oFilterMapController;
@property (strong, nonatomic) UIPopoverController *oFilterMapPopoverController;
@property (strong, nonatomic) BuildingPopoverViewController *oBuildingController;
@property (strong, nonatomic) UIPopoverController *oBuildingPopoverController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *oListButton;
@property (weak, nonatomic) IBOutlet UITableView *oTblBuildingMini;
@property (weak, nonatomic) IBOutlet UIControl *oCtnTableSearch;
@property (weak, nonatomic) IBOutlet UITextField *oTxtSearh;
@property (weak, nonatomic) IBOutlet UITableView *oTblBuildings;

@property (weak, nonatomic) IBOutlet UILabel *oLblSectorName;

@property (weak, nonatomic) IBOutlet UIButton *oBtnShowSectors;
@property (weak, nonatomic) IBOutlet UIImageView *oImgContentSector;
@property (strong, nonatomic) SectorBean *oSector;
@property (strong, nonatomic) SectorListPopoverViewController *oSectorController;
@property (strong, nonatomic) UIPopoverController *oSectorPopoverController;

@property (strong, nonatomic) FilterMapForm *oFilterMap;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *oActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *oLblListadoLocales;
@property (weak, nonatomic) IBOutlet UILabel *oLblListadoCity;


- (IBAction)showCities:(id)sender;
- (IBAction)showAreas:(id)sender;
- (IBAction)showFilterMap:(id)sender;
- (IBAction)showOrHideSearch:(id)sender;
- (IBAction)showSectors:(id)sender;

@end
