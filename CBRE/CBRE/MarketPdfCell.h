//
//  MarketPdfCell.h
//  CBRE
//
//  Created by ddominguezh on 28/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentBean.h"

@interface MarketPdfCell : UICollectionViewCell

@property (strong, nonatomic) DocumentBean *oDocument;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPdf;
@property (weak, nonatomic) IBOutlet UIButton *oBtnEmail;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
