//
//  ClientAreaViewController.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientAreaMapViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "AreaBean.h"
#import "ClientAnnotation.h"
#import "ClientPhotoViewController.h"
#import "ClientMiniCell.h"
#import "ClientCell.h"
#import "CBREPopoverBackgroundView.h"
#import "CBREPopoverNotBackgroundView.h"
#import "CBREMapUtils.h"
#import "AreaDao.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ClientAreaMapViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property (strong, nonatomic) NSArray<AreaBean> *aAreas;

@end

@implementation ClientAreaMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    _aAreas = [_oCity getAreas];
    
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblCityName, _oLblAreaName, _oLblSectorName, nil] withType:CBREFontTypeHelveticaNeue47LightCondensed];
    
    [CBREFont setFontInLabel:_oLblListadoCases withType:CBREFontTypeHelveticaNeue57Condensed];
    [_oLblListadoCases setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"].uppercaseString];
    [CBREFont setFontInLabel:_oLblListadoCity withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
    
    UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _oListButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(showListClientsShowsInMap:)];
    _oListButton.tag = 1;
    self.navigationItem.rightBarButtonItem = _oListButton;
    
    [_oTblClients setBackgroundColor:[UIColor clearColor]];
    
    [super loadMenuButtonsBack:self andIdMenu:2];
    
    [self initScreen];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIColor *color = [UIColor whiteColor];
    _oTxtSearh.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.map.placeholder"] attributes:@{NSForegroundColorAttributeName: color}];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _oTxtSearh.leftView = paddingView;
    _oTxtSearh.leftViewMode = UITextFieldViewModeAlways;
    [_oMap becomeFirstResponder];
}

- (void) initScreen{
    [self loadMapView];
    [self paintOverlayAreaSelected];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName]];
    
}

- (void) reloadScreen{
    _oFilterMap.nSector = _oSector.nIdSector;
    [self loadAnnotationsAreas];
    [self updateComboLabels];
    [self getSectionsBuildings];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName]];
}

- (void)getSectionsBuildings{
    NSMutableArray *aSections = [[NSMutableArray alloc] init];
    _aClientSections = [[NSMutableDictionary alloc] init];
    
    long nLoopClients = 0, nCountClients = _aClients.count;
    ClientBean *oClient;
    for ( ; nLoopClients < nCountClients ; nLoopClients++) {
        oClient = [_aClients objectAtIndex:nLoopClients];
        NSString *sSectionName = [oClient.sName substringToIndex:1].uppercaseString;
        
        NSMutableArray<ClientBean> *aClientSection = NULL;
        if ([aSections containsObject:sSectionName]) {
            aClientSection = (NSMutableArray<ClientBean> *)[_aClientSections objectForKey:sSectionName];
        }else{
            aClientSection = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] init];
            [aSections addObject:sSectionName];
        }
        
        [aClientSection addObject:oClient];
        [_aClientSections setObject:aClientSection forKey:sSectionName];
    }
    _aSections = [aSections sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [_oTblClients reloadData];
}

- (void)updateComboLabels{
    [_oLblCityName setText:_oCity.sName];
    [_oLblAreaName setText:_oArea.sName];
    [_oLblSectorName setText:_oSector.sName];
    if (_oSector == NULL) {
        [_oLblSectorName setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.sector.type.todos"].uppercaseString];
    }else{
        [_oLblSectorName setText:_oSector.sName];
    }
    [_oLblListadoCity setText:_oCity.sName.uppercaseString];
}

- (void)loadMapView{
    
    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{

        dispatch_async(dispatch_get_main_queue(), ^{
            [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:CLLocationCoordinate2DMake(_oArea.nLatitude.floatValue, _oArea.nLongitude.floatValue) andZoomLevel:_oArea.nZoom]];

            [self loadAnnotationsAreas];
            [self updateComboLabels];
            [self getSectionsBuildings];
            
            [_oActivityIndicator stopAnimating];
            [_oActivityIndicator setHidden:TRUE];
        });
        
    });
}

- (void)loadAnnotationsAreas{
    [self loadClientAnnotations:[_oArea getClientsByFilterMap:_oFilterMap]];
}

- (void)loadClientAnnotations:(NSArray<ClientBean> *) aClients{
    [CBREMapUtils removeAllAnnotations:_oMap];
    
    _aClients = aClients;
    long nLoopClients = 0, nCountClients = aClients.count;
    ClientBean *oClient = NULL;
    for (; nLoopClients < nCountClients; nLoopClients++) {
        oClient = [aClients objectAtIndex:nLoopClients];
        ClientAnnotation *oAnnotation = [[ClientAnnotation alloc] initWithClient:oClient sPin:_oArea.sPin];
        [_oMap addAnnotation:oAnnotation];
    }
    _aClientsFilter = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] initWithArray:aClients];
    [_oTblClientMini reloadData];
    [_oTxtSearh setText:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showCities:(id)sender {
    if (_oCitiesController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oCitiesController = [oStoryBoard instantiateViewControllerWithIdentifier:@"CitiesPopoverViewController"];
        _oCitiesController.delegate = self;
        _oCitiesController.oCity = _oCity;
    }
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentCity.frame];
    if (_oCityPopoverController == NULL) {
        _oCityPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oCitiesController];
        _oCityPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oCityPopoverController.isPopoverVisible) {
            [_oCityPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)hidePopoverAndSelectedCity:(CityBean *)oCity{
    if (_oCityPopoverController != NULL) {
        [_oCityPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oCity.nIdCity isEqualToNumber:oCity.nIdCity]) {
        _oCity = oCity;
        [self loadAreaControllerPopover];
        [_oAreasController reloadAreasByCity:_oCity];
        [self loadMapView];
        [self reloadScreen];
    }
}

- (IBAction)showAreas:(id)sender {
    [self loadAreaControllerPopover];
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentArea.frame];
    if (_oAreasPopoverController == NULL) {
        _oAreasPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oAreasController];
        _oAreasPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oAreasPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oAreasPopoverController.isPopoverVisible) {
            [_oAreasPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oAreasPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)loadAreaControllerPopover{
    if (_oAreasController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oAreasController = [oStoryBoard instantiateViewControllerWithIdentifier:@"AreasPopoverViewController"];
        _oAreasController.delegate = self;
        _oAreasController.oCity = _oCity;
        _oAreasController.oArea = _oArea;
    }
}

- (void)hidePopoverAndSelectedArea:(AreaBean *)oArea{
    if (_oAreasPopoverController != NULL) {
        [_oAreasPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oArea.nIdArea isEqualToNumber:oArea.nIdArea]) {
        _oArea = oArea;
        [self paintOverlayAreaSelected];
        [self reloadScreen];
    }
}

- (void)paintOverlayAreaSelected{
    [_oMap removeOverlays:_oMap.overlays];
    NSArray<AreaPointBean> *aAreaPoints = [_oArea getAreaPoints];
    long nLoopPoints = 0, nCountPoints = (long)aAreaPoints.count;
    CLLocationCoordinate2D aAreaPointsCoords[nCountPoints];
    AreaPointBean *oAreaPoint = NULL;
    for (; nLoopPoints < nCountPoints; nLoopPoints++) {
        oAreaPoint = [aAreaPoints objectAtIndex:nLoopPoints];
        aAreaPointsCoords[nLoopPoints] = CLLocationCoordinate2DMake(oAreaPoint.nLatitude.floatValue, oAreaPoint.nLongitude.floatValue);
    }
    MKPolygon *oDrawArea = [MKPolygon polygonWithCoordinates:aAreaPointsCoords count:nCountPoints];
    [oDrawArea setTitle:_oArea.sName];
    oDrawArea.subtitle = _oArea.sColourArea;
    [_oMap addOverlay:oDrawArea];
}

- (IBAction)showSectors:(id)sender{
    if (_oSectorController == NULL) {
        _oSectorController = [self.storyboard instantiateViewControllerWithIdentifier:@"SectorPopoverViewController"];
        _oSectorController.delegate = self;
        _oSectorController.oSector = _oSector;
    }
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentSector.frame];
    if (_oSectorPopoverController == NULL) {
        _oSectorPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oSectorController];
        _oSectorPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oSectorPopoverController.isPopoverVisible) {
            [_oSectorPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)hidePopoverAndSelectedSector:(SectorBean *) oSector{
    if (_oSectorPopoverController != NULL) {
        [_oSectorPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oSector.nIdSector isEqualToNumber:oSector.nIdSector]) {
        _oSector = oSector;
        [self reloadScreen];
    }
}

- (IBAction)showListClientsShowsInMap:(id)sender {
    UIBarButtonItem *oButton = (UIBarButtonItem *)sender;
    if (oButton.tag == 1) {
        [_oTblClients setHidden:FALSE];
        [_oLblListadoCity setHidden:FALSE];
        [_oLblListadoCases setHidden:FALSE];
        [_oMap setHidden:TRUE];
        oButton.tag = 2;
        UIImage *image = [[UIImage imageNamed:@"btnNavShowMap"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [oButton setImage: image];
    }else{
        [_oTblClients setHidden:TRUE];
        [_oLblListadoCity setHidden:TRUE];
        [_oLblListadoCases setHidden:TRUE];
        [_oMap setHidden:FALSE];
        oButton.tag = 1;
        UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [oButton setImage:image];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoPhotosClient" isEqualToString:segue.identifier]) {
        ClientPhotoViewController *oClientPhotoView = (ClientPhotoViewController *) segue.destinationViewController;
        oClientPhotoView.oCity = _oCity;
        oClientPhotoView.oClient = _oClientSelected;
    }
}

- (IBAction)showFilterMap:(id)sender{
    [self loadFilterMapControllerPopover];
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oBtnShowFilterMap.frame];
    if (_oFilterMapPopoverController == NULL) {
        _oFilterMapPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oFilterMapController];
        _oFilterMapPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oFilterMapPopoverController.isPopoverVisible) {
            [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)loadFilterMapControllerPopover{
    if (_oFilterMapController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oFilterMapController = [oStoryBoard instantiateViewControllerWithIdentifier:@"FilterMapClientPopoverViewController"];
        _oFilterMapController.delegate = self;
        _oFilterMapController.oFilterMap = _oFilterMap;
    }
}

- (void)hidePopoverAndFilterMapClient:(FilterMapClientForm *)oFilter{
    _oFilterMap = oFilter;
    if (_oFilterMapPopoverController != NULL) {
        [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
    }
    [self reloadScreen];
}

#pragma mark mapkit delegate functions

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(ClientAnnotation *)annotation fillMKAnnotationView:oPinView];
    CGRect oFrame = oPinView.frame;
    oFrame.size = CGSizeMake(oFrame.size.width * 25 / 100, oFrame.size.height * 25 / 100);
    oPinView.frame = oFrame;
    oPinView.centerOffset = CGPointMake(0, -20);
    return oPinView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    long nLoopClient = 0, nCountClient = (long)_aClients.count;
    BOOL bClientNotFound = TRUE;
    ClientBean *oClient = NULL;
    for (; nLoopClient < nCountClient && bClientNotFound; nLoopClient++) {
        oClient = [_aClients objectAtIndex:nLoopClient];
        if (oClient.nIdClient.intValue == view.tag)
            bClientNotFound = FALSE;
    }
    _oClientSelected = oClient;
    CGPoint oAnnotationPoint = view.layer.frame.origin;
    CGSize oSiceImage = view.layer.frame.size;
    oAnnotationPoint.x += (oSiceImage.width/2);
    oAnnotationPoint.y += 60 + (oSiceImage.height/2);
    [self loadAndShowPopoverClient:oClient andFrame:oAnnotationPoint];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if([overlay isKindOfClass:[MKPolygon class]]){
        MKPolygon *oPolygon = (MKPolygon *)overlay;
        MKPolygonRenderer *oPolygonRenderer = [[MKPolygonRenderer alloc] initWithPolygon:oPolygon];
        oPolygonRenderer.lineWidth=1;
        UIColor *oColourArea = [[UIColor alloc] initWhitHexadecimalColor:oPolygon.subtitle andAlpha:1];
        oPolygonRenderer.strokeColor= oColourArea;
        oPolygonRenderer.fillColor=[oColourArea colorWithAlphaComponent:0.5];
        return oPolygonRenderer;
    }
    return nil;
}
- (void)loadClientControllerPopover{
    if (_oClientController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oClientController = [oStoryBoard instantiateViewControllerWithIdentifier:@"ClientPopoverViewController"];
        _oClientController.delegate = self;
    }
}

- (void)loadAndShowPopoverClient:(ClientBean *) oClient andFrame:(CGPoint) oPosition{
    [self loadClientControllerPopover];
    [_oClientController loadClientView:oClient];
    if (_oClientPopoverController == NULL) {
        _oClientPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oClientController];
        _oClientPopoverController.popoverBackgroundViewClass = [CBREPopoverNotBackgroundView class];

        [_oClientPopoverController presentPopoverFromRect:CGRectMake(oPosition.x, oPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
        [_oClientPopoverController.contentViewController.view setBackgroundColor:[UIColor clearColor]];
    }else{
        if (_oClientPopoverController.isPopoverVisible) {
            [_oClientPopoverController dismissPopoverAnimated:TRUE];
            _oClientPopoverController = NULL;
        }else{
            [_oClientPopoverController presentPopoverFromRect:CGRectMake(oPosition.x, oPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
        }
    }
}

#pragma mark building controller view delegate

- (void)closePopoverDetailClient{
    [self hidePopoverDetailClient];
}

- (void)closePopoverAndSendEmail{
    [self hidePopoverDetailClient];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        // Creamos el objeto mail apuntando a la clase MFMailComposeViewController.
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        [mail setSubject:_oClientSelected.sName];                                       // Asunto del correo.
        [mail setMessageBody: @"" isHTML:NO];    // Mensaje (Puede pasarse código HTML)
        [mail setToRecipients:@[ _oClientSelected.sEmail ]];                   // Array con el listado de destinatarios.
        
        mail.mailComposeDelegate = self;
        [self presentViewController:mail animated:YES completion:NULL];
    }
}

#pragma mark - MFMailCompose Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Correo enviado correctamente.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Envío de correo fallido: %@", error.description);
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Envío de correo cancelado.");
            break;
        default:
            break;
    }
    
    // Es nuestro deber descartar el controlador.
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)closePopoverAndShowClientPhotos{
    [self hidePopoverDetailClient];
    AlbumCollectionForm *oAlbumForm = [[AlbumCollectionForm alloc] init];
    oAlbumForm.aPhotos = [(NSMutableArray<PhotoBean> *)[NSMutableArray alloc] initWithArray:[_oClientSelected getPhotos]];
    AlbumPhotoViewController *oAlbumView = [CBREUtils obtainAlbumPhotoViewControlerWithDocuments:oAlbumForm oStoryBoard:self.storyboard sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName] nIdMenu:2];
    [self.navigationController pushViewController:oAlbumView animated:true];
}

- (void)closePopoverAndShowDetail{
    [self hidePopoverDetailClient];
    [self performSegueWithIdentifier:@"gotoPhotosClient" sender:self];
}

- (void)hidePopoverDetailClient{
    if (_oClientPopoverController != NULL) {
        [_oClientPopoverController dismissPopoverAnimated:TRUE];
        _oClientPopoverController = NULL;
    }
    NSArray *aAnnotationsSelected = _oMap.selectedAnnotations;
    [_oMap deselectAnnotation:[aAnnotationsSelected objectAtIndex:0] animated:TRUE];
}

- (IBAction)showOrHideSearch:(id)sender{
    [_oCtnTableSearch setHidden:!_oCtnTableSearch.hidden];
    [_oTxtSearh setHidden:!_oTxtSearh.hidden];
    [_oTblClientMini setHidden:!_oTblClientMini.hidden];
    [_oTxtSearh resignFirstResponder];
}

#pragma mark - Table view delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([_oTblClients isEqual:tableView]) {
        return _aSections.count;
    }
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if ([_oTblClients isEqual:tableView]) {
        return [_aSections objectAtIndex:section];
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    if ([_oTblClients isEqual:tableView]) {
        NSString *sSection = [_aSections objectAtIndex:section];
        
        UILabel *oLblSection = [[UILabel alloc] initWithFrame:CGRectMake(280, 0, 40, 30)];
        [oLblSection setText:sSection];
        [oLblSection setTextColor:[UIColor whiteColor]];
        [oLblSection setTextAlignment:NSTextAlignmentCenter];
        [oLblSection setFont:[UIFont fontWithName:oLblSection.font.fontName size:24.0]];
        [CBREFont setFontInLabel:oLblSection withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
        UIImageView *oImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"barraSectionMap"]];
        
        [view addSubview:oImageView];
        [view addSubview:oLblSection];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_oTblClients isEqual:tableView]) {
        NSString *sSection = [_aSections objectAtIndex:section];
        NSArray<ClientBean> *aClientss = [_aClientSections objectForKey:sSection];
        return aClientss.count;
    }
    return _aClientsFilter.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_oTblClients isEqual:tableView]) {
        NSString *sCellIdentifier = @"ClientCell";
        ClientCell *oClientCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        
        NSString *sSection = [_aSections objectAtIndex:indexPath.section];
        NSMutableArray<ClientBean> *aClients = [_aClientSections objectForKey:sSection];
        ClientBean *oClient = [aClients objectAtIndex:indexPath.row];
        oClientCell.oClient = oClient;
        [oClientCell.oLblName setText:oClient.sName];
        [oClientCell.oLblAddress setText:oClient.sAddress];
        [oClientCell.oLblPhone setText:oClient.sPhone];
        [oClientCell.oLblEmail setText:oClient.sEmail];
        
        NSURL *oURLImage = [NSURL URLWithString:oClient.sImgMain];
        [oClientCell.oImgBuilding setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oClientCell.oImgBuilding.image andInsertIntUIImageView:oClientCell.oImgBuilding];

        [oClientCell.oBntShowMap addTarget:self action:@selector(showMapAndShowCityInMap:) forControlEvents:UIControlEventTouchDown];
        [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:oClientCell.oLblName, oClientCell.oLblAddress, oClientCell.oLblEmail, oClientCell.oLblPhone, oClientCell.oLblTitleEmail, oClientCell.oLblTitlePhone, nil] withType:CBREFontTypeHelveticaRegular];
        
        return oClientCell;
    }else{
        NSString *sCellIdentifier = @"ClientMiniCell";
        ClientMiniCell *oClientMiniCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentifier forIndexPath:indexPath];
        ClientBean *oClient = [_aClientsFilter objectAtIndex:indexPath.row];
        oClientMiniCell.oClient = oClient;
        
        NSURL *oURLImage = [NSURL URLWithString:oClient.sImgMain];
        [oClientMiniCell.oImgCity setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [CBREUtils resizeImage:oClientMiniCell.oImgCity.image andInsertIntUIImageView:oClientMiniCell.oImgCity];

        [oClientMiniCell.oLblName setText:oClient.sName];
        [oClientMiniCell.oLblAddress setText:oClient.sAddress];
        [oClientMiniCell.oLblPhone setText:oClient.sPhone];
        [oClientMiniCell.oLblMail setText:oClient.sEmail];
        [oClientMiniCell setBackgroundColor:[UIColor clearColor]];
        [CBREFont setFontInLabel:oClientMiniCell.oLblName withType:CBREFontTypeHelveticaBold];
        [CBREFont setFontInLabels:[NSArray arrayWithObjects:oClientMiniCell.oLblAddress, oClientMiniCell.oLblTitlePhone, oClientMiniCell.oLblPhone, oClientMiniCell.oLblMail, nil] withType:CBREFontTypeHelveticaRegular];
        return oClientMiniCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_oTblClients isEqual:tableView]) {
        ClientCell *oClientCell = (ClientCell *)[tableView cellForRowAtIndexPath:indexPath];
        _oClientSelected = oClientCell.oClient;
        [self performSegueWithIdentifier:@"gotoPhotosClient" sender:self];
    }else{
        ClientMiniCell *oClientMiniCell = (ClientMiniCell *)[tableView cellForRowAtIndexPath:indexPath];
        ClientBean *oClient = oClientMiniCell.oClient;
        
        CLLocationCoordinate2D oPositionAnnotation = CLLocationCoordinate2DMake(oClient.nLatitude.floatValue, oClient.nLongitude.floatValue);
        [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:oPositionAnnotation andZoomLevel:[NSNumber numberWithInt:13]]];

        
        [self moveScroolToClientInTableViewList:oClient];
    }
}

- (void)moveScroolToClientInTableViewList:(ClientBean *) oClient{
    BOOL bClientsFound = FALSE;
    long nLoopSections = 0, nCountSections = _aSections.count;
    for (; nLoopSections < nCountSections && !bClientsFound; nLoopSections++) {
        NSString *sSection = [_aSections objectAtIndex:nLoopSections];
        NSMutableArray<ClientBean> *aClients = [_aClientSections objectForKey:sSection];
        
        long nLoopClients = 0, nCountClients = aClients.count;
        ClientBean *oClientFound = NULL;
        for ( ; nLoopClients < nCountClients && !bClientsFound; nLoopClients++) {
            oClientFound = [aClients objectAtIndex:nLoopClients];
            if (oClient.nIdClient.intValue == oClientFound.nIdClient.intValue) {
                bClientsFound = TRUE;
                [_oTblClients scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:nLoopClients inSection:nLoopSections] atScrollPosition:UITableViewScrollPositionTop animated:TRUE];
            }
        }
    }
}

- (IBAction)showMapAndShowCityInMap:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    ClientCell *oClientCell = (ClientCell *)oButton.superview.superview;
    ClientBean *oClient = oClientCell.oClient;
    
    CLLocationCoordinate2D oPositionAnnotation = CLLocationCoordinate2DMake(oClient.nLatitude.floatValue, oClient.nLongitude.floatValue);
    [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:oPositionAnnotation andZoomLevel:[NSNumber numberWithInt:13]]];

    _oListButton.tag = 1;
    [_oTblClients setHidden:TRUE];
    [_oLblListadoCity setHidden:TRUE];
    [_oLblListadoCases setHidden:TRUE];
    [_oMap setHidden:FALSE];
    UIImage *image = [[UIImage imageNamed:@"btnNavShowList"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_oListButton setImage:image];
}

#pragma mark text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    NSString *sTextValue = [NSString stringWithString:textField.text];
    sTextValue = [sTextValue
                 stringByReplacingCharactersInRange:range withString:string].uppercaseString;
    
    long nLoopClient = 0, nCountClients = _aClients.count;
    ClientBean *oClient = NULL;
    _aClientsFilter = (NSMutableArray<ClientBean>*)[[NSMutableArray alloc] init];
    for ( ; nLoopClient < nCountClients ; nLoopClient++) {
        oClient = [_aClients objectAtIndex:nLoopClient];
        int nLength = (int)[oClient.sName.uppercaseString rangeOfString:sTextValue].length;
        if (nLength > 0 || [@"" isEqualToString:sTextValue]) {
            [_aClientsFilter addObject:oClient];
        }
    }
    
    [_oTblClientMini reloadData];
    return true;
}

@end
