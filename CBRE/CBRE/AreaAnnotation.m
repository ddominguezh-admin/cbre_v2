//
//  AreaAnnotation.m
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaAnnotation.h"
#import "AreaBean.h"

@implementation AreaAnnotation

- (id)initWithArea:(AreaBean *) oArea{
    if (self == NULL)
        self = [super init];
    self.coordinate = CLLocationCoordinate2DMake(oArea.nLatitude.floatValue, oArea.nLongitude.floatValue);
    self.oArea = oArea;
    self.oImage = [UIImage imageNamed:@""];
    return self;
}

- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation{
    oPinAnnotation.image = self.oImage;
    oPinAnnotation.enabled = YES;
    oPinAnnotation.canShowCallout = NO;
    oPinAnnotation.tag = self.oArea.nIdArea.intValue;
}


@end
