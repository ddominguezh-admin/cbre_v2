//
//  BuildingPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 10/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BuildingBean;

@protocol BuildingPopoverViewControllerDelegate;

@interface BuildingPopoverViewController : UIViewController

@property (strong, nonatomic) id<BuildingPopoverViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;
@property (weak, nonatomic) IBOutlet UILabel *oLblMeters;
@property (weak, nonatomic) IBOutlet UILabel *oLblRent;
@property (weak, nonatomic) IBOutlet UITextView *oTxtDescription;
@property (weak, nonatomic) IBOutlet UIImageView *oImgBuilding;

@property (strong, nonatomic) BuildingBean *oBuilding;

- (void)loadBuildingView:(BuildingBean *) oBuilding;
- (IBAction)closePopover:(id)sender;
- (IBAction)selectedBuilding:(id)sender;

@end

@protocol BuildingPopoverViewControllerDelegate <NSObject>

- (void)closePopoverAndShowDetailBuilding;
- (void)closePopoverDetailBuilding;
- (void)closePopoverAndShowBuildingPhotos;
- (void)closePopoverAndSendEmail;

@end
