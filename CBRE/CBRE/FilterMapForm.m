//
//  FilterMapForm.m
//  CBRE
//
//  Created by ddominguezh on 08/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FilterMapForm.h"

@implementation FilterMapForm

- (id)init{
    if (self == NULL) {
        self = [super init];
    }
    self.bFilterByMeters = FALSE;
    self.bFilterByPlants = FALSE;
    self.bFilterByRentType = FALSE;
    self.nRentType = [NSNumber numberWithInt:0];
    return self;
}

- (NSString *)getParamsUrl{
    NSString *sParams = @"";

    if (_bFilterByMeters) {
        sParams = [NSString stringWithFormat:@"%@min_meters=%@&", sParams, _nMeters];
        if ([_nMeters2 intValue] != 0) {
            sParams = [NSString stringWithFormat:@"%@max_meters=%@&", sParams, _nMeters2];
        }
    }
    
    if (_bFilterByPlants) {
        sParams = [NSString stringWithFormat:@"%@min_plant=%@&", sParams, _nPlants];
        if ([_nPlants2 intValue] != 0) {
            sParams = [NSString stringWithFormat:@"%@max_plant=%@&", sParams, _nPlants2];
        }
    }
    
    if (_bFilterByRentType)
        sParams = [NSString stringWithFormat:@"%@rent=%@&", sParams, _nRentType];
    
    if (_nSector != NULL && _nSector.intValue != 0)
        sParams = [NSString stringWithFormat:@"%@sector=%@&", sParams, _nSector];
    
    return sParams;
}

@end
