//
//  CityAnnotation.h
//  CBRE
//
//  Created by ddominguezh on 07/09/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>
@class CityBean;

@interface CityAnnotation : NSObject<MKAnnotation>

@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) UIImage *oImage;
@property (strong, nonatomic) CityBean *oCity;

- (id)initWithCity:(CityBean *) oCity;
- (void) fillMKAnnotationView:(MKAnnotationView *) oPinAnnotation;


@end
