//
//  CityConverter.h
//  CBRE
//
//  Created by ddominguezh on 24/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityBean.h"

@interface CityConverter : NSObject

+ (NSArray<CityBean> *) jsonToCities:(NSDictionary *) oData;
+ (CityBean *) jsonToCity:(NSDictionary *) oData;

@end
