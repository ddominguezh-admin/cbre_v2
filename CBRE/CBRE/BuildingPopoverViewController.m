//
//  BuildingPopoverViewController.m
//  CBRE
//
//  Created by ddominguezh on 10/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "BuildingPopoverViewController.h"
#import "BuildingBean.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface BuildingPopoverViewController ()

@end

@implementation BuildingPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabels:[[NSArray alloc]initWithObjects:_oLblName, _oLblMeters, _oLblRent, nil] withType:CBREFontTypeHelveticaBold];
    [CBREFont setFontInTextView:_oTxtDescription withType:CBREFontTypeHelveticaBold];
    [self reloadInputsWithBuilding];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.superview.layer.cornerRadius = 0.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadBuildingView:(BuildingBean *) oBuilding{
    _oBuilding = oBuilding;
    [self reloadInputsWithBuilding];
}

- (void)reloadInputsWithBuilding{
    [_oLblName setText:_oBuilding.sName];
    [_oLblMeters setText:[NSString stringWithFormat:@"%@ m2", _oBuilding.nMeters]];
    [_oLblRent setText:[NSString stringWithFormat:@"%@ m2/Euros", _oBuilding.nRent]];
    [_oTxtDescription setText:_oBuilding.sDescription];
    
    NSURL *oURLImage = [NSURL URLWithString:_oBuilding.sImgMain];
    [_oImgBuilding setImageWithURL:oURLImage placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [CBREUtils resizeImage:_oImgBuilding.image andInsertIntUIImageView:_oImgBuilding];
    
    /*
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
        NSURL *oURLImage = [NSURL URLWithString:_oBuilding.sImgMain];
        NSData *oData = [NSData dataWithContentsOfURL:oURLImage];
        UIImage *oImgPhoto = [UIImage imageWithData:oData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [CBREUtils resizeImage:oImgPhoto andInsertIntUIImageView:_oImgBuilding];
        });
    });
    */
}

- (IBAction)closePopover:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverDetailBuilding];
}

- (IBAction)selectedBuilding:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndShowDetailBuilding];
}

- (IBAction)closePopoverAndShowPhtos:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndShowBuildingPhotos];
}

- (IBAction)closePopoverAndSendEmail:(id)sender {
    if (_delegate != NULL)
        [_delegate closePopoverAndSendEmail];
}

@end
