//
//  FacilityMenuViewController.m
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "FacilityMenuViewController.h"
#import "DocumentDao.h"

@interface FacilityMenuViewController ()

@end

@implementation FacilityMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabel:_oLblTitle withType:CBREFontTypeHelveticaNeue77BoldCondensed];
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnCorporativo, _oBtnPymes, nil] withType:CBREFontTypeHelveticaNeue57Condensed];
    
    [_oLblTitle setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.facility.facility"].uppercaseString];
    [_oBtnCorporativo setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.facility.coorporativo"]forState:UIControlStateNormal];
    [_oBtnPymes setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.facility.pymes"] forState:UIControlStateNormal];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.facility.facility"]];

    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)gotoShowPdfsCorporativo:(id)sender {
    [self loadAndShotPdfsViews:[[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesFacilityCorporative] andTitleScreen:_oBtnCorporativo.titleLabel.text];
}

- (IBAction)gotoShowPdfsPymes:(id)sender {
    [self loadAndShotPdfsViews:[[DocumentDao sharedInstance] getDocumentsByType:DocumentocupantesFacilityPymes] andTitleScreen:_oBtnPymes.titleLabel.text];
}

- (void)loadAndShotPdfsViews:(NSArray<DocumentBean> *) aDocuments andTitleScreen:(NSString *) sTitleScreen{
    NSString *sSubTitleNavBar = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.facility.facility"], sTitleScreen];
    UIViewController *oPDFView = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:sTitleScreen sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:sSubTitleNavBar sBackground:@"FacilityFondo" nIdMenu:2];
    
    if (oPDFView)
        [self.navigationController pushViewController:oPDFView animated:true];
    
}

@end
