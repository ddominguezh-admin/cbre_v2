//
//  AreasPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 05/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CityBean;
@class AreaBean;

@protocol AreasPopoverViewControllerDelegate;

@interface AreasPopoverViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) CityBean *oCity;
@property (strong, nonatomic) AreaBean *oArea;
@property (strong, nonatomic) id<AreasPopoverViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *oTblAreas;

- (void)reloadAreasByCity:(CityBean *) oCity;

@end

@protocol AreasPopoverViewControllerDelegate <NSObject>

- (void)hidePopoverAndSelectedArea:(AreaBean *) oArea;

@end

