//
//  ClientCityViewController.m
//  CBRE
//
//  Created by ddominguezh on 17/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ClientCityMapViewController.h"
#import "AppDelegate.h"
#import "CityBean.h"
#import "AreaBean.h"
#import "AreaAnnotation.h"
#import "ClientAreaMapViewController.h"
#import "ButtonAreaCell.h"
#import "CBREPopoverBackgroundView.h"
#import "CBREMapUtils.h"
#import "UIColor+CBREHexColor.h"
#import "AreaDao.h"

@interface ClientCityMapViewController ()

@property (strong, nonatomic) AppDelegate *oDelegate;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
@property (strong, nonatomic) NSArray<AreaBean> *aAreas;
@property (strong, nonatomic) AreaBean *oAreaSelected;

@end

@implementation ClientCityMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _oDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    _aCities = _oDelegate.aCities;
    
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects: _oLblSelectedCity, _oLblSectorName, nil] withType:CBREFontTypeHelveticaNeue47LightCondensed];
    
    [super loadMenuButtonsBack:self andIdMenu:2];
    [self initScreen];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_oMap becomeFirstResponder];
}

- (void) initScreen{
    _oFilterMap = [[FilterMapClientForm alloc] init];
    [self loadMapView];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName]];
    
    UITapGestureRecognizer *oTagGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedArea:)];
    oTagGesture.numberOfTouchesRequired = 1;
    oTagGesture.numberOfTapsRequired = 1;
    [_oMap addGestureRecognizer:oTagGesture];

}

- (void) reloadScreen{
    _oFilterMap.nSector = _oSector.nIdSector;
    [self updateComboLabels];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.project"], _oCity.sName]];
    [self loadAnnotationsAreas];
    [self calculatePositionTblAreas];
}

- (void)updateComboLabels{
    [_oLblSelectedCity setText:_oCity.sName];
    [_oLblSectorName setText:_oSector.sName];
    if (_oSector == NULL) {
        [_oLblSectorName setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.sector.type.todos"].uppercaseString];
    }else{
        [_oLblSectorName setText:_oSector.sName];
    }
}


- (void)calculatePositionTblAreas{
    int nNormalHeight = 6 * 29;
    int nAreasHeight = (int)_aAreas.count * 29;
    int nDiff = nNormalHeight - nAreasHeight;
    
    _oTblConstHeight.constant = nAreasHeight;
    _oTblConstPosY.constant = 457 + nDiff;
}

- (void)loadMapView{
    [_oActivityIndicator setHidden:FALSE];
    [_oActivityIndicator startAnimating];
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(backgroundQueue, ^{
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [_oMap setRegion:[CBREMapUtils makeRegionByCoordinate:CLLocationCoordinate2DMake(_oCity.nLatitude.floatValue, _oCity.nLongitude.floatValue) andZoomLevel:_oCity.nZoom]];
            
            [self reloadScreen];
 
            [_oActivityIndicator stopAnimating];
            [_oActivityIndicator setHidden:TRUE];
        });
        
    });
}

- (void)loadAnnotationsAreas{
    [CBREMapUtils removeAllAnnotations:_oMap];
    [_oMap removeOverlays:_oMap.overlays];
    
    _aAreas = [_oCity getAreasByFilterClientMap:_oFilterMap];
    int nLoopAreas = 0, nCountAreas = (int)_aAreas.count;
    AreaBean *oArea = NULL;
    for (; nLoopAreas < nCountAreas; nLoopAreas++) {
        oArea = [_aAreas objectAtIndex:nLoopAreas];

        AreaAnnotation *oAnnotation = [[AreaAnnotation alloc] initWithArea:oArea];
        [_oMap addAnnotation:oAnnotation];
        
        NSArray<AreaPointBean> *aAreaPoints = [oArea getAreaPoints];
        long nLoopPoints = 0, nCountPoints = (long)aAreaPoints.count;
        CLLocationCoordinate2D aAreaPointsCoords[nCountPoints];
        AreaPointBean *oAreaPoint = NULL;
        
        for (; nLoopPoints < nCountPoints; nLoopPoints++) {
            oAreaPoint = [aAreaPoints objectAtIndex:nLoopPoints];
            aAreaPointsCoords[nLoopPoints] = CLLocationCoordinate2DMake(oAreaPoint.nLatitude.floatValue, oAreaPoint.nLongitude.floatValue);
        }
        
        MKPolygon *oDrawArea = [MKPolygon polygonWithCoordinates:aAreaPointsCoords count:nCountPoints];
        [oDrawArea setTitle:oArea.sName];
        [oDrawArea setSubtitle: oArea.sColourArea];
        [_oMap addOverlay:oDrawArea];
    }
    [_oTblAreas reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark mapkit delegate functions

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *oPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"curr"];
    [(AreaAnnotation *)annotation fillMKAnnotationView:oPinView];
    return oPinView;
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    long nLoopArea = 0, nCountArea = (long)_aAreas.count;
    BOOL bAreaNotFound = TRUE;
    AreaBean *oArea = NULL;
    for (; nLoopArea < nCountArea && bAreaNotFound; nLoopArea++) {
        oArea = [_aAreas objectAtIndex:nLoopArea];
        if (oArea.nIdArea.intValue == view.tag)
            bAreaNotFound = FALSE;
    }
    _oAreaSelected = oArea;
    [self performSegueWithIdentifier:@"gotoClientArea" sender:self];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if([overlay isKindOfClass:[MKPolygon class]]){
        MKPolygon *oPolygon = (MKPolygon *)overlay;
        MKPolygonRenderer *oPolygonRenderer = [[MKPolygonRenderer alloc] initWithPolygon:oPolygon];
        oPolygonRenderer.lineWidth=1;
        UIColor *oColourArea = [[UIColor alloc] initWhitHexadecimalColor:oPolygon.subtitle andAlpha:1];
        oPolygonRenderer.strokeColor= oColourArea;
        oPolygonRenderer.fillColor=[oColourArea colorWithAlphaComponent:0.5];
        return oPolygonRenderer;
    }
    return nil;
}

- (IBAction)selectedArea:(UITapGestureRecognizer *) oTagGesture
{
    CGPoint nMapPoint = [self obtainPointTouchInsideMap:oTagGesture];
    
    NSArray *aOverlays = _oMap.overlays;
    long nLoopOverlay = 0, nCountOverlay = (long)aOverlays.count;
    BOOL bFoundArea = FALSE;
    
    for ( ; nLoopOverlay < nCountOverlay && !bFoundArea ; ) {
        MKPolygon *oPolygon = [aOverlays objectAtIndex: nLoopOverlay];
        if ([self isTouchInsidePolygon:oPolygon nTapPoint:nMapPoint])
            bFoundArea = TRUE;
        else
            nLoopOverlay++;
    }
    
    if (bFoundArea) {
        _oAreaSelected = [_aAreas objectAtIndex:nLoopOverlay];
        [self performSegueWithIdentifier:@"gotoClientArea" sender:self];
    }
}

- (CGPoint) obtainPointTouchInsideMap:(UITapGestureRecognizer *) oTagGesture{
    CGPoint nPoint = [oTagGesture locationInView:_oMap];
    CLLocationCoordinate2D oTapCoord = [_oMap convertPoint:nPoint toCoordinateFromView:_oMap];
    MKMapPoint oMapPoint = MKMapPointForCoordinate(oTapCoord);
    return CGPointMake(oMapPoint.x, oMapPoint.y);
}

- (BOOL)isTouchInsidePolygon:(MKPolygon *) oPolygon nTapPoint:(CGPoint) nPoint{
    CGMutablePathRef oPathRef = CGPathCreateMutable();
    MKMapPoint *aPoints = oPolygon.points;
    long nLoopPoint = 0, nCountPoint = (long)oPolygon.pointCount;
    for ( ; nLoopPoint < nCountPoint ; nLoopPoint++) {
        MKMapPoint oMapPoint = aPoints[nLoopPoint];
        if (nLoopPoint == 0)
            CGPathMoveToPoint(oPathRef, NULL, oMapPoint.x, oMapPoint.y);
        else
            CGPathAddLineToPoint(oPathRef, NULL, oMapPoint.x, oMapPoint.y);
    }
    return CGPathContainsPoint(oPathRef , NULL, nPoint, FALSE);
}

#pragma mark prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([@"gotoClientArea" isEqualToString:segue.identifier]) {
        ClientAreaMapViewController *oAreaMapView = (ClientAreaMapViewController *)segue.destinationViewController;
        oAreaMapView.oArea = _oAreaSelected;
        oAreaMapView.oCity = _oCity;
        oAreaMapView.oSector = _oSector;
        oAreaMapView.oFilterMap = _oFilterMap;
    }
}

#pragma mark table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aAreas.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"ButtonAreaCell";
    ButtonAreaCell *oButtonAreaCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer forIndexPath:indexPath];
    AreaBean *oArea = [_aAreas objectAtIndex:indexPath.row];
    oButtonAreaCell.oArea = oArea;
    [oButtonAreaCell.oLblName setText:oArea.sName];
    [oButtonAreaCell.oImgBackground setImage:[UIImage imageNamed:oArea.sLeyenda]];
    [CBREFont setFontInLabel:oButtonAreaCell.oLblName withType:CBREFontTypeHelveticaRegular];
    [oButtonAreaCell.oLblName setTextColor:[[UIColor alloc] initWhitHexadecimalColor:oArea.sColourText andAlpha:1.0]];
    return oButtonAreaCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonAreaCell *oButtonAreaCell = (ButtonAreaCell *)[tableView cellForRowAtIndexPath:indexPath];
    _oAreaSelected = oButtonAreaCell.oArea;
    [self performSegueWithIdentifier:@"gotoClientArea" sender:self];
}

- (IBAction)showCities:(id)sender {
    if (_oCitiesController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oCitiesController = [oStoryBoard instantiateViewControllerWithIdentifier:@"CitiesPopoverViewController"];
        _oCitiesController.delegate = self;
        _oCitiesController.oCity = _oCity;
    }
    
    
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentCity.frame];
    
    if (_oCityPopoverController == NULL) {
        _oCityPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oCitiesController];
        _oCityPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
    }else{
        if (_oCityPopoverController.isPopoverVisible) {
            [_oCityPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oCityPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)hidePopoverAndSelectedCity:(CityBean *)oCity{
    if (_oCityPopoverController != NULL) {
        [_oCityPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oCity.nIdCity isEqualToNumber:oCity.nIdCity]) {
        _oCity = oCity;
        [self loadMapView];
    }
}

- (IBAction)showSectors:(id)sender{
    if (_oSectorController == NULL) {
        _oSectorController = [self.storyboard instantiateViewControllerWithIdentifier:@"SectorPopoverViewController"];
        _oSectorController.delegate = self;
    }
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oImgContentSector.frame];
    if (_oSectorPopoverController == NULL) {
        _oSectorPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oSectorController];
        _oSectorPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oSectorPopoverController.isPopoverVisible) {
            [_oSectorPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oSectorPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)hidePopoverAndSelectedSector:(SectorBean *) oSector{
    if (_oSectorPopoverController != NULL) {
        [_oSectorPopoverController dismissPopoverAnimated:TRUE];
    }
    if (![_oSector.nIdSector isEqualToNumber:oSector.nIdSector]) {
        _oSector = oSector;
        [self reloadScreen];
    }
}

- (IBAction)showFilterMap:(id)sender{
    [self loadFilterMapControllerPopover];
    CGPoint oButtonPosition = [CBREUtils calculatePositionPopoverButtons:_oBtnShowFilterMap.frame];
    if (_oFilterMapPopoverController == NULL) {
        _oFilterMapPopoverController = [[UIPopoverController alloc] initWithContentViewController:_oFilterMapController];
        _oFilterMapPopoverController.popoverBackgroundViewClass = [CBREPopoverBackgroundView class];
        [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        
    }else{
        if (_oFilterMapPopoverController.isPopoverVisible) {
            [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
        }else{
            [_oFilterMapPopoverController presentPopoverFromRect:CGRectMake(oButtonPosition.x, oButtonPosition.y, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:TRUE];
        }
    }
}

- (void)loadFilterMapControllerPopover{
    if (_oFilterMapController == NULL) {
        UIStoryboard *oStoryBoard = ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController.storyboard;
        _oFilterMapController = [oStoryBoard instantiateViewControllerWithIdentifier:@"FilterMapClientPopoverViewController"];
        _oFilterMapController.delegate = self;
        _oFilterMapController.oFilterMap = _oFilterMap;
    }
}

- (void)hidePopoverAndFilterMapClient:(FilterMapClientForm *)oFilter{
    _oFilterMap = oFilter;
    if (_oFilterMapPopoverController != NULL) {
        [_oFilterMapPopoverController dismissPopoverAnimated:TRUE];
    }
    [self reloadScreen];
}


@end
