//
//  ProjectManagementMetodologiaViewController.m
//  CBRE
//
//  Created by ddominguezh on 03/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ProjectManagementMetodologiaViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionCell.h"
#import "DocumentDao.h"

@interface ProjectManagementMetodologiaViewController ()

@property (strong, nonatomic) DocumentBean *oDocumentSelected;
@property int indexPathSelected;


@end

@implementation ProjectManagementMetodologiaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _indexPathSelected = 1;
    
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnAllCases, _oBtnActas, _oBtnProcedimientos, _oBtnPlanificacion, nil] withType:CBREFontTypeHelveticaRegular];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    
    _aDocumentsActas = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesGestionProyectosMetodologiaActas];
    _aDocumentsProcedimientos = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesGestionProyectosMetodologiaProcedimientos];
    _aDocumentsPlanificacion = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesGestionProyectosMetodologiaPlanificacion];
    
    [_oBtnAllCases setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.metodologia.todos"] forState:UIControlStateNormal];
    [_oBtnActas setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.metodologia.actas"] forState:UIControlStateNormal];
    [_oBtnProcedimientos setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.metodologia.procedimientos"] forState:UIControlStateNormal];
    [_oBtnPlanificacion setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.metodologia.planificacion"] forState:UIControlStateNormal];
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.projectManagement"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.metodologia"]]];
  
    [super loadMenuButtonsBack:self andIdMenu:1];
    
    [self executeAllCases];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection view delegate functions

- (long) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (long)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aDocuments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"PDFCollectionCell";
    PDFCollectionCell *oPDFCollectionCell = (PDFCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    DocumentBean *oDocument = [_aDocuments objectAtIndex:indexPath.row];
    oPDFCollectionCell.oDocument = oDocument;
    [oPDFCollectionCell.oBtnPdf setImage:[UIImage imageNamed:oDocument.sImgName] forState:UIControlStateNormal];
    [oPDFCollectionCell.oBtnPdf setTag:indexPath.row];
    [oPDFCollectionCell.oBtnPdf addTarget:self action:@selector(viewPdf:) forControlEvents:UIControlEventTouchDown];
    [oPDFCollectionCell.oLblName setText:oDocument.sName];
    return oPDFCollectionCell;
}

- (IBAction)previousDocument:(id)sender {
    BOOL bCanMove = _indexPathSelected > 1;
    if (bCanMove) {
        _indexPathSelected--;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)nextDocument:(id)sender {
    long nLastStep = _aDocuments.count - 2;
    BOOL bCanMove = _indexPathSelected < nLastStep;
    if (bCanMove) {
        _indexPathSelected++;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)viewPdf:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = 2;
    oDetailForm.sTitleNavBar = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"];
    oDetailForm.sSubTitleNavBar = [NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.projectManagement"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.metodologia"]];
    oDetailForm.sBackground = @"ProjectManagementFondo";
    oDetailForm.oDocument = [_aDocuments objectAtIndex:oButton.tag];
    oPdfDetail.oDetailForm = oDetailForm;
    [self.navigationController pushViewController:oPdfDetail animated:true];
}

- (IBAction)filterAllCases:(id)sender {
    [self resetColorLabels];
    [self executeAllCases];
}

- (void)executeAllCases{
    [self setSelectedLable:_oBtnAllCases];
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    
    [aDocuments addObjectsFromArray:_aDocumentsActas];
    [aDocuments addObjectsFromArray:_aDocumentsProcedimientos];
    [aDocuments addObjectsFromArray:_aDocumentsPlanificacion];
    
    _aDocuments = (NSArray<DocumentBean> *)[[NSArray alloc] initWithArray:aDocuments];
    [self reloadCollectionData];
}

- (IBAction)filterActas:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnActas];
    _aDocuments = _aDocumentsActas;
    [self reloadCollectionData];
}

- (IBAction)filterProcedimientos:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnProcedimientos];
    _aDocuments = _aDocumentsProcedimientos;
    [self reloadCollectionData];
}

- (IBAction)filterPlanificacion:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnPlanificacion];
    _aDocuments = _aDocumentsPlanificacion;
    [self reloadCollectionData];
}

- (void)resetColorLabels{
    [_oBtnAllCases setSelected:FALSE];
    [_oBtnActas setSelected:FALSE];
    [_oBtnProcedimientos setSelected:FALSE];
    [_oBtnPlanificacion setSelected:FALSE];
    _indexPathSelected = 1;
}

- (void)setSelectedLable:(UIButton *) oButton{
    NSString *sTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.metodologia.metodologia"];
    [_oLblNameScreen setText:[NSString stringWithFormat:@"%@ (%@)", sTitle, oButton.titleLabel.text].uppercaseString];
    [oButton setSelected:TRUE];
}

- (void)reloadCollectionData{
    long nCountDocuments = _aDocuments.count;
    if (nCountDocuments < 4) {
        [_oBtnPrevious setHidden:TRUE];
        [_oBtnNext setHidden:TRUE];
    }else{
        [_oBtnPrevious setHidden:FALSE];
        [_oBtnNext setHidden:FALSE];
    }
    if (nCountDocuments < 3)
        [self calculateFrameCollectionView:nCountDocuments];
    else
        [self calculateFrameCollectionView:3];
    [_oClvDocuments reloadData];
}

- (void)calculateFrameCollectionView:(long) nCountDocuments{
    float nWidth = 760 - ((760 / 3) * nCountDocuments);
    _oConstClvDocsWidth.constant = 760 - nWidth;
    _oConstClvDocsPosX.constant = 128 + (nWidth / 2);
}

@end
