
//
//  CBREUtils.m
//  CBRE
//
//  Created by ddominguezh on 19/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CBREUtils.h"
#import "PDFCollectionViewController.h"
#import "PDFCollectionForm.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "MenuCollectionViewController.h"
#import "MenuCollectionForm.h"

@implementation CBREUtils

+ (CGPoint)calculatePositionPopoverButtons:(CGRect) oFrame{
    CGPoint oPoint = oFrame.origin;
    CGSize oSize = oFrame.size;
    oPoint.x += (oSize.width/2);
    oPoint.y += oSize.height;
    return oPoint;
}

+ (void) hideBackgroundWebView:(UIView *) oView {
    [oView setOpaque:FALSE];
    [oView setBackgroundColor:[UIColor clearColor]];
    NSArray *aSubViews = oView.subviews;
    int nLoopSubViews = 0, nCountSubViews = (int)aSubViews.count;
    UIView *oSubView = NULL;
    for ( ; nLoopSubViews < nCountSubViews ; nLoopSubViews++) {
        oSubView = [aSubViews objectAtIndex:nLoopSubViews];
        if ([oSubView isKindOfClass:[UIImageView class]])
            oSubView.hidden = TRUE;
        [CBREUtils hideBackgroundWebView:oSubView];
    }
}

+ (UIView *)createTitleViewNavigation:(NSString *) sTitle
{
    return [CBREUtils createTitleViewNavigation:sTitle.uppercaseString andFontSize:23];
}

+ (UIView *)createTitleViewNavigation:(NSString *) sTitle andFontSize:(int) nFontSize{
    int nWidthTitleView = 824;
    int nHeightTitleView = 30;
    UIView *oTitleView = [[UIView alloc] initWithFrame:CGRectMake((1024 - nWidthTitleView) / 2, 0, nWidthTitleView, nHeightTitleView)];
    
    UILabel *oLabelTitle = [[UILabel alloc] init];
    [oLabelTitle setTextColor:[UIColor whiteColor]];
    [oLabelTitle setText:sTitle];
    [oLabelTitle setTextAlignment:NSTextAlignmentCenter];
    [oLabelTitle setFont:[UIFont fontWithName:[CBREFont obtainFontNameWithType:CBREFontTypeHelveticaNeue57Condensed] size:nFontSize]];
    [CBREFont setFontInLabel:oLabelTitle withType:CBREFontTypeHelveticaNeue57Condensed];
    
    CGSize nSize = [CBREUtils sizeOfLabel:oLabelTitle withText:sTitle];
    
    if (nSize.width > nWidthTitleView) {
        return [CBREUtils createTitleViewNavigation:sTitle andFontSize:nFontSize - 2];
    }
    
    oLabelTitle.frame = CGRectMake(((nWidthTitleView - nSize.width)/2), ((nHeightTitleView - nSize.height)/ 2), nSize.width, nSize.height);
    [oTitleView addSubview:oLabelTitle];
    
    return oTitleView;
}

+ (UIView *)createTitleViewNavigation:(NSString *) sTitle andSubtitle:(NSString *) sSubtitle{
    sTitle = [NSString stringWithFormat:@"%@ /", sTitle.uppercaseString];
    sSubtitle = [NSString stringWithFormat:@" %@", sSubtitle];
    return [CBREUtils createTitleViewNavigation:sTitle andSubtitle:sSubtitle andFontSize:24];
}

+ (UIView *)createTitleViewNavigation:(NSString *) sTitle andSubtitle:(NSString *) sSubtitle andFontSize:(int) nFontSize{
    int nWidthTitleView = 824;
    int nHeightTitleView = 30;
    
    UIView *oTitleView = [[UIView alloc] initWithFrame:CGRectMake((1024 - nWidthTitleView) / 2, 0, nWidthTitleView, nHeightTitleView)];
    
    UILabel *oLabelTitle = [[UILabel alloc] init];
    [oLabelTitle setTextColor:[UIColor whiteColor]];
    [oLabelTitle setText:sTitle];
    [oLabelTitle setTextAlignment:NSTextAlignmentRight];
    [oLabelTitle setFont:[UIFont fontWithName:[CBREFont obtainFontNameWithType:CBREFontTypeHelveticaNeue57Condensed] size:nFontSize-1]];
    
    UILabel *oLabelSubTitle = [[UILabel alloc] init];
    [oLabelSubTitle setTextColor:[UIColor colorWithRed:(54.0/255.0f) green:(215.0/255.0f) blue:(231.0/255.0f) alpha:1.0f]];
    [oLabelSubTitle setText:sSubtitle];
    [oLabelSubTitle setFont:[UIFont fontWithName:[CBREFont obtainFontNameWithType:CBREFontTypeHelveticaNeue77BoldCondensed] size:nFontSize]];
    
    
    CGSize nSizeTitle = [CBREUtils sizeOfLabel:oLabelTitle withText:sTitle];
    CGSize nSizeSubTitle = [CBREUtils sizeOfLabel:oLabelSubTitle withText:sSubtitle];
    
    CGFloat nSumSizeLabels = nSizeTitle.width + nSizeSubTitle.width;
    if (nSumSizeLabels >= nWidthTitleView) {
        return [CBREUtils createTitleViewNavigation:sTitle andSubtitle:sSubtitle andFontSize:nFontSize-2];
    }
    
    CGFloat nPositionTitle = (nWidthTitleView - nSumSizeLabels) / 2;
    CGFloat nPositionSubtitle = nPositionTitle + nSizeTitle.width;
    
    oLabelTitle.frame = CGRectMake(nPositionTitle, ((nHeightTitleView - nSizeTitle.height)/ 2), nSizeTitle.width, nSizeTitle.height);
    oLabelSubTitle.frame = CGRectMake(nPositionSubtitle, ((nHeightTitleView - nSizeSubTitle.height)/ 2), nSizeSubTitle.width, nSizeSubTitle.height);
    
    [oTitleView addSubview:oLabelTitle];
    [oTitleView addSubview:oLabelSubTitle];
    
    return oTitleView;
}

+ (UIViewController *)obtainViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sSubTitleScreen:(NSString *) sSubTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu{
    
    UIViewController *oViewController = NULL;
    if (aDocuments.count > 1) {
        oViewController = [CBREUtils obtainPDFCollectionViewControlerWithDocuments:aDocuments oStoryBoard:oStoryBoard sTitleScreen:sTitleScreen sSubTitleScreen:sSubTitleScreen sTitleNavBar:sTitleNavBar  sSubTitleNavBar:sSubTitleNavBar sBackground:sBackground nIdMenu:nIdMenu];
    }else if(aDocuments.count > 0){
        oViewController = [CBREUtils obtainPDFDetailViewControlerWithDocuments:aDocuments oStoryBoard:oStoryBoard sTitleScreen:sTitleScreen sTitleNavBar:sTitleNavBar sSubTitleNavBar:sSubTitleNavBar sBackground:sBackground nIdMenu:nIdMenu];
    }
    return oViewController;
}

+ (UIViewController *)obtainViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu{
    
    UIViewController *oViewController = NULL;
    if (aDocuments.count > 1) {
        oViewController = [CBREUtils obtainPDFCollectionViewControlerWithDocuments:aDocuments oStoryBoard:oStoryBoard sTitleScreen:sTitleScreen sSubTitleScreen:@"" sTitleNavBar:sTitleNavBar sSubTitleNavBar:sSubTitleNavBar sBackground:sBackground nIdMenu:nIdMenu];
    }else if(aDocuments.count > 0){
        oViewController = [CBREUtils obtainPDFDetailViewControlerWithDocuments:aDocuments oStoryBoard:oStoryBoard sTitleScreen:sTitleScreen sTitleNavBar:sTitleNavBar sSubTitleNavBar:sSubTitleNavBar sBackground:sBackground nIdMenu:nIdMenu];
    }
    return oViewController;
}

+ (PDFCollectionViewController *) obtainPDFCollectionViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sSubTitleScreen:(NSString *) sSubTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu{
    
    PDFCollectionViewController *oPDFCollection = (PDFCollectionViewController *)[oStoryBoard instantiateViewControllerWithIdentifier:@"PDFCollectionViewController"];
    PDFCollectionForm *oPDFCollectionForm = [[PDFCollectionForm alloc] init];
    oPDFCollectionForm.nIdMenu = nIdMenu;
    oPDFCollectionForm.sTitleScreen = sTitleScreen;
    oPDFCollectionForm.sSubTitleScreen = sSubTitleScreen;
    oPDFCollectionForm.sTitleNavBar = sTitleNavBar;
    oPDFCollectionForm.sSubTitleNavBar = sSubTitleNavBar;
    oPDFCollectionForm.sBackground = sBackground;
    oPDFCollectionForm.aDocuments = aDocuments;
    oPDFCollection.oCollectionForm = oPDFCollectionForm;
    return oPDFCollection;
}

+ (PDFDetailViewController *) obtainPDFDetailViewControlerWithDocuments:(NSArray<DocumentBean> *) aDocuments oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu{
    
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[oStoryBoard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = nIdMenu;
    oDetailForm.sTitleNavBar = sTitleNavBar;
    oDetailForm.sSubTitleNavBar = sSubTitleNavBar;
    oDetailForm.sBackground = sBackground;
    oDetailForm.oDocument = [aDocuments objectAtIndex:0];
    oPdfDetail.oDetailForm = oDetailForm;
    return oPdfDetail;
}

+ (MenuCollectionViewController *) obtainMenuCollectionViewControlerWithDocuments:(MenuCollectionForm *) oMenuCollectionForm oStoryBoard:(UIStoryboard *) oStoryBoard sTitleScreen:(NSString *) sTitleScreen sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar sBackground:(NSString *) sBackground nIdMenu:(int) nIdMenu{
    
    MenuCollectionViewController *oMenuCollection = (MenuCollectionViewController *)[oStoryBoard instantiateViewControllerWithIdentifier:@"MenuCollectionViewController"];
    oMenuCollectionForm.nIdMenu = nIdMenu;
    oMenuCollectionForm.sTitleScreen = sTitleScreen;
    oMenuCollectionForm.sTitleNavBar = sTitleNavBar;
    oMenuCollectionForm.sSubTitleNavBar = sSubTitleNavBar;
    oMenuCollectionForm.sBackground = sBackground;
    oMenuCollection.oMenuForm = oMenuCollectionForm;
    return oMenuCollection;
}

+ (AlbumPhotoViewController *) obtainAlbumPhotoViewControlerWithDocuments:(AlbumCollectionForm *) oAlbumForm oStoryBoard:(UIStoryboard *) oStoryBoard sTitleNavBar:(NSString *) sTitleNavBar sSubTitleNavBar:(NSString *) sSubTitleNavBar nIdMenu:(int) nIdMenu{
    AlbumPhotoViewController *oAlbum = (AlbumPhotoViewController *)[oStoryBoard instantiateViewControllerWithIdentifier:@"AlbumPhotoViewController"];
    oAlbumForm.sTitleNavBar = sTitleNavBar;
    oAlbumForm.sSubTitleNavBar = sSubTitleNavBar;
    oAlbumForm.nIdMenu = nIdMenu;
    oAlbum.oAlbumForm = oAlbumForm;
    return oAlbum;
}

+ (CGSize)sizeOfLabel:(UILabel *) label withText:(NSString *)text {
    label.font = label.font;
    label.text = text;
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    [label setNumberOfLines:0];
    CGSize maximumLabelSize = CGSizeMake(label.frame.size.width, 9999);
    CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
    return expectedSize;
}

+ (void)createAndResizeImage:(NSString *) sImage andInsertIntUIImageView:(UIImageView *) oContentImage{
    UIImage *oRealImage = [UIImage imageNamed:sImage];
    [CBREUtils resizeImage:oRealImage andInsertIntUIImageView:oContentImage];
}

+ (void)resizeImage:(UIImage *) oImage andInsertIntUIImageView:(UIImageView *) oContentImage{
    CGSize nSize = oContentImage.frame.size;
    UIGraphicsBeginImageContextWithOptions(nSize, NO, 0.0);
    [oImage drawInRect:CGRectMake(0, 0, nSize.width, nSize.height)];
    UIImage *oResizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [oContentImage setImage:oResizeImage];
}
@end
