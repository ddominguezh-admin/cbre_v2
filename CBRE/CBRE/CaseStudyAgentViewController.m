//
//  CaseStudyAgentViewController.m
//  CBRE
//
//  Created by ddominguezh on 01/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "CaseStudyAgentViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionCell.h"
#import "DocumentDao.h"
#import "AgencyTenantViewController.h"

@interface CaseStudyAgentViewController ()

@property (strong, nonatomic) DocumentBean *oDocumentSelected;
@property int indexPathSelected;

@end

@implementation CaseStudyAgentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _indexPathSelected = 1;
    
    [CBREFont setFontInButtons:[[NSArray alloc] initWithObjects:_oBtnAllCases, _oBtnNaves, _oBtnLocales, _oBtnStayAndGo, nil] withType:CBREFontTypeHelveticaRegular];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    
    _aDocumentsNaves = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesCasosDeEstudioAgenciaNaves];
    _aDocumentsLocales = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesCasosDeEstudioAgenciaLocales];
    _aDocumentsStayAndGo = [[DocumentDao sharedInstance] getDocumentsByType:DocumentOcupantesCasosDeEstudioAgenciaStayAndGo];
    
    [_oBtnAllCases setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.todos"] forState:UIControlStateNormal];
    [_oBtnNaves setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.naves"] forState:UIControlStateNormal];
    [_oBtnLocales setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.locales"] forState:UIControlStateNormal];
    [_oBtnStayAndGo setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.stayAndGo"] forState:UIControlStateNormal];

    NSArray *aViews = self.navigationController.viewControllers;
    UIViewController *oLastView = [aViews objectAtIndex:aViews.count - 2];
    
    NSString *sSubTitle = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"]];
    
    if ([oLastView isKindOfClass:[AgencyTenantViewController class]]) {
        sSubTitle = [NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.agent.agent"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"]];
    }
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:sSubTitle];
    
    [super loadMenuButtonsBack:self andIdMenu:1];
    
    [self executeAllCases];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection view delegate functions

- (long) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (long)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aDocuments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"PDFCollectionCell";
    PDFCollectionCell *oPDFCollectionCell = (PDFCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    DocumentBean *oDocument = [_aDocuments objectAtIndex:indexPath.row];
    oPDFCollectionCell.oDocument = oDocument;
    [oPDFCollectionCell.oBtnPdf setImage:[UIImage imageNamed:oDocument.sImgName] forState:UIControlStateNormal];
    [oPDFCollectionCell.oBtnPdf setTag:indexPath.row];
    [oPDFCollectionCell.oBtnPdf addTarget:self action:@selector(viewPdf:) forControlEvents:UIControlEventTouchDown];
    [oPDFCollectionCell.oLblName setText:oDocument.sName];
    return oPDFCollectionCell;
}

- (IBAction)previousDocument:(id)sender {
    BOOL bCanMove = _indexPathSelected > 1;
    if (bCanMove) {
        _indexPathSelected--;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)nextDocument:(id)sender {
    long nLastStep = _aDocuments.count - 2;
    BOOL bCanMove = _indexPathSelected < nLastStep;
    if (bCanMove) {
        _indexPathSelected++;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)viewPdf:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = 2;
    oDetailForm.sTitleNavBar = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"];
    oDetailForm.sSubTitleNavBar = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent"]];
    oDetailForm.sBackground = @"AgencyFondo";
    oDetailForm.oDocument = [_aDocuments objectAtIndex:oButton.tag];
    oPdfDetail.oDetailForm = oDetailForm;
    [self.navigationController pushViewController:oPdfDetail animated:true];
}

- (IBAction)filterAllCases:(id)sender {
    [self resetColorLabels];
    [self executeAllCases];
}

- (void)executeAllCases{
    [self setSelectedLable:_oBtnAllCases];
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    
    [aDocuments addObjectsFromArray:_aDocumentsNaves];
    [aDocuments addObjectsFromArray:_aDocumentsLocales];
    [aDocuments addObjectsFromArray:_aDocumentsStayAndGo];
    
    _aDocuments = (NSArray<DocumentBean> *)[[NSArray alloc] initWithArray:aDocuments];
    [self reloadCollectionData];
}

- (IBAction)filterNaves:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnNaves];
    _aDocuments = _aDocumentsNaves;
    [self reloadCollectionData];
}

- (IBAction)filterLocales:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnLocales];
    _aDocuments = _aDocumentsLocales;
    [self reloadCollectionData];
}

- (IBAction)filterStayAndGo:(id)sender {
    [self resetColorLabels];
    [self setSelectedLable:_oBtnStayAndGo];
    _aDocuments = _aDocumentsStayAndGo;
    [self reloadCollectionData];
}

- (void)resetColorLabels{
    [_oBtnAllCases setSelected:FALSE];
    [_oBtnNaves setSelected:FALSE];
    [_oBtnLocales setSelected:FALSE];
    [_oBtnStayAndGo setSelected:FALSE];
    _indexPathSelected = 1;
}

- (void)setSelectedLable:(UIButton *) oButton{
    NSString *sTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.agent.agent"];
    [_oLblNameScreen setText:[NSString stringWithFormat:@"%@ (%@)", sTitle, oButton.titleLabel.text].uppercaseString];
    [oButton setSelected:TRUE];
}

- (void)reloadCollectionData{
    long nCountDocuments = _aDocuments.count;
    if (nCountDocuments < 4) {
        [_oBtnPrevious setHidden:TRUE];
        [_oBtnNext setHidden:TRUE];
    }else{
        [_oBtnPrevious setHidden:FALSE];
        [_oBtnNext setHidden:FALSE];
    }
    if (nCountDocuments < 3)
        [self calculateFrameCollectionView:nCountDocuments];
    else
        [self calculateFrameCollectionView:3];
    [_oClvDocuments reloadData];
}

- (void)calculateFrameCollectionView:(long) nCountDocuments{
    float nWidth = 760 - ((760 / 3) * nCountDocuments);
    _oConstClvDocsWidth.constant = 760 - nWidth;
    _oConstClvDocsPosX.constant = 128 + (nWidth / 2);
}

@end
