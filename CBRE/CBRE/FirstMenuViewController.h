//
//  FirstMenuViewController.h
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface FirstMenuViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UIButton *oBtnGoRetail;
@property (weak, nonatomic) IBOutlet UIButton *oBtnGoIndustrial;
@property (weak, nonatomic) IBOutlet UIButton *oBtnGoHotels;
@property (weak, nonatomic) IBOutlet UIButton *oBtnGoResidential;
@property (weak, nonatomic) IBOutlet UILabel *oLblGoHotels;
@property (weak, nonatomic) IBOutlet UILabel *oLblGoOffices;
@property (weak, nonatomic) IBOutlet UILabel *oLblGoResidential;
@property (weak, nonatomic) IBOutlet UILabel *oLblGoRetail;
@property (weak, nonatomic) IBOutlet UILabel *oLblGoIndustrial;

@end
