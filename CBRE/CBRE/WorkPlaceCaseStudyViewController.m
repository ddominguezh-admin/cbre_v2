//
//  WorkPlaceCaseStudyViewController.m
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "WorkPlaceCaseStudyViewController.h"
#import "PDFDetailViewController.h"
#import "PDFDetailForm.h"
#import "PDFCollectionCell.h"
#import "DocumentDao.h"
#import "WorkPlaceOptionViewController.h"
#import "CBREPopoverNotBackgroundView.h"
#import "ClientNameCell.h"
#import "ClientDao.h"

@interface WorkPlaceCaseStudyViewController ()

@property int indexPathSelected;

@end

@implementation WorkPlaceCaseStudyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInButton:_oBtnFilterClient withType:CBREFontTypeHelveticaNeue87HeavyCondensed];
    [CBREFont setFontInLabels:[[NSArray alloc] initWithObjects:_oLblGestion, _oLblMediacion, _oLblDefinicion, nil] withType:CBREFontTypeHelveticaRegular];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    [_oBtnDefinicion setSelected:TRUE];
    [_oBtnGestion setSelected:TRUE];
    [_oBtnMediacion setSelected:TRUE];
    [self reloadCollectionData];
    
    [_oLblNameScreen setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.casos"].uppercaseString];
    [_oLblDefinicion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.definicion"].uppercaseString];
    [_oLblGestion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.gestion"].uppercaseString];
    [_oLblMediacion setText:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.mediacion"].uppercaseString];
    [_oBtnFilterClient setTitle:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.filtro"].uppercaseString forState:UIControlStateNormal];
    
    ClientBean *oClientZero = [[ClientBean alloc] init];
    oClientZero.nIdClient = 0;
    oClientZero.sName = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.todos.los.clientes"];
    _oClient = oClientZero;
    
    NSArray *aViews = self.navigationController.viewControllers;
    UIViewController *oLastView = [aViews objectAtIndex:aViews.count - 2];
    
    NSString *sSubTitle = [NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace"]];
    
    if ([oLastView isKindOfClass:[WorkPlaceOptionViewController class]]) {
        sSubTitle = [NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.casesStudy.casesStudy"]];
    }
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:sSubTitle];
    
    _aClients = (NSMutableArray<ClientBean> *)[[NSMutableArray alloc] init];
    [_aClients addObject:oClientZero];

    [self obtainDataAndFormat];
    [self filterDocumentsClients];
    [super loadMenuButtonsBack:self andIdMenu:2];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark collection view delegate functions

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _aDocuments.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"PDFCollectionCell";
    PDFCollectionCell *oPDFCollectionCell = (PDFCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];
    
    DocumentBean *oDocument = [_aDocuments objectAtIndex:indexPath.row];
    oPDFCollectionCell.oDocument = oDocument;
    [oPDFCollectionCell.oBtnPdf setImage:[UIImage imageNamed:oDocument.sImgName] forState:UIControlStateNormal];
    [oPDFCollectionCell.oBtnPdf setTag:indexPath.row];
    [oPDFCollectionCell.oBtnPdf addTarget:self action:@selector(viewPdf:) forControlEvents:UIControlEventTouchDown];
    [oPDFCollectionCell.oLblName setText:oDocument.sName];
    return oPDFCollectionCell;
}

- (IBAction)previousDocument:(id)sender {
    BOOL bCanMove = _indexPathSelected > 1;
    if (bCanMove) {
        _indexPathSelected--;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)nextDocument:(id)sender {
    long nLastStep = _aDocuments.count - 2;
    BOOL bCanMove = _indexPathSelected < nLastStep;
    if (bCanMove) {
        _indexPathSelected++;
        [_oClvDocuments scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_indexPathSelected inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:TRUE];
    }
}

- (IBAction)viewPdf:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    PDFDetailViewController *oPdfDetail = (PDFDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"PDFDetailViewController"];
    PDFDetailForm *oDetailForm = [[PDFDetailForm alloc] init];
    oDetailForm.nIdMenu = 2;
    oDetailForm.sTitleNavBar = @"OCUPANTES";
    oDetailForm.sSubTitleNavBar = @"Workplace";
    oDetailForm.sBackground = @"WorkplaceFondo";
    oDetailForm.oDocument = [_aDocuments objectAtIndex:oButton.tag];
    oPdfDetail.oDetailForm = oDetailForm;
    [self.navigationController pushViewController:oPdfDetail animated:true];
}

- (IBAction)selectedCheck:(id)sender {
    UIButton *oButton = (UIButton *)sender;
    [oButton setSelected:!oButton.selected];
    [self filterDocumentsClients];
}

- (void)filterDocumentsClients{
    NSString *sTitle = [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.workplace.workplace.casos"];
   
    
    NSMutableArray<DocumentBean> *aDocuments = (NSMutableArray<DocumentBean> *)[[NSMutableArray alloc] init];
    
    
    if (_oClient.nIdClient.intValue == 0) {
        
        [_oLblNameScreen setText:[NSString stringWithFormat:@"%@",sTitle].uppercaseString];
        
        long nLoopDocuments = 0, nCountDocuments = _aAllDocument.count;
        DocumentBean *oDocument = NULL;
        for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
            oDocument = [_aAllDocument objectAtIndex:nLoopDocuments];
            
            if(_oBtnDefinicion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoEstrategia)
                [aDocuments addObject:oDocument];
            
            if(_oBtnGestion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoCambio)
                [aDocuments addObject:oDocument];
            
            if(_oBtnMediacion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoMediacion)
                [aDocuments addObject:oDocument];
            
        }
        
    }else{
        
        [_oLblNameScreen setText:[NSString stringWithFormat:@"%@ (%@)",sTitle , _oClient.sName].uppercaseString];
        
        long nLoopDocuments = 0, nCountDocuments = _aAllDocument.count;
        DocumentBean *oDocument = NULL;
        for (; nLoopDocuments < nCountDocuments; nLoopDocuments++) {
            oDocument = [_aAllDocument objectAtIndex:nLoopDocuments];
            
            if (oDocument.nIdOwner.intValue == _oClient.nIdClient.intValue) {
                if(_oBtnDefinicion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoEstrategia)
                    [aDocuments addObject:oDocument];
                
                if(_oBtnGestion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoCambio)
                    [aDocuments addObject:oDocument];
                
                if(_oBtnMediacion.selected && oDocument.nIdDocumentType.intValue == DocumentOcupantesLugarDeTrabajoMediacion)
                    [aDocuments addObject:oDocument];
            }
        }
        
    }

    _aDocuments = (NSArray<DocumentBean> *)[[NSArray alloc] initWithArray:aDocuments];

    [self reloadCollectionData];
}

- (void)reloadCollectionData{
    _indexPathSelected = 1;
    long nCountDocuments = _aDocuments.count;
    if (nCountDocuments < 4) {
        [_oBtnPrevious setHidden:TRUE];
        [_oBtnNext setHidden:TRUE];
    }else{
        [_oBtnPrevious setHidden:FALSE];
        [_oBtnNext setHidden:FALSE];
    }
    if (nCountDocuments < 3)
        [self calculateFrameCollectionView:nCountDocuments];
    else
        [self calculateFrameCollectionView:3];
    [_oClvDocuments reloadData];
}

- (void)calculateFrameCollectionView:(long) nCountDocuments{
    float nWidth = 760 - ((760 / 3) * nCountDocuments);
    _oConstClvDocsWidth.constant = 760 - nWidth;
    _oConstClvDocsPosX.constant = 128 + (nWidth / 2);
}

#pragma mark client list popover view delegate functions

- (IBAction)showPopoverClientList:(id)sender {
    [self hideOrShowPopoverClient:FALSE];
}

- (IBAction)hidePopoverControl:(id)sender {
    [self hideOrShowPopoverClient:TRUE];
}

- (void)hideOrShowPopoverClient:(BOOL) bHidden{
    [_oCntViewPopup setHidden:bHidden];
    [_oTblClients setHidden:bHidden];
}

#pragma mark table view delegate functions

- (long)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (long)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _aClients.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentificer = @"ClientNameCell";
    ClientNameCell *oClientNameCell = [tableView dequeueReusableCellWithIdentifier:sCellIdentificer];
    if (oClientNameCell == nil) {
        oClientNameCell = [[ClientNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sCellIdentificer];
    }
    ClientBean *oClient = [_aClients objectAtIndex:indexPath.row];
    oClientNameCell.oClient = oClient;
    [oClientNameCell.oLblName setText:oClient.sName.uppercaseString];
    [CBREFont setFontInLabel:oClientNameCell.oLblName withType:CBREFontTypeHelveticaNeue67Medium];
    return oClientNameCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ClientNameCell *oClientNameCell = (ClientNameCell *)[tableView cellForRowAtIndexPath:indexPath];
    ClientBean *oClient = oClientNameCell.oClient;
    if (_oClient == NULL || oClient.nIdClient.intValue != _oClient.nIdClient.intValue) {
        _oClient = oClient;
        [self filterDocumentsClients];
    }
    [self hideOrShowPopoverClient:TRUE];
}

- (void)obtainDataAndFormat{
    _aAllDocument = (NSMutableArray<DocumentBean>*)[[NSMutableArray alloc] init];
    NSArray *aClients = [[ClientDao sharedInstance] getClientsDocumentsWorkplace];
    [_aClients addObjectsFromArray:aClients];
    
    long nLoopClient = 0, nCountClient = (long)aClients.count;
    ClientBean *oClient = NULL;
    for ( ; nLoopClient < nCountClient ; nLoopClient++) {
        oClient = [aClients objectAtIndex:nLoopClient];
        
        NSArray<DocumentBean> *aDocuments = oClient.aDocuments;
        long nLoopDocument = 0, nCountDocument = (long)aDocuments.count;
        DocumentBean *oDocument = NULL;
        for ( ; nLoopDocument < nCountDocument ; nLoopDocument++) {
            oDocument = [aDocuments objectAtIndex:nLoopDocument];
            oDocument.nIdOwner = oClient.nIdClient;
            [_aAllDocument addObject:oDocument];
        }
    }
}

@end
