//
//  BuildingPhotoViewController.h
//  CBRE
//
//  Created by ddominguezh on 16/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@class AlbumCollectionForm;
#import "PhotoBean.h"
@class BuildingBean;

@interface AlbumPhotoViewController : CBRENavigationViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) AlbumCollectionForm *oAlbumForm;
@property (strong, nonatomic) NSArray<PhotoBean> *aPhotos;
@property (weak, nonatomic) IBOutlet UICollectionView *oCblPhotos;
@property (weak, nonatomic) IBOutlet UIScrollView *oScrContentImage;
@property (weak, nonatomic) IBOutlet UIImageView *oContentImage;
@property (weak, nonatomic) IBOutlet UILabel *oLblContentCollection;
@property (weak, nonatomic) IBOutlet UIImageView *oImgFondoLuto;
@property (weak, nonatomic) IBOutlet UIButton *oBtnPreviosPhoto;
@property (weak, nonatomic) IBOutlet UIButton *oBtnNextPhoto;

- (IBAction)showPreviousPhoto:(id)sender;
- (IBAction)showNextPhoto:(id)sender;

@end
