//
//  SpanishMapViewController.h
//  CBRE
//
//  Created by ddominguezh on 04/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface SpanishMapViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet MKMapView *oMapView;

@end
