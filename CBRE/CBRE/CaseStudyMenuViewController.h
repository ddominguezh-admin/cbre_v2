//
//  CaseStudyMenuViewController.h
//  CBRE
//
//  Created by ddominguezh on 01/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface CaseStudyMenuViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UIButton *oBtnAgencia;
@property (weak, nonatomic) IBOutlet UIButton *oBtnProject;
@property (weak, nonatomic) IBOutlet UIButton *oBtnEnergia;
@property (weak, nonatomic) IBOutlet UIButton *oBtnWorkplace;

@end
