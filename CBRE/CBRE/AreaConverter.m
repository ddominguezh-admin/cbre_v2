//
//  AreaConverter.m
//  CBRE
//
//  Created by ddominguezh on 24/08/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "AreaConverter.h"

@implementation AreaConverter

+ (NSArray<AreaBean> *) jsonToAreas:(NSDictionary *) oData{
    NSMutableArray<AreaBean> *aAreas = (NSMutableArray<AreaBean> *)[[NSMutableArray alloc] init];
    NSArray *oResult = [oData objectForKey:@"results"];
    
    long nLoopAreas = 0, nCountAreas = (long)oResult.count;
    NSDictionary *oDataArea = NULL;
    
    for (; nLoopAreas < nCountAreas; nLoopAreas++) {
        oDataArea = [oResult objectAtIndex:nLoopAreas];
        [aAreas addObject:[AreaConverter jsonToArea:oDataArea]];
    }
    
    return aAreas;
}

+ (AreaBean *) jsonToArea:(NSDictionary *) oData{
    NSNumber *nIdArea = [oData objectForKey:@"id"];
    NSString *sName = [oData objectForKey:@"name"];
    NSNumber *nLatitude = [oData objectForKey:@"latitud"];
    NSNumber *nLongitude = [oData objectForKey:@"longitud"];
    NSNumber *nZoom = [oData objectForKey:@"zoom"];
    NSDictionary *oDataColour = [oData objectForKey:@"map_colour"];
    NSString *sLeyenda = [oDataColour objectForKey:@"leyend"];
    NSString *sPin = [oDataColour objectForKey:@"pin"];
    NSString *sColourText = [oDataColour objectForKey:@"text_colour"];
    NSString *sColourArea = [oDataColour objectForKey:@"pin"];
    NSArray<AreaPointBean> *aAreaPoints = [AreaConverter jsonToAreaPoints:[oData objectForKey:@"area_points"]];
    return [[AreaBean alloc] initWhitParameters:nIdArea sName:sName nLatitude:nLatitude nLongitude:nLongitude nZoom:nZoom sLeyenda:sLeyenda sPin:sPin sColourText:sColourText sColourArea:sColourArea aAreaPoints:aAreaPoints];
}

+ (NSArray<AreaPointBean> *) jsonToAreaPoints:(NSArray *) aJsonPoints{
    NSMutableArray<AreaPointBean> * aPoints = (NSMutableArray<AreaPointBean> *)[[NSMutableArray alloc] init];
    
    long nLoopPoints = 0, nCountPoints = (long)aJsonPoints.count;
    NSDictionary *oDataPoint = NULL;
    for (; nLoopPoints < nCountPoints; nLoopPoints++) {
        oDataPoint = [aJsonPoints objectAtIndex:nLoopPoints];
        [aPoints addObject:[AreaConverter jsonToAreaPoint:oDataPoint]];
    }
    
    return aPoints;
}

+ (AreaPointBean *) jsonToAreaPoint:(NSDictionary *) oData{
    NSNumber *nIdPoint = NULL;// [oData objectForKey:@"id"];
    NSNumber *nLatitude = [oData objectForKey:@"latitud"];
    NSNumber *nLongitude = [oData objectForKey:@"longitud"];
    return [[AreaPointBean alloc] initWhitParameters:nIdPoint nLatitude:nLatitude nLongitude:nLongitude];
}

@end
