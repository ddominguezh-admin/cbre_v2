//
//  MarketPdfCell.m
//  CBRE
//
//  Created by ddominguezh on 28/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MarketPdfCell.h"

@implementation MarketPdfCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
