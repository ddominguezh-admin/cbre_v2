//
//  ProjectManagementGestionViewController.m
//  CBRE
//
//  Created by ddominguezh on 02/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ProjectManagementGestionViewController.h"
#import "MenuCollectionCell.h"
#import "MenuCollection.h"
#import "MenuCollectionForm.h"
#import "DocumentDao.h"
#import "DocumentBean.h"

@interface ProjectManagementGestionViewController ()

@end

@implementation ProjectManagementGestionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CBREFont setFontInLabel:_oLblNameScreen withType:CBREFontTypeHelveticaNeue57Condensed];
    _oMenuForm = [[MenuCollectionForm alloc] init];
    [_oMenuForm.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlCalidad" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.calidad"] nTypeDocument:DocumentOcupantesGestionProyectosGestionCalidad]];
    [_oMenuForm.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlSeguros" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.seguros"] nTypeDocument:DocumentOcupantesGestionProyectosGestionSeguros]];
    [_oMenuForm.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlSeguridadSalud" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.seguridad"] nTypeDocument:DocumentOcupantesGestionProyectosGestionSeguridad]];
    [_oMenuForm.aMenus addObject:[[MenuCollection alloc] initWhitParameters:@"CvlMetodologia" sName:[[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion.metodologia"] nTypeDocument:DocumentOcupantesGestionProyectosGestionMetodologia]];
    
    _oMenuForm.nRows = 1;
    _oMenuForm.nColumns = 4;
    
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] andSubtitle:[NSString stringWithFormat:@"%@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.projectManagement"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion"]]];
    [super loadMenuButtonsBack:self andIdMenu:1];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection view delegate functions

- (long) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (long)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _oMenuForm.aMenus.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sCellIdentifier = @"MenuCollectionCell";
    MenuCollectionCell *oMenuCollectionCell = (MenuCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:sCellIdentifier forIndexPath:indexPath];

    MenuCollection *oMenuCollection = [_oMenuForm.aMenus objectAtIndex:indexPath.row];
    oMenuCollectionCell.oMenu = oMenuCollection;
    [oMenuCollectionCell.oBtnMenu setImage:[UIImage imageNamed:oMenuCollection.sImgButton] forState:UIControlStateNormal];
    [oMenuCollectionCell.oBtnMenu setTag:indexPath.row];
    [oMenuCollectionCell.oBtnMenu addTarget:self action:@selector(viewMenu:) forControlEvents:UIControlEventTouchDown];
    [oMenuCollectionCell.oLblName setText:oMenuCollection.sName];
    [CBREFont setFontInLabel:oMenuCollectionCell.oLblName withType:CBREFontTypeHelveticaNeue57Condensed];

    return oMenuCollectionCell;
}

- (IBAction)viewMenu:(id)sender{
    UIButton *oButton = (UIButton *)sender;
    MenuCollection *oMenuCollection = [_oMenuForm.aMenus objectAtIndex:oButton.tag];
    if (DocumentOcupantesGestionProyectosGestionMetodologia == oMenuCollection.nTypeDocument) {
        [self performSegueWithIdentifier:@"showMenuMediacion" sender:self];
    }else{
        NSArray<DocumentBean> *aDocuments = [[DocumentDao sharedInstance] getDocumentsByType:oMenuCollection.nTypeDocument];
        NSString *sSubTitleNavBar = [NSString stringWithFormat:@"%@ / %@ / %@", [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.projectManagement"], [[CBRELocalized sharedInstance] getMessage:@"i18n.occupants.projectManagement.gestion"], oMenuCollection.sName];
        
        UIViewController *oPDFControler = [CBREUtils obtainViewControlerWithDocuments:aDocuments oStoryBoard:self.storyboard sTitleScreen:oMenuCollection.sName sTitleNavBar:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"] sSubTitleNavBar:sSubTitleNavBar sBackground:@"ProjectManagementFondo" nIdMenu:2];
        if (oPDFControler != NULL)
            [self.navigationController pushViewController:oPDFControler animated:true];
    }
}

@end
