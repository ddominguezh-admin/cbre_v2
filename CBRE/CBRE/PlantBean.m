//
//  PlantBean.m
//  CBRE
//
//  Created by ddominguezh on 21/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PlantBean.h"

@implementation PlantBean

- (id)initWhitParameters:(NSString *) nPlant
                 nMeters:(NSNumber *) nMeters{
    if (self == NULL)
        self = [super init];
    self.nPlant = nPlant;
    self.nMeters = nMeters;
    return self;
}

@end
