//
//  FilterMapClientPopoverViewController.h
//  CBRE
//
//  Created by ddominguezh on 13/07/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterMapClientForm.h"

@protocol FilterMapClientPopoverViewControllerDelegate;

@interface FilterMapClientPopoverViewController : UIViewController

@property (strong, nonatomic) id<FilterMapClientPopoverViewControllerDelegate> delegate;
@property (strong, nonatomic) FilterMapClientForm *oFilterMap;

@property (weak, nonatomic) IBOutlet UISwitch *oSwtMetersOne;
@property (weak, nonatomic) IBOutlet UISwitch *oSwtMetersTwo;
@property (weak, nonatomic) IBOutlet UISwitch *oSwtMetersThree;
@property (weak, nonatomic) IBOutlet UIButton *oBtnFilter;

@property (weak, nonatomic) IBOutlet UILabel *oLblTitle;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleMetersOne;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleMetersTwo;
@property (weak, nonatomic) IBOutlet UILabel *oLblTitleMetersThree;

- (IBAction)filterMap:(id)sender;
- (IBAction)enableMetersOne:(id)sender;
- (IBAction)enableMetersTwo:(id)sender;
- (IBAction)enableMetersThree:(id)sender;

@end

@protocol FilterMapClientPopoverViewControllerDelegate <NSObject>

- (void)hidePopoverAndFilterMapClient:(FilterMapClientForm *) oFilter;

@end
