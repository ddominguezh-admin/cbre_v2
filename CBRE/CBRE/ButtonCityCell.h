//
//  ButtonCityCell.h
//  CBRE
//
//  Created by ddominguezh on 28/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityBean.h"

@interface ButtonCityCell : UITableViewCell

@property (strong, nonatomic) CityBean *oCity;
@property (weak, nonatomic) IBOutlet UILabel *oLblName;

@end
