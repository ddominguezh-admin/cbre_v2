//
//  AgencyAnalysisViewController.h
//  CBRE
//
//  Created by ddominguezh on 29/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "DocumentBean.h"

@interface AgencyAnalysisViewController : CBRENavigationViewController

@property (weak, nonatomic) IBOutlet UILabel *oLblNameScreen;
@property (weak, nonatomic) IBOutlet UILabel *oLblAnalisis;
@property (weak, nonatomic) IBOutlet UILabel *oLblMatriz;

- (IBAction)viewAnalisis:(id)sender;
- (IBAction)viewMatriz:(id)sender;

@end
