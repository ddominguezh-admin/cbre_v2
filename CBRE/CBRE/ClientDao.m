//
//  CustomerDao.m
//  CBRE
//
//  Created by ddominguezh on 27/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "ClientDao.h"
#import "ClientConverter.h"
#import "PhotoConverter.h"

@implementation ClientDao

static ClientDao *sharedInstance;

+ (instancetype)sharedInstance{
    if(sharedInstance == NULL)
        sharedInstance = [[ClientDao alloc] init];
    return sharedInstance;
}

- (NSArray<ClientBean> *)getClientsByArea:(NSNumber *) nIdArea{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"client/?area=%@&", nIdArea]];
        return [ClientConverter jsonToClients:oData];

    }

    return (NSArray<ClientBean> *)[[NSArray alloc] init];
}

- (NSArray<ClientBean> *)getClientsByArea:(NSNumber *) nIdArea andFilterForm:(FilterMapClientForm *)oFilterMap{
    
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"client/?area=%@&%@", nIdArea, [oFilterMap getParamsUrl]]];
        return [ClientConverter jsonToClients:oData];
        
    }
    
   return (NSArray<ClientBean> *)[[NSArray alloc] init]; 
}

- (NSArray<ClientBean> *)getAllClientsName{
    if ([super checkInternetConecction]) {
    
        NSDictionary *oData = [super getDataByURL:@"client?"];
        return [ClientConverter jsonToClients:oData];
        
    }
    
    return (NSArray<ClientBean> *)[[NSArray alloc] init];
}

- (NSArray<ClientBean> *)getAllClientsWorkplaceName{
    if ([super checkInternetConecction]) {
        
        NSDictionary *oData = [super getDataByURL:@"client/?flg_show_workspace=1&"];
        return [ClientConverter jsonToClients:oData];
        
    }
    
    return (NSArray<ClientBean> *)[[NSArray alloc] init];
}

- (void)fillFullDataCustomer:(ClientBean **) oClient{
    if ([super openConnection]) {
        
        char *sSelect = "";
        sqlite3_stmt *oStatement = NULL;;
        [super executeSQLStatement:sSelect oStatement:&oStatement];
        
        while(sqlite3_step(oStatement) == SQLITE_ROW){
        }

        [super closeConnection];
    }
}

- (NSArray<PhotoBean> *)getPhotosByClient:(NSNumber *) nIdClient{
    if ([super openConnection]) {
        
        NSDictionary *oData = [super getDataByURL:[NSString stringWithFormat:@"client_documents/?client=%@&document_type=%d&", nIdClient, DocumentClientPhoto]];
        return [PhotoConverter jsonToPhotos:oData];
        
    }
    return (NSArray<PhotoBean> *)[[NSArray alloc] init];
}

- (NSArray<ClientBean> *)getClientsDocumentsWorkplace{
    if ([super checkInternetConecction]) {
        NSDictionary *oData = [super getDataByURL:@"client_documents/clients_workspace?"];
        return [ClientConverter arrayToClients:(NSArray *)oData];
    }
    return (NSArray<ClientBean> *)[[NSArray alloc] init];
}
@end
