//
//  AppDelegate.h
//  CBRE
//
//  Created by Berganza on 20/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Mapbox/Mapbox.h>
#import "CityBean.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSArray<CityBean> *aCities;
//@property (strong, nonatomic) NSMutableDictionary *aCitiesTiles;
//@property (strong, nonatomic) RMMapView *oMap;
// Comentario

@end
