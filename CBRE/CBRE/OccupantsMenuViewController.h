//
//  OccupantsMenuViewController.h
//  CBRE
//
//  Created by ddominguezh on 03/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

@interface OccupantsMenuViewController : CBRENavigationViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *oScrollContent;

@property (weak, nonatomic) IBOutlet UILabel *oLblOcupants;

@property (weak, nonatomic) IBOutlet UILabel *oLblBusquedaYAnalisis;
@property (weak, nonatomic) IBOutlet UILabel *oLblBusquedaAlternativas;
@property (weak, nonatomic) IBOutlet UILabel *oLblAnalisisEspacio;
@property (weak, nonatomic) IBOutlet UILabel *oLblEstudiosConsumo;
@property (weak, nonatomic) IBOutlet UILabel *oLblAnalisisEscenarios;
@property (weak, nonatomic) IBOutlet UILabel *oLblEvaluacionTecnica;

@property (weak, nonatomic) IBOutlet UILabel *oLblNegociacionCierre;
@property (weak, nonatomic) IBOutlet UILabel *oLblNegociacion;

@property (weak, nonatomic) IBOutlet UILabel *oLblProyectoConstruccion;
@property (weak, nonatomic) IBOutlet UILabel *oLblDesarrolloProyecto;
@property (weak, nonatomic) IBOutlet UILabel *oLblDisenioSostenible;
@property (weak, nonatomic) IBOutlet UILabel *oLblGestionCambio;
@property (weak, nonatomic) IBOutlet UILabel *oLblProcesoLicitacion;
@property (weak, nonatomic) IBOutlet UILabel *oLblGestionConstruccion;
@property (weak, nonatomic) IBOutlet UILabel *oLblConstruccionSostenible;

@property (weak, nonatomic) IBOutlet UILabel *oLblSeguimiento;
@property (weak, nonatomic) IBOutlet UILabel *oLblStayVersusGo;
@property (weak, nonatomic) IBOutlet UILabel *oLblMediacionAjuste;
@property (weak, nonatomic) IBOutlet UILabel *oLblOperacionSostenible;
@property (weak, nonatomic) IBOutlet UILabel *oLblFacilityManagement;

@property (weak, nonatomic) IBOutlet UILabel *oLblCaseStudies;

@property (weak, nonatomic) IBOutlet UILabel *oLblKickOff;
@property (weak, nonatomic) IBOutlet UILabel *oLblAprobacionBuss;
@property (weak, nonatomic) IBOutlet UILabel *oLblFirmaContrato;
@property (weak, nonatomic) IBOutlet UILabel *oLblInicioCompra;
@property (weak, nonatomic) IBOutlet UILabel *oLblMudanza;
@property (weak, nonatomic) IBOutlet UILabel *oLblRevisionCumplimiento;

- (IBAction)moveContentScroll:(id)sender;
- (IBAction)pushButtonGoToNewScreen:(id)sender;

@end
