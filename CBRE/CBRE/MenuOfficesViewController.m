//
//  MenuOfficesViewController.m
//  CBRE
//
//  Created by ddominguezh on 21/05/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "MenuOfficesViewController.h"

@interface MenuOfficesViewController ()

@end

@implementation MenuOfficesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [CBREUtils createTitleViewNavigation:[[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.navigation.title"]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString *sOffices = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.offices"];
    [_oLblOffices setText:sOffices];
    NSString *sPropietaries = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.proprietary"];
    [_oLblPropietaries setText:sPropietaries];
    NSString *sOccupants = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.occupants"];
    [_oLblOccupants setText:sOccupants];
    NSString *sResearch = [[CBRELocalized sharedInstance] getMessage:@"i18n.menuOffices.research"];
    [_oLblResearch setText:sResearch];
    
    NSArray *aLabesl = [[NSArray alloc] initWithObjects:_oLblOffices, _oLblPropietaries, _oLblOccupants, _oLblResearch, nil];
    [CBREFont setFontInLabels:aLabesl withType:CBREFontTypeHelveticaNeue57Condensed];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
