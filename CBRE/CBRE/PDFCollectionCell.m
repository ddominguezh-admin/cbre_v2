//
//  PDFCollectionCell.m
//  CBRE
//
//  Created by ddominguezh on 26/06/14.
//  Copyright (c) 2014 Berganza. All rights reserved.
//

#import "PDFCollectionCell.h"

@implementation PDFCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
